#!/bin/bash

HOST=epc.ethv.net
PORT=222
USER=epc

source upload-info
FILES="upload/latest-windows.json upload/win-release.txt upload/win-release"
rsync -a -e "ssh -p $PORT" upload/ $USER@$HOST:~/serve/

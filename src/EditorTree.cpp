#include <QMenu>

#include "EditorTree.h"

EditorTree::EditorTree() : m_synchronizing(false), m_menuFor(nullptr) {
    setContextMenuPolicy(Qt::CustomContextMenu);
    sortByColumn(0, Qt::AscendingOrder);

    connect(this, &QTreeWidget::customContextMenuRequested,
        [=](const QPoint &pos) {

        auto item = itemAt(pos);
        if(!item) return;

        auto type = typeOf(item);

        if(type == -1) return;

        auto menu_it = m_contextMenus.find(type);
        if(menu_it == m_contextMenus.end()) return;

        auto menu = menu_it->second(item);

        if(menu) {
            m_menuFor = item;
            menu->popup(mapToGlobal(pos));
            menu->exec();
            m_menuFor = nullptr;
        }
    });
}

#pragma once

#include <functional>

#include "CharacterWidget.h"

#include "data/Character.h"

#include "TraitProxy.h"

class QTreeWidget;
class QTreeWidgetItem;
class QListWidget;
class QListWidgetItem;
class QComboBox;
class QLabel;

class StatDisplay;

class CharacterMorphSelect;
class CharacterBPSummary;
class EditorTree;

class CharacterGear : public CharacterWidget {
private:
    enum ItemType {
        NoType,
        LocationType,
        MorphType,
        GearType,
        TraitContainerType,
        TraitType,
    };
private:
    Data::Character *m_character;
    EditorTree *m_gearTree;

    QLabel *m_selectionDetails;

    TraitProxy m_traitProxy;
    bool m_synchronizing = false;
public:
    CharacterGear(CharacterView *cv);

    virtual void textDump(QTextStream &stream);
    virtual void updateDisplay();
private:
    void setupGearTreeMenus();

    void updateLocation(QTreeWidgetItem *locationRoot);
    void updateMorph(QTreeWidgetItem *morphItem);
    int updateGearNode(QTreeWidgetItem *gearItem);
    void updateTraitContainer(QTreeWidgetItem *gearItem, Data::Morph *morph);
    void updateTrait(QTreeWidgetItem *gearItem);
    
    void addNewLocation();
    void addNewMorph(Data::Location *location);
    void addNewGear(Data::GearNode *node, QString filter="",
        Data::Morph *morph = nullptr);
    void addNewTrait(QTreeWidgetItem *item);

    void contextMenuDispatch(const QPoint &pos);
    void selectionChanged();
    bool gearTreeDropEvent(QDropEvent *event);
};

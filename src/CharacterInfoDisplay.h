#pragma once

#include <QVBoxLayout>
#include <QListWidget>

#include "CharacterWidget.h"

#include "data/Ego.h"
#include "data/Morph.h"
#include "data/Location.h"

class QLabel;
class QFrame;
class QTreeWidgetItem;

class EditorTree;

class CharacterInfoDisplay : public CharacterWidget {
private:
    QFrame *m_outer;
    QWidget *m_container;
    QString m_ego;
    QString m_location;
    QString m_morph;

    bool m_wantsEgo = false;
    bool m_wantsLocation = false;
    bool m_wantsMorph = false;
public:
    CharacterInfoDisplay(CharacterView *cv);

    virtual void setup(QString desc = "");

    void setEgo(QString name) { m_ego = name; }
    void setLocation(QString name) { m_location = name; }
    void setMorph(QString name) { m_morph = name; }

    void setWantsEgo() { m_wantsEgo = true; }
    void setWantsLocation() { m_wantsLocation = true; }
    void setWantsMorph() { m_wantsMorph = true; }

    virtual void textDump(QTextStream &) {}
    
    virtual void updateDisplay();
protected:
    virtual bool wantEgo() const { return m_wantsEgo; }
    virtual bool wantLocation() const { return m_wantsLocation; }
    virtual bool wantMorph() const { return m_wantsMorph; }

    virtual QWidget *container() { return m_container; }
    virtual void updateInfoDisplay(const Data::CalculatedStats &cs,
        Data::Ego *ego, Data::Location *location, Data::Morph *morph) = 0;
};

class CharacterInfoLabelDisplay : public CharacterInfoDisplay {
private:
    QLabel *m_label = nullptr;
public:
    using CharacterInfoDisplay::CharacterInfoDisplay;

    virtual void setup(QString desc = "");

protected:
    QLabel *label() const { return m_label; }
};

class CharacterInfoTemplateDisplay : public CharacterInfoLabelDisplay {
private:
    QString m_template;
public:
    using CharacterInfoLabelDisplay::CharacterInfoLabelDisplay;

    void setTemplate(QString templ) { m_template = templ; }
protected:
    virtual void updateInfoDisplay(const Data::CalculatedStats &cs,
        Data::Ego *ego, Data::Location *location, Data::Morph *morph);
};

class CharacterInfoStatDisplay : public CharacterInfoDisplay {
private:
    QStringList m_tags;
    bool m_disjunction;
    QVBoxLayout *m_layout = nullptr;
    EditorTree *m_tree = nullptr;
    bool m_onlyDeltas = false;
public:
    using CharacterInfoDisplay::CharacterInfoDisplay;

    virtual void setup(QString desc = "");

    void setTags(QStringList tags) { m_tags = tags; }
    void setDisjunction(bool d) { m_disjunction = d; }
    void setOnlyDeltas() { m_onlyDeltas = true; }
protected:
    virtual void updateInfoDisplay(const Data::CalculatedStats &cs,
        Data::Ego *ego, Data::Location *location, Data::Morph *morph);
};

class CharacterInfoMessageDisplay : public CharacterInfoDisplay {
private:
    QStringList m_tags;
    bool m_disjunction;
    EditorTree *m_list;
    QLabel *m_label;
public:
    using CharacterInfoDisplay::CharacterInfoDisplay;

    virtual void setup(QString desc = "");

    void setTags(QStringList tags) { m_tags = tags; }
    void setDisjunction(bool d) { m_disjunction = d; }
protected:
    virtual void updateInfoDisplay(const Data::CalculatedStats &cs,
        Data::Ego *ego, Data::Location *location, Data::Morph *morph);
};

#include <functional>

#include <QJsonObject>
#include <QVariant>
#include <QList>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QInputDialog>

#include "Character.h"

#include "BaseModifiers.h"
#include "State.h"

#include "data/Career.h"

namespace Data {

Character::Character(State *globalState) : m_primaryEgo(nullptr),
    m_currentLocation(nullptr), m_currentMorph(nullptr) {

    m_state = new State(globalState);
}

Character::~Character() {
    delete m_state;
}

Ego *Character::createEgo(QString name) {
    auto ego = new Ego(m_state, name);
    return m_egos[ego->name()] = ego;
}

void Character::removeEgo(QString name) {
    auto ego = m_egos[name];
    if(name == Ego::PrimaryName) {
        qWarning("Removing primary ego somehow!");
        return;
    }

    if(ego) {
        delete ego;
        m_egos.erase(name);
    }
}

void Character::newCharInit() {
    m_primaryEgo = createEgo(Ego::PrimaryName);

    // default of one flex pool point
    m_primaryEgo->fixed(Ego::Pools)->amount[m_state->getStatByName("pool:flex")] = StatValue(1);
    /* location setup */
    makeLocation("Default");
}

void Character::saveInto(pugi::xml_node node) {
    node.set_name("epc-char");
    /* first save the State object */
    m_state->saveInto(node.append_child());

    // write ego info
    for(auto ego : m_egos) {
        ego.second->saveInto(node.append_child());
    }

    // write location info
    pugi::xml_node locs = node.append_child("locations");
    for(auto location : m_locations) {
        pugi::xml_node ln = locs.append_child("location");
        ln.append_attribute("name").set_value(qUtf8Printable(location.first));

        // per-morph info
        for(auto morph : location.second->morphs()) {
            pugi::xml_node mn = ln.append_child("morph");

            mn.append_attribute("name").set_value(qUtf8Printable(morph.second->name()));
            mn.append_attribute("type").set_value(qUtf8Printable(morph.second->type()));

            // write morph trait info
            QStringList traitList;
            for(auto trait : morph.second->traits()) {
                traitList.push_back(trait);
            }
            mn.append_attribute("trait-list").set_value(qUtf8Printable(traitList.join(" ")));

            morph.second->gear.saveInto(mn.append_child());
        }

        // gear info
        location.second->gear.saveInto(ln.append_child());
    }
}

bool Character::loadFrom(pugi::xml_node node) {
    /* first, load the state. */
    m_state->loadFrom(node.child("epc-data"));

    // load the egos
    bool anyEgo = false;
    for(auto ego : node.children("ego")) {
        anyEgo = true;
        auto e = Ego::loadFrom(m_state, ego);
        m_egos[e->name()] = e;
    }

    if(!anyEgo) {
        qWarning("old-style save file with no egos! stopping load.");
        return false;
    }

    m_primaryEgo = m_egos[Ego::PrimaryName];

    for(auto location : node.child("locations").children()) {
        auto loc = new Location();
        loc->name = location.attribute("name").as_string();
        for(auto morph : location.children("morph")) {
            auto m = new Morph(
                m_state,
                morph.attribute("name").as_string(),
                morph.attribute("type").as_string(),
                QString(morph.attribute("trait-list").as_string()).split(" ", QString::SkipEmptyParts));
            if(!m->gear.loadFrom(morph.child("gear"))) return false;
            loc->addMorph(m);
        }
        auto locgear = location.child("gear");
        if(!locgear.empty()) {
            if(!loc->gear.loadFrom(locgear)) return false;
        }
        addLocation(loc);
    }

    return false;
}

void Character::createSubskill(Stat *substat, Stat *parent,
    Stat *link) const {

    Q_ASSERT(parent->name().indexOf(':') == -1);

    auto linkmod = new LinkedModifier(link, substat);
    if(substat->hasTag("specskill")) {
        m_state->addModifier("skill-spec-stage", linkmod);
    }
    else {
        m_state->addModifier("skill-field-stage", linkmod);
    }
}

void Character::makeLocation(QString name) {
    Location *location = new Location();
    location->name = name;
    addLocation(location);

    location->gear.count = 0;
    location->gear.name = "Location gear";
}

std::vector<QString> Character::locationList() const {
    std::vector<QString> result;
    for(auto location : m_locations) result.push_back(location.first);
    return result;
}

GearNode *Character::createGear(QString type, bool populate, int count) {
    GearNode *node = new GearNode();
    node->isActive = false;
    node->count = count;
    node->name = type;

    // parse fixed gear out of collection, if needed
    if(populate && !node->isContainer()) {
        auto col = m_state->getCollectionByName(type);
        for(auto fname : col->map("fixed").split(" ")) {
            if(fname == "") continue;

            auto f = createGear(fname, populate, 1);
            f->isFixed = true;
            node->children.push_back(f);
        }
        node->isPopulated = true;
    }

    return node;
}

Morph *Character::createMorphIn(Location *location, QString type, QString name) {
    Morph *morph = new Morph(m_state, name, type);

    auto col = m_state->getCollectionByName(type);

    auto gearList = col->map("gear");
    if(gearList.length() > 0) {
        auto fixedGear = new GearNode{"Built-in gear", 0};
        morph->gear.children.push_back(fixedGear);

        for(auto gear : gearList.split(" ")) {
            if(gear == "") continue;
            fixedGear->children.push_back(createGear(gear, true));
        }
        std::vector<GearNode *> st{fixedGear};
        while(st.size()) {
            auto g = st.back(); st.pop_back();
            if(g->count > 0) g->isFixed = true;
            std::copy(g->children.begin(), g->children.end(), std::back_inserter(st));
        }
    }

    m_state->getCollectionByName(type)->addToState(morph->state());

    location->addMorph(morph);

    return morph;
}

} // namespace Data

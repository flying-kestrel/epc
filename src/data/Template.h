#pragma once

#include "State.h"

namespace Data {

class Template {
private:
    State *m_state;
public:
    Template(State *state) : m_state(state) {}

    QString expand(QString string) { return expand(m_state, string); }
    static QString expand(const State *state, QString string);
    static QString expand(const State *state, const CalculatedStats &cs,
        QString string);
};

} // namespace Data

#pragma once

#include <functional>

#include <QDir>
#include "pugi/pugixml.hpp"

#include "Stat.h"
#include "BaseModifiers.h"

class QXmlStreamReader;
class QXmlStreamWriter;

namespace Data {

class State : public QObject { Q_OBJECT
public:
    typedef std::function<const State *(const State *)> StateGenerator;
    const StateGenerator wrapGenerator = [](const State *state) { return state->m_wrap; };
private:
    const State *m_wrap;

    std::map<QString, Stat *> m_stats;
    std::vector<QString> m_stages;
    std::map<QString, std::set<StatModifier *>> m_stageModifiers;
    std::map<QString, CollectionModifier *> m_collectionModifiers;

    CollectionModifier *m_currentCollection;

    std::vector<QDir> m_parseStack;
public:
    State() : m_wrap(nullptr) {}
    State(const State *wrap) : m_wrap(wrap) {}
    ~State();

    const State *wrap() const { return m_wrap; }

    // stat management
    Stat *getStatByName(const QString &name) const;
    std::vector<Stat *> getStatsByTag(const QString &tag) const;
    std::vector<Stat *> getStatsByTags(const QStringList &tags, bool disjunction) const;
    void addStat(Stat *stat);
    void removeStat(Stat *stat);

    // stat meta-data
    bool statExists(const QString &name) const;
    bool stateHasStat(Stat *stat) const;
    std::vector<Stat *> linkSources(Stat *stat) const;
    std::vector<Stat *> stateStats() const;
    std::vector<Stat *> allStats() const;

    // modifier management
    void addModifier(QString to, StatModifier *modifier);
    void removeModifier(StatModifier *modifier);

    // stage introspection
    const std::vector<QString> &stages() const;
    std::set<StatModifier *> stageModifiers(const QString &stage) const;

    // collection management
    void addCollection(CollectionModifier *modifier);
    CollectionModifier *getCollectionByName(const QString &name) const;
    std::vector<CollectionModifier *> getCollectionsByTag(const QString &tag) const;
    std::vector<CollectionModifier *> getCollectionsByTags(const std::vector<QString> &tags,
        bool disjunction) const;
    std::vector<CollectionModifier *> getCollections() const;
    bool stateHasCollection(QString name) const;
    bool collectionExists(QString name) const;
    void removeCollection(const QString &name);

    // serialization/deserialization
    bool loadFrom(const QString &filename);
    bool loadFrom(pugi::xml_node node);
    void saveInto(pugi::xml_node node);

    // calculation
    CalculatedStats calculate(
        const StateGenerator &next = [](const State *s) { return s->m_wrap; } )
        const { return calculateTo(next, "", true); }
    CalculatedStats calculateTo(const StateGenerator &next, QString category,
        bool include = true) const;
private:
    bool readNext(QXmlStreamReader &stream);
    bool readLoop(QXmlStreamReader &stream, const char *tag);

    bool parseInclude(QXmlStreamReader &stream);

    bool parseStage(QXmlStreamReader &stream);

    bool parseMap(QXmlStreamReader &stream);

    bool parseStat(QXmlStreamReader &stream);

    bool parseLink(QXmlStreamReader &stream);
    // QDomElement saveLink(LinkedModifier *link, QString stage);
    bool parseOffsets(QXmlStreamReader &stream);
    // QDomElement saveOffsets(OffsetModifier *offset, QString stage);
    bool parseCollection(QXmlStreamReader &stream);
    // QDomElement saveCollection(CollectionModifier *collection);

    bool stageExists(const QString &name) const;
    void calculateStage(const StateGenerator &next, const QString &stage,
        CalculatedStats &stats) const;
};

} // namespace Data

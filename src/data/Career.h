#pragma once

class QXmlStreamReader;
class QXmlStreamWriter;

namespace Data {

class Career {
public:
    static Career *load(QXmlStreamReader &stream);
    void save(QXmlStreamWriter &stream);
};

} // namespace Data

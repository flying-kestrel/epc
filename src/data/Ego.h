#pragma once

#include "State.h"

namespace Data {

class Career;

class Ego {
public:
    enum FixedOffset {
        Aptitudes,
        Reps,
        Pools,
        BaseSkills,
        SpecSkills
    };
    static const QString PrimaryName;
private:
    State *m_state;
    CollectionModifier *m_ego;
    QString m_name;
    Career *m_career;

    std::set<QString> m_traits;

    std::map<FixedOffset, OffsetModifier *> m_fixed;
public:
    Ego(State *charState, QString name, QStringList traits = QStringList());

    State *state() const { return m_state; }
    CollectionModifier *col() const { return m_ego; }
    QString name() const { return m_name; }
    Career *career() const { return m_career; }
    void setCareer(Career *career) { m_career = career; }

    OffsetModifier *fixed(FixedOffset which) const {
        auto it = m_fixed.find(which);
        if(it == m_fixed.end()) return nullptr;
        return it->second;
    }

    static Ego *loadFrom(State *charState, pugi::xml_node node);
    void saveInto(pugi::xml_node node);

    /* trait specifics */
    const std::set<QString> &traits() const { return m_traits; }
    bool hasTrait(QString name) { return m_traits.count(name) > 0; }
    void addTrait(QString name);
    void removeTrait(QString name);
private:
    void baseEgoInit(State *charState);
    void baseEgoLoad();
};

} // namespace Data

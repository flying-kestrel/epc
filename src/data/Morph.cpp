#include "Morph.h"

#include "State.h"

namespace Data {

Morph::Morph(State *state, QString name, QString type, QStringList traits)
    : m_name(name), m_type(type) {

    m_state = new Data::State(state);
    auto col = state->getCollectionByName(type);
    col->addToState(m_state);

    for(auto trait : traits) if(trait != "") addTrait(trait, false);

    gear.name = "Morph gear";
    gear.count = 0;

    for(auto trait : col->map("traits").split(" ")) {
        if(trait != "") addTrait(trait, true);
    }
}

void Morph::addTrait(QString trait, bool fixed) {
    auto col = m_state->getCollectionByName(trait);
    col->addToState(m_state);
    if(fixed) m_fixedTraits.insert(trait);
    else m_traits.insert(trait);
}

void Morph::removeTrait(QString trait, bool fixed) {
    auto col = m_state->getCollectionByName(trait);
    col->removeFromState(m_state);
    if(fixed) m_fixedTraits.erase(trait);
    else m_traits.erase(trait);
}

void Morph::syncGear() {
    // remove all gear collections from current state
    for(auto col : m_state->getCollections()) {
        if(!col->hasTag("gear")) continue;

        if(col->active(m_state)) col->removeFromState(m_state);
    }

    // re-add gear collections.
    gear.addTo(m_state);
}

} // namespace Data

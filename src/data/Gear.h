#pragma once

#include <set>
#include <map>

#include <QString>
#include <QMetaType>

#include "BaseModifiers.h"

namespace Data {

class Morph;

struct GearNode {
    QString name;
    int count;
    // user-specified notes
    QString note = "";
    // name of ego this gear creates
    QString egoName = "";
    // "fixed" gear cannot be removed
    bool isFixed = false;
    // has fixed gear been populated?
    bool isPopulated = false;
    // "active" gear: terminators that are enabled
    bool isActive = false;
    bool isContainer() const { return count == 0; }

    std::vector<GearNode *> children = std::vector<GearNode *>();

    void changeActive(State *state, bool activate);
    void addTo(State *state);
    void removeFrom(State *state);

    bool loadFrom(pugi::xml_node node);
    void saveInto(pugi::xml_node node);
};

} // namespace Data

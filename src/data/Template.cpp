#include <QRegularExpression>

#include "Template.h"

namespace Data {

QString Template::expand(const State *state, QString string) {
    auto cs = state->calculate();
    return expand(state, cs, string);
}

QString Template::expand(const State *state, const CalculatedStats &cs,
    QString string) {

    QRegularExpression tRE("\\${([^}]*)}");

    while(true) {
        auto match = tRE.match(string);
        if(!match.hasMatch()) break;
        int from = match.capturedStart(0);
        int len = match.capturedLength(0);

        auto stat = state->getStatByName(match.captured(1));

        auto it = cs.values.find(stat);
        if(it != cs.values.end()) {
            string = string.replace(from, len, it->second.format());
        }
        else {
            string.replace(from, len, "(0)");
        }
    }
    
    return string;
}

} // namespace Data

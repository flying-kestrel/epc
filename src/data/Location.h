#pragma once

#include <map>

#include <QString>

#include "Gear.h"
#include "Morph.h"

namespace Data {

class Location {
private:
    std::map<QString, Morph *> m_morphs;
public:
    Location();
    QString name;
    GearNode gear;

    const std::map<QString, Morph *> &morphs() const { return m_morphs; }
    void addMorph(Morph *morph) { m_morphs[morph->name()] = morph; }
    void removeMorph(QString name) { m_morphs.erase(name); }

    void used(Data::State *charState, int &gp, int &mp);
};

} // namespace Data

#pragma once

#include <set>
#include <map>

#include "Stat.h"
#include "BaseModifiers.h"
#include "Gear.h"
#include "Morph.h"
#include "Ego.h"
#include "Location.h"

class QXmlStreamReader;
class QXmlStreamWriter;

namespace Data {

class State;

class Character {
public:
private:
    State *m_state;
    Ego *m_primaryEgo;

    std::map<QString, Location *> m_locations;
    std::map<QString, Ego *> m_egos;

    Location *m_currentLocation;
    Morph *m_currentMorph;
private:
    bool m_egoRefactor = false;
public:
    Character(State *globalState);
    ~Character();

    State *state() const { return m_state; }
    State *egoState() const { return m_primaryEgo->state(); }

    Ego *primaryEgo() const { return m_primaryEgo; }
    Ego *createEgo(QString name);
    Ego *ego(QString name) const { return m_egos.find(name)->second; }
    const std::map<QString, Ego *> &egoMap() const { return m_egos; }
    void removeEgo(QString name);

    void newCharInit();
    void loadCharInit();

    bool loadFrom(pugi::xml_node node);
    void saveInto(pugi::xml_node node);

    /* specific getters */
    Data::OffsetModifier *baseAptitudes() const
        { return m_primaryEgo->fixed(Ego::Aptitudes); }
    Data::OffsetModifier *baseReps() const
        { return m_primaryEgo->fixed(Ego::Reps); }
    Data::OffsetModifier *basePools() const
        { return m_primaryEgo->fixed(Ego::Pools); }
    Data::OffsetModifier *baseSkills() const
        { return m_primaryEgo->fixed(Ego::BaseSkills); }
    Data::OffsetModifier *specSkills() const
        { return m_primaryEgo->fixed(Ego::SpecSkills); }

    /* subskill specifics */
    void createSubskill(Stat *substat, Stat *parent,
        Stat *source) const;

    /* trait specifics */
    const std::set<QString> &traits() const { return m_primaryEgo->traits(); }
    bool hasTrait(QString name)
        { return m_primaryEgo->hasTrait(name) || (m_currentMorph && m_currentMorph->hasTrait(name)); }
    void addTrait(QString name) { m_primaryEgo->addTrait(name); }
    void removeTrait(QString name) { m_primaryEgo->removeTrait(name); }

    /* location/morph specifics */
    Location *location(QString name) const
        { auto it = m_locations.find(name); if(it == m_locations.end()) return nullptr; else return it->second; }
    void makeLocation(QString name);
    void addLocation(Location *location)
        { m_locations[location->name] = location; }
    void removeLocation(Location *location)
        { m_locations.erase(location->name); }
    std::vector<QString> locationList() const;

    Location *currentLocation() const { return m_currentLocation; }

    GearNode *createGear(QString type, bool populate, int count=1);
    Morph *createMorphIn(Location *location, QString type, QString name);
};

} // namespace Data

#pragma once

#include <QStringList>

#include "Gear.h"

namespace Data {

class Morph {
private:
    State *m_state;
    std::set<QString> m_traits;
    std::set<QString> m_fixedTraits;
    QString m_name, m_type;
public:
    Morph(State *charState, QString name, QString type,
        QStringList traits = QStringList());

    State *state() const { return m_state; }

    const QString &name() const { return m_name; }
    const QString &type() const { return m_type; }

    const std::set<QString> &traits() const { return m_traits; }
    void addTrait(QString trait, bool fixed);
    void removeTrait(QString trait, bool fixed);
    bool hasTrait(QString trait) const
        { return m_traits.find(trait) != m_traits.end()
            || m_fixedTraits.find(trait) != m_fixedTraits.end(); }

    GearNode gear;

    void syncGear();
};

} // namespace Data

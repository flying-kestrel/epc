#include <cassert>
#include <QtDebug>

#include "BaseModifiers.h"
#include "State.h"

namespace Data {

LinkedModifier::LinkedModifier(Stat *source, Stat *target, double multiplier)
    : m_source(source), m_target(target), m_multiplier(multiplier) {

    if(!m_source) qFatal("NULL source for LinkedModifier");
    if(!m_target) qFatal("NULL target for LinkedModifier");
}

void LinkedModifier::mutate(CalculatedStats &stats) {
    /*qDebug("LinkedModifier mutating %s with value from %s...",
        m_target->name().toStdString().c_str(),
        m_source->name().toStdString().c_str());*/
    auto offset = stats.values[m_source] * m_multiplier;
    stats.values[m_target] += offset;
    stats.derivation[m_target] +=
        " + " + offset.format() + " (" + m_source->shortDesc() + " link)";
}

LinkedModifier *LinkedModifier::loadFrom(State *state, pugi::xml_node node) {
    auto desc = node.attribute("desc").as_string();

    auto source = state->getStatByName(node.attribute("source").as_string());
    auto target = state->getStatByName(node.attribute("target").as_string());

    auto ret = new LinkedModifier(
        source, target, node.attribute("multiplier").as_double(1.0));
    ret->changeDesc(desc);

    return ret;
}

void LinkedModifier::saveInto(pugi::xml_node node) {
    node.set_name("link");
    node.append_attribute("desc").set_value(qUtf8Printable(desc()));
    node.append_attribute("source").set_value(qUtf8Printable(m_source->name()));
    node.append_attribute("target").set_value(qUtf8Printable(m_target->name()));
    node.append_attribute("multiplier").set_value(m_multiplier);
}

OffsetModifier *OffsetModifier::loadFrom(State *state, pugi::xml_node node) {
    auto desc = node.attribute("desc").as_string();
    auto ret = new OffsetModifier(desc);

    for(auto offset : node.children("offset")) {
        auto name = offset.attribute("stat").as_string();
        ret->amount[state->getStatByName(name)]
            = StatValue::parse(offset.attribute("value").as_string());
    }

    return ret;
}

void OffsetModifier::saveInto(pugi::xml_node node) {
    node.set_name("offsets");
    node.append_attribute("desc").set_value(qUtf8Printable(desc()));

    for(auto a : amount) {
        auto offset = node.append_child("offset");
        offset.append_attribute("stat").set_value(qUtf8Printable(a.first->name()));
        offset.append_attribute("value").set_value(qUtf8Printable(a.second.format()));
    }
}

void CollectionModifier::removeFromCollection(StatModifier *modifier) {
    for(unsigned i = 0; i < m_collection.size(); i ++) {
        if(m_collection[i].second == modifier) {
            m_collection.erase(m_collection.begin() + i);
            return;
        }
    }
}

void CollectionModifier::addToState(State *state) {
    Q_ASSERT(m_activeIn.count(state) == 0);
    for(auto x : m_collection) {
        state->addModifier(x.first, x.second);
    }
    m_activeIn.insert(state);
}

void CollectionModifier::removeFromState(State *state) {
    Q_ASSERT(m_activeIn.count(state) > 0);
    for(auto x : m_collection) {
        state->removeModifier(x.second);
    }
    m_activeIn.erase(state);
}

CollectionModifier *CollectionModifier::loadFrom(Data::State *state,
    pugi::xml_node node) {

    auto name = node.attribute("name").as_string();
    auto desc = node.attribute("desc").as_string();

    auto ret = new CollectionModifier(name, desc);

    auto tags = node.attribute("tags").as_string();
    for(auto tag : QString(tags).split(" ")) {
        if(tag == "") continue;
        ret->addTag(tag);
    }

    for(auto map : node.children("map")) {
        ret->m_map[map.attribute("from").as_string()]
            = map.attribute("value").as_string();
    }

    for(auto link : node.children("link")) {
        ret->m_collection.push_back(std::make_pair(
            link.attribute("stage").as_string(),
            LinkedModifier::loadFrom(state, link)));
    }
    for(auto offset : node.children("offsets")) {
        ret->m_collection.push_back(std::make_pair(
            offset.attribute("stage").as_string(),
            OffsetModifier::loadFrom(state, offset)));
    }

    return ret;
}

void CollectionModifier::saveInto(pugi::xml_node node) {
    node.set_name("collection");
    node.append_attribute("name").set_value(qUtf8Printable(m_name));
    node.append_attribute("desc").set_value(qUtf8Printable(m_desc));

    QStringList tags;
    for(auto tag : m_tags) {
        tags << tag;
    }
    node.append_attribute("tags").set_value(qUtf8Printable(tags.join(" ")));

    for(auto map : m_map) {
        auto mn = node.append_child("map");
        mn.append_attribute("from").set_value(qUtf8Printable(map.first));
        mn.append_attribute("value").set_value(qUtf8Printable(map.second));
    }

    for(auto element : m_collection) {
        pugi::xml_node n = node.append_child();
        if(auto link = dynamic_cast<LinkedModifier *>(element.second)) {
            link->saveInto(n);
        }
        else if(auto offset = dynamic_cast<OffsetModifier *>(element.second)) {
            offset->saveInto(n);
        }
        else Q_ASSERT(false);

        n.append_attribute("stage").set_value(qUtf8Printable(element.first));
    }
}

} // namespace Data

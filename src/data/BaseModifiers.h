#pragma once

#include "Stat.h"

namespace Data {

class State;

class LinkedModifier : public StatModifier {
private:
    Stat *m_source, *m_target;
    double m_multiplier;
public:
    LinkedModifier(Stat *source, Stat *target, double multiplier = 1);

    Stat *source() const { return m_source; }
    Stat *target() const { return m_target; }
    double multiplier() const { return m_multiplier; }

    virtual void mutate(CalculatedStats &stats);

    static LinkedModifier *loadFrom(State *state, pugi::xml_node node);
    void saveInto(pugi::xml_node node);
};

class OffsetModifier : public StatModifier {
public:
    using StatModifier::StatModifier;
    StatMap amount;

    virtual void mutate(CalculatedStats &stats) {
        for(auto &i : amount) {
            stats.values[i.first] += i.second;
            stats.derivation[i.first] += " + " + i.second.format() + " (" + m_desc + ")";
        }
    }

    static OffsetModifier *loadFrom(State *state, pugi::xml_node node);
    void saveInto(pugi::xml_node node);
};


class CollectionModifier {
private:
    std::vector<std::pair<QString, StatModifier *>> m_collection;
    QString m_name, m_desc;
    std::set<QString> m_tags;
    std::map<QString, QString> m_map;
    std::set<const State *> m_activeIn;
public:
    CollectionModifier(QString name, QString desc)
        : m_name(name), m_desc(desc) {}

    const QString &name() const { return m_name; }
    void rename(QString name) { m_name = name; }
    bool hasTag(const QString &tag) const { return m_tags.count(tag); }
    void addTag(const QString &tag) { m_tags.insert(tag); }
    void removeTag(const QString &tag) { m_tags.erase(tag); }
    const std::set<QString> &tags() const { return m_tags; }

    const QString &desc() const { return m_desc; }
    void changeDesc(QString desc) { m_desc = desc; }

    const std::vector<std::pair<QString, StatModifier *>> &collection() const { return m_collection; }
    void addToCollection(QString stage, StatModifier *modifier)
        { m_collection.push_back(std::make_pair(stage, modifier)); }
    void removeFromCollection(StatModifier *modifier);

    void addToState(State *state);
    void removeFromState(State *state);
    bool active(const State *state) const { return m_activeIn.count(state) > 0; }

    const std::map<QString, QString> &getMap() const { return m_map; }
    QString map(QString from) { return m_map[from]; }
    void setMap(QString from, QString to) { m_map[from] = to; }
    void clearMap(QString from) { m_map.erase(from); }

    static CollectionModifier *loadFrom(Data::State *state, pugi::xml_node node);
    void saveInto(pugi::xml_node node);
};

} // namespace Data

#include "Ego.h"
#include "State.h"

namespace Data {

const QString Ego::PrimaryName = "Primary";

Ego::Ego(State *charState, QString name, QStringList traits) : m_name(name) {
    m_state = new Data::State(charState);
    if(m_state->collectionExists("ego:" + name)) {
        baseEgoLoad();
    }
    else baseEgoInit(charState);

    for(auto trait : traits) if(trait != "") addTrait(trait);
}

Ego *Ego::loadFrom(State *charState, pugi::xml_node node) {
    auto traits = QString(node.attribute("trait-list").as_string())
        .split(" ", QString::SkipEmptyParts);
    return new Ego(charState, node.attribute("name").as_string(), traits);
}

void Ego::saveInto(pugi::xml_node node) {
    node.set_name("ego");
    node.append_attribute("name").set_value(qUtf8Printable(m_name));

    QStringList traitList;
    for(auto trait : m_traits) {
        traitList.push_back(trait);
    }
    node.append_attribute("trait-list").set_value(qUtf8Printable(traitList.join(" ")));
}

void Ego::addTrait(QString name) {
    auto collection = m_state->getCollectionByName(name);
    Q_ASSERT(collection);

    Q_ASSERT(!collection->hasTag("trait:morph"));

    m_traits.insert(name);

    collection->addToState(m_state);
}

void Ego::removeTrait(QString name) {
    if(m_traits.count(name)) {
        m_traits.erase(name);
    }
    else {
        qDebug("Couldn't find trait \"%s\"",
            name.toStdString().c_str());
        return;
    }

    auto collection = m_state->getCollectionByName(name);
    collection->removeFromState(m_state);
}

void Ego::baseEgoInit(State *charState) {
    m_ego = new CollectionModifier("ego:" + m_name, m_name + " ego");
    m_ego->addTag("ego");
    charState->addCollection(m_ego);

    auto makeFixed = [=](const char *stage) {
        auto mod = new Data::OffsetModifier("Fixed offsets");
        m_ego->addToCollection(stage, mod);
        return mod;
    };

    m_fixed[Aptitudes] = makeFixed("aptitude-stage");
    m_fixed[Reps] = makeFixed("rep-stage");
    m_fixed[Pools] = makeFixed("misc-stage");
    m_fixed[BaseSkills] = makeFixed("skill-stage");
    m_fixed[SpecSkills] = makeFixed("skill-spec-stage");

    /* aptitude setup */
    auto aptitudes = m_state->getStatsByTag("aptitude");
    for(auto apt : aptitudes) m_fixed[Aptitudes]->amount[apt] = StatValue(0);

    /* rep setup */
    auto reps = m_state->getStatsByTag("rep");
    for(auto rep : reps) m_fixed[Reps]->amount[rep] = StatValue(0);

    /* skill setup */
    auto skills = m_state->getStatsByTag("skill");
    for(auto skill : skills) {
        if(!skill->hasTag("subskill")) {
            m_fixed[BaseSkills]->amount[skill] = StatValue(0);
        }
    }

    m_ego->addToState(m_state);
}

void Ego::baseEgoLoad() {
    m_ego = m_state->getCollectionByName("ego:" + m_name);

    auto loadFixed = [&](FixedOffset fixed, const char *stage) {
        for(auto m : m_ego->collection()) {
            if(m.first == stage) {
                m_fixed[fixed] = static_cast<OffsetModifier *>(m.second);
                return;
            }
        }
        qCritical("Couldn't find FixedOffset for stage %s!", stage);
    };

    loadFixed(Aptitudes, "aptitude-stage");
    loadFixed(Reps, "rep-stage");
    loadFixed(Pools, "misc-stage");
    loadFixed(BaseSkills, "skill-stage");
    loadFixed(SpecSkills, "skill-spec-stage");

    // traits
    for(auto trait : m_ego->map("traits").split(" ", QString::SkipEmptyParts)) {
        addTrait(trait);
    }

    m_ego->addToState(m_state);
}

} // namespace Data

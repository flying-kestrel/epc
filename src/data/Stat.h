#pragma once

#include <QString>
#include <map>
#include <vector>
#include <set>

#include "pugi/pugixml.hpp"

namespace Data {

class StatValue {
public:
    static const int maxDice = 21;
    enum DVType {
        NoDV,
        Stress,
        Damage
    };
private:
    double m_dice[maxDice];
    DVType m_dv;
public:
    StatValue() {
        m_dv = NoDV;
        for(int i = 0; i < maxDice; i ++) m_dice[i] = 0;
    }

    StatValue(double constant) : StatValue() {
        m_dice[0] = constant;
    }

    StatValue(int nd, int size, double constant = 0) : StatValue() {
        m_dice[size] = nd;
        m_dice[0] = constant;
    }

    double die(int index) const { return m_dice[index]; }

    StatValue operator+(const StatValue &other) const {
        StatValue ret;
        for(int i = 0; i < maxDice; i ++) {
            ret.m_dice[i] = m_dice[i] + other.m_dice[i];
        }

        ret.m_dv = m_dv;
        if(other.m_dv != m_dv) {
            qDebug("type disagree!");
        }
        return ret;
    }

    StatValue operator-(const StatValue &other) const {
        StatValue ret;
        for(int i = 0; i < maxDice; i ++) {
            ret.m_dice[i] = m_dice[i] - other.m_dice[i];
        }
        return ret;
    }

    StatValue &operator+=(const StatValue &other) {
        for(int i = 0; i < maxDice; i ++) {
            m_dice[i] += other.m_dice[i];
        }

        if(other.m_dv != m_dv && m_dv == NoDV) {
            m_dv = other.m_dv;
        }

        return *this;
    }

    StatValue operator*(double by) const {
        StatValue ret;
        for(int i = 0; i < maxDice; i ++) {
            ret.m_dice[i] = m_dice[i] * by;
        }
        ret.m_dv = m_dv;
        return ret;
    }

    QString format() const;
    static StatValue parse(QString from);

    bool operator==(const StatValue &other) const {
        for(int i = 0; i < maxDice; i ++) {
            if(other.m_dice[i] != m_dice[i]) return false;
        }
        return m_dv == other.m_dv;
    }

    bool operator!=(const StatValue &other) const {
        return !(*this == other);
    }
};

class Stat {
private:
    QString m_name;
    QString m_shortDesc;
    QString m_longDesc;
    std::set<QString> m_tags;
public:
    Stat(const QString &name, const QString &shortDesc,
        const QString &longDesc)
        : m_name(name), m_shortDesc(shortDesc), m_longDesc(longDesc) {}

    const QString &name() const { return m_name; }
    const QString &shortDesc() const { return m_shortDesc; }
    const QString &longDesc() const { return m_longDesc; }

    bool hasTag(const QString &tag) const { return m_tags.count(tag); }
    void addTag(const QString &tag) { m_tags.insert(tag); }
    const std::set<QString> &tags() const { return m_tags; }

    bool operator<(const Stat &other) const { return m_name < other.m_name; }
    bool operator==(const Stat &other) const { return m_name == other.m_name; }
    bool operator!=(const Stat &other) const { return m_name != other.m_name; }

    static Stat *loadFrom(pugi::xml_node node);
    void saveInto(pugi::xml_node node);
};

typedef std::map<Stat *, StatValue> StatMap;

struct CalculatedStats {
    StatMap values;
    std::map<Stat *, QString> derivation;
};

class StatModifier {
protected:
    QString m_desc;
public:
    StatModifier(QString desc = "") : m_desc(desc) {}
    virtual ~StatModifier() {}

    const QString &desc() const { return m_desc; }
    void changeDesc(const QString &desc) { m_desc = desc; }

    virtual void mutate(CalculatedStats &stats) = 0;
};

} // namespace Data

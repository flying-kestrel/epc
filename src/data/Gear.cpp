#include "Gear.h"

#include "State.h"

#include "moc_Gear.cpp"

namespace Data {

void GearNode::changeActive(State *state, bool activate) {
    bool isTerminator = false;
    if(count > 0) {
        auto col = state->getCollectionByName(name);
        isTerminator = col->hasTag("gear:terminator");
    }

    // set isActive, if relevant
    isActive |= activate && isTerminator;

    std::function<void (GearNode *)> rv = [&](GearNode *node) {
        if(node->count > 0) {
            auto col = state->getCollectionByName(node->name);
            if(col->hasTag("gear:terminator") && !node->isActive) {
                return;
            }
            if(activate) col->addToState(state);
            else col->removeFromState(state);
        }
        for(auto ch : node->children) {
            rv(ch);
        }
    };

    rv(this);

    // clear isActive
    isActive &= activate;
}

void GearNode::addTo(State *state) {
    if(count > 0) {
        auto col = state->getCollectionByName(name);
        if(col->hasTag("gear:terminator") && !isActive) {
            return;
        }
        col->addToState(state);
    }
    for(auto ch : children) {
        ch->addTo(state);
    }
}

void GearNode::removeFrom(State *state) {
    if(count > 0) {
        auto col = state->getCollectionByName(name);
        if(col->hasTag("gear:terminator") && !isActive) {
            return;
        }
        col->removeFromState(state);
    }
    for(auto ch : children) {
        ch->removeFrom(state);
    }
}

bool GearNode::loadFrom(pugi::xml_node node) {
    name = node.attribute("name").as_string();
    count = node.attribute("count").as_int(1);
    note = node.attribute("note").as_string();
    isActive = node.attribute("active").as_bool();
    isFixed = node.attribute("fixed").as_bool();

    for(auto gear : node.children("gear")) {
        auto child = new GearNode();
        if(!child->loadFrom(gear)) return false;
        children.push_back(child);
    }
    return true;
}

void GearNode::saveInto(pugi::xml_node node) {
    node.set_name("gear");

    node.append_attribute("name").set_value(qUtf8Printable(name));
    node.append_attribute("count").set_value(count);

    if(note.length() > 0) {
        node.append_attribute("note").set_value(qUtf8Printable(note));
    }
    if(isActive) node.append_attribute("active").set_value(true);
    if(isFixed) node.append_attribute("fixed").set_value(true);

    for(auto c : children) {
        c->saveInto(node.append_child());
    }
}

} // namespace Data

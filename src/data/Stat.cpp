#include <QStringList>

#include "Stat.h"

namespace Data {

QString StatValue::format() const {
    QString result;
    bool first = true;
    for(int i = maxDice-1; i > 0; i --) {
        if(m_dice[i] == 0) continue;
        if(first) first = false;
        else result += " + ";
        result += QString::number(m_dice[i]) + "d" + QString::number(i);
    }

    /* round down when formatting the constant. */
    if(m_dice[0] != 0) {
        if(first) first = false;
        else result += " + ";

        result += QString::number((int)m_dice[0]);
    }

    if(result == "") result = "0";

    if(m_dv == Stress) result = "SV " + result;
    else if(m_dv == Damage) {
        result = "DV " + result;
    }

    return result;
}

StatValue StatValue::parse(QString from) {
    StatValue result;

    if(from.startsWith("SV ")) {
        result.m_dv = Stress;
        from = from.mid(3);
    }
    else if(from.startsWith("DV ")) {
        result.m_dv = Damage;
        from = from.mid(3);
    }

    QStringList ds = from.split("+");
    for(auto s : ds) {
        if(s.indexOf("d") == -1) {
            result.m_dice[0] = s.toInt();
        }
        else {
            QStringList sl = s.split("d");
            result.m_dice[sl[1].toInt()] = sl[0].toInt();
        }
    }

    return result;
}

Stat *Stat::loadFrom(pugi::xml_node node) {
    auto name = node.attribute("name").as_string();
    auto shortDesc = node.attribute("short-desc").as_string();
    auto longDesc = node.attribute("long-desc").as_string(nullptr);
    auto tags = node.attribute("tags").as_string();

    if(!longDesc) {
        longDesc = shortDesc;
    }

    auto ret = new Stat(name, shortDesc, longDesc);

    for(auto tag : QString(tags).split(" ", QString::SkipEmptyParts)) {
        ret->addTag(tag);
    }

    return ret;
}

void Stat::saveInto(pugi::xml_node node) {
    node.set_name("stat");
    node.append_attribute("name").set_value(qUtf8Printable(m_name));
    node.append_attribute("short-desc").set_value(qUtf8Printable(m_shortDesc));
    node.append_attribute("long-desc").set_value(qUtf8Printable(m_longDesc));

    QStringList tags;
    for(auto tag : m_tags) {
        tags << tag;
    }
    node.append_attribute("tags").set_value(qUtf8Printable(tags.join(" ")));
}

} // namespace Data

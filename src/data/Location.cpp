#include <functional>

#include "Location.h"

#include "State.h"

namespace Data {

Location::Location() {
    m_morphs.clear();
}

void Location::used(Data::State *charState, int &gpUsed, int &mpUsed) {
    gpUsed = mpUsed = 0;

    std::function<void (Data::GearNode *)> sum = [&](Data::GearNode *node) {
        if(!node->isContainer() && !node->isFixed) {
            auto col = charState->getCollectionByName(node->name);
            gpUsed += col->map("cost").toInt();
        }
        for(auto c : node->children) sum(c);
    };

    sum(&gear);

    auto mpUsedStat = charState->getStatByName("mp:used");
    int morphCount = 0;
    int zeroCostMorphs = 0;
    for(auto m : m_morphs) {
        auto col = charState->getCollectionByName(m.second->type());
        int cost = col->map("cost").toInt();
        bool isMorph = !col->hasTag("morph:unsleevable");
        if(isMorph) morphCount ++;
        if(isMorph && cost == 0) zeroCostMorphs ++;
        mpUsed += cost;

        sum(&m.second->gear);

        auto cs = m.second->state()->calculate();
        mpUsed += cs.values[mpUsedStat].die(0);

        // compensate for fixed traits
        for(auto trait : col->map("traits").split(" ")) {
            if(trait == "") continue;
            auto tcol = charState->getCollectionByName(trait);
            Data::State st(charState);
            tcol->addToState(&st);
            auto cs = st.calculate();
            int amount = cs.values[charState->getStatByName("mp:used")].die(0);
            mpUsed -= amount;
            tcol->removeFromState(&st);
        }
    }
    if(morphCount > 1) mpUsed += zeroCostMorphs;
}

} // namespace Data

#include <functional>

#include <QDebug>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QFileInfo>

#include "State.h"
#include "BaseModifiers.h"

#include "moc_State.cpp"

namespace Data {

State::~State() {
    for(auto stat : m_stats) delete stat.second;
}

Stat *State::getStatByName(const QString &name) const {
    auto it = m_stats.find(name);
    if(it != m_stats.end()) {
        if(!it->second) qFatal("Expected stat mapping to be non-null");
        return it->second;
    }
    else if(m_wrap) {
        // qDebug("Checking m_wrap for \"%s\"", name.toStdString().c_str());
        return m_wrap->getStatByName(name);
    }
    else {
        qFatal("No stat \"%s\" found!", name.toStdString().c_str());
        return nullptr;
    }
}

std::vector<Stat *> State::getStatsByTag(const QString &tag) const {
    std::vector<Stat *> result;
    if(m_wrap) result = m_wrap->getStatsByTag(tag);

    for(auto stat : m_stats) {
        if(stat.second && stat.second->hasTag(tag)) {
            result.push_back(stat.second);
        }
    }
    return result;
}

std::vector<Stat *> State::getStatsByTags(const QStringList &tags,
    bool disjunction) const {

    std::vector<Stat *> result;
    if(m_wrap) result = m_wrap->getStatsByTags(tags, disjunction);

    for(auto stat : m_stats) {
        if(!stat.second) continue;
        bool ok = !disjunction;
        if(disjunction && tags.size()) for(auto tag : tags) ok |= stat.second->hasTag(tag);
        else if(disjunction) ok = true;
        else for(auto tag : tags) ok &= stat.second->hasTag(tag);

        if(ok) result.push_back(stat.second);
    }

    return result;
}

void State::addStat(Stat *stat) {
    m_stats[stat->name()] = stat;
}

void State::removeStat(Stat *stat) {
    Q_ASSERT(m_stats.count(stat->name()) == 1);
    m_stats.erase(stat->name());
}

bool State::statExists(const QString &name) const {
    if(m_stats.count(name)) return true;
    else if(m_wrap) return m_wrap->statExists(name);
    else return false;
}

bool State::stateHasStat(Stat *stat) const {
    return stat && m_stats.count(stat->name()) > 0;
}

std::vector<Stat *> State::linkSources(Stat *stat) const {
    std::vector<Stat *> result;
    if(m_wrap) result = m_wrap->linkSources(stat);
    for(auto st : m_stageModifiers) {
        for(auto mod : st.second) {
            LinkedModifier *link = dynamic_cast<LinkedModifier *>(mod);
            if(!link) continue;
            if(link->target() == stat) result.push_back(link->source());
        }
    }

    return result;
}

std::vector<Stat *> State::stateStats() const {
    std::vector<Stat *> ret;
    for(auto x : m_stats) ret.push_back(x.second);
    return ret;
}

std::vector<Stat *> State::allStats() const {
    std::vector<Stat *> ret;
    if(m_wrap) ret = m_wrap->allStats();
    for(auto x : m_stats) ret.push_back(x.second);
    return ret;
}

void State::addModifier(QString to, StatModifier *modifier) {
    if(modifier == nullptr) {
        qFatal("NULL modifier being added to state!");
    }
    if(!stageExists(to)) {
        qDebug("Trying to add modifier to non-existent stage \"%s\"!",
            to.toStdString().c_str());
        return;
    }
    m_stageModifiers[to].insert(modifier);
    // qDebug("Added %p modifier to stage \"%s\"...", modifier, to.toStdString().c_str());
}

void State::removeModifier(StatModifier *modifier) {
    for(auto &x : m_stageModifiers) {
        if(x.second.count(modifier)) {
            // qDebug("Removing modifier %p from stage \"%s\"...", modifier, x.first.toStdString().c_str());
            x.second.erase(modifier);
        }
    }
}

const std::vector<QString> &State::stages() const {
    if(m_wrap) return m_wrap->stages();
    return m_stages;
}

std::set<StatModifier *> State::stageModifiers(const QString &stage) const {
    std::set<StatModifier *> result;
    if(m_wrap) result = m_wrap->stageModifiers(stage);
    auto it = m_stageModifiers.find(stage);
    if(it != m_stageModifiers.end()) {
        result.insert(it->second.begin(), it->second.end());
    }
    return result;
}

void State::addCollection(CollectionModifier *modifier) {
    m_collectionModifiers[modifier->name()] = modifier;
}

CollectionModifier *State::getCollectionByName(const QString &name) const {
    auto it = m_collectionModifiers.find(name);
    if(it != m_collectionModifiers.end()) return it->second;
    else if(m_wrap) {
        return m_wrap->getCollectionByName(name);
    }
    else {
        qDebug("Failed to find collection \"%s\" by name!",
            name.toStdString().c_str());
        return nullptr;
    }
}

std::vector<CollectionModifier *> State::getCollectionsByTag(const QString &tag) const {
    std::vector<CollectionModifier *> result;
    if(m_wrap) result = m_wrap->getCollectionsByTag(tag);

    for(auto mod : m_collectionModifiers) {
        if(mod.second && mod.second->hasTag(tag)) {
            result.push_back(mod.second);
        }
    }

    return result;
}

std::vector<CollectionModifier *> State::getCollectionsByTags(
    const std::vector<QString> &tags, bool disjunction) const {

    std::vector<CollectionModifier *> result;
    if(m_wrap) result = m_wrap->getCollectionsByTags(tags, disjunction);

    for(auto mod : m_collectionModifiers) {
        if(!mod.second) continue;
        bool ok = !disjunction;
        if(disjunction) for(auto tag : tags) ok |= mod.second->hasTag(tag);
        else for(auto tag : tags) ok &= mod.second->hasTag(tag);

        if(ok) result.push_back(mod.second);
    }

    return result;
}

std::vector<CollectionModifier *> State::getCollections() const {
    std::vector<CollectionModifier *> result;
    if(m_wrap) result = m_wrap->getCollections();

    for(auto mod : m_collectionModifiers) {
        result.push_back(mod.second);
    }

    return result;
}

bool State::stateHasCollection(QString name) const {
    return m_collectionModifiers.count(name) > 0;
}

bool State::collectionExists(QString name) const {
    return stateHasCollection(name)
        || (m_wrap && m_wrap->collectionExists(name));
}

void State::removeCollection(const QString &name) {
    m_collectionModifiers.erase(name);
}

bool State::loadFrom(const QString &filename) {
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug("Couldn't open \"%s\"...", filename.toStdString().c_str());
        return false;
    }

    pugi::xml_document doc;
    auto presult = doc.load_file(qUtf8Printable(filename));
    if(presult.status != pugi::xml_parse_status::status_ok) {
        qDebug("failed to parse file \"%s\": %s", qUtf8Printable(filename), presult.description());
        return false;
    }
    
    m_parseStack.push_back(QFileInfo(file).absoluteDir());

    bool result = loadFrom(doc.document_element());

    m_parseStack.pop_back();

    return result;
}

bool State::loadFrom(pugi::xml_node node) {
    // first, stages
    for(auto stage : node.children("stage")) {
        if(m_wrap) {
            qWarning("stage definition in non-root State!");
            return false;
        }
        auto name = stage.attribute("name").as_string(nullptr);
        if(!name) {
            qWarning("empty stage name!");
            continue;
        }
        m_stageModifiers[name] = std::set<StatModifier *>();
        m_stages.push_back(name);
    }

    // now load all the stats
    for(auto n : node.children("stat")) {
        auto stat = Stat::loadFrom(n);
        if(stat) addStat(stat);
    }

    // now all the includes
    for(auto inc : node.children("include")) {
        auto path = inc.attribute("path").as_string();
        bool ret = loadFrom(m_parseStack.back().absoluteFilePath(path));
        if(!ret) return false;
    }

    // then all the collections
    for(auto n : node.children("collection")) {
        auto col = CollectionModifier::loadFrom(this, n);
        m_collectionModifiers[col->name()] = col;
    }

    // and finally all the bare offsets/links
    for(auto n : node.children("link")) {
        auto off = LinkedModifier::loadFrom(this, n);
        addModifier(n.attribute("stage").as_string(nullptr), off);
    }
    for(auto n : node.children("offsets")) {
        auto off = OffsetModifier::loadFrom(this, n);
        addModifier(n.attribute("stage").as_string(nullptr), off);
    }

    return true;
}

void State::saveInto(pugi::xml_node root) {
    root.set_name("epc-data");
    // first stats
    for(auto s : m_stats) {
        s.second->saveInto(root.append_child());
    }
    // then collections
    for(auto c : m_collectionModifiers) {
        c.second->saveInto(root.append_child());
    }

    // modifiers are all managed at the Character level, or already loaded
    // from data files
}

CalculatedStats State::calculateTo(const StateGenerator &next,
    QString category, bool include) const {

    CalculatedStats st;
    // qDebug("beginning calculation");
    for(auto &c : stages()) {
        if(c == category && !include) break;

        calculateStage(next, c, st);

        /*qDebug("    calculateStage(%s) results:", c.toStdString().c_str());
        for(auto x : st.values) {
            qDebug("        %s = %s", x.first->name().toStdString().c_str(),
                x.second.format().toStdString().c_str());
        }*/

        if(c == category) break;
    }
    return st;
}

bool State::stageExists(const QString &name) const {
    if(m_stageModifiers.count(name)) return true;
    if(!m_wrap) return false;
    else return m_wrap->stageExists(name);
}

void State::calculateStage(const StateGenerator &next, const QString &stage,
    CalculatedStats &stats) const {

    // qDebug("calculateStage(this=%p, stage=%s)", this, qPrintable(stage));

    if(auto w = next(this)) w->calculateStage(next, stage, stats);

    auto st = m_stageModifiers.find(stage);
    if(st == m_stageModifiers.end()) return;
    for(auto &mod : st->second) {
        // qDebug("Applying modifier %p (\"%s\")", mod, mod->desc().toStdString().c_str());
        mod->mutate(stats);
    }
}

} // namespace Data

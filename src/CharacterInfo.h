#pragma once

#include <QVBoxLayout>

#include "CharacterWidget.h"

class CharacterInfoDisplay;

class CharacterInfo : public CharacterWidget {
private:
    std::set<CharacterInfoDisplay *> m_infoWidgets;
    QVBoxLayout *m_scrollLayout;
public:
    CharacterInfo(CharacterView *cv);

    virtual void textDump(QTextStream &) {}

    virtual void updateDisplay();
private:
    void addEgoBuildPointDisplay();
    void addLocationBuildPointDisplay();
    void addEgoStatDisplay();
    void addMorphStatDisplay();
    void addSkillDisplay();
    void addCheckDisplay();
    void addPoolDisplay();
    void addMessageDisplay(bool ego, bool morph);
};

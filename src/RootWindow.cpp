#include <QMenuBar>
#include <QTabWidget>
#include <QFile>
#include <QFileDialog>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QStyle>
#include <QTextStream>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QTabBar>
#include <QCoreApplication>
#include <QStandardPaths>
#include <QSettings>
#include <QMessageBox>
#include "pugi/pugixml.hpp"

#include "CharacterRoot.h"
#include "CharacterDebug.h"
#include "CharacterView.h"
#include "DataEditor.h"

#include "data/State.h"
#include "DataInvariants.h"

#include "moc_RootWindow.cpp"

static void handler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
        case QtDebugMsg:
            fprintf(stderr, "[DBG %20s]: %s\n", context.function, localMsg.constData());
            break;
        case QtInfoMsg:
            fprintf(stderr, "[INF %20s]: %s\n", context.function, localMsg.constData());
            break;
        case QtWarningMsg:
            fprintf(stderr, "[WRN %20s]: %s\n", context.function, localMsg.constData());
            QMessageBox::warning(nullptr, "Warning!", msg);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "[CRT %20s]: %s\n", context.function, localMsg.constData());
            QMessageBox::critical(nullptr, "Critical error!", msg);
            break;
        case QtFatalMsg:
            fprintf(stderr, "[FAT %20s]: %s\n", context.function, localMsg.constData());
            QMessageBox::critical(nullptr, "Fatal error!", msg);
            break;
    }

}

RootWindow::RootWindow() {
    qInstallMessageHandler(handler);
    m_central = new QTabWidget(this);

    QSettings qs;
    qs.setValue("fileLocation", qs.value("fileLocation", 
        QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)));

    m_central->setTabPosition(QTabWidget::South);
    setCentralWidget(m_central);

    auto bar = m_central->tabBar();
    auto debugEgoAction = new QAction("Debug character ego");
    connect(debugEgoAction, SIGNAL(triggered()), this, SLOT(debugCharacterEgo()));
    bar->addAction(debugEgoAction);
    auto debugMorphAction = new QAction("Debug character morph");
    connect(debugMorphAction, SIGNAL(triggered()), this, SLOT(debugCharacterMorph()));
    bar->addAction(debugMorphAction);
    bar->setContextMenuPolicy(Qt::ActionsContextMenu);

    buildMenus();
    loadData();
}

void RootWindow::buildMenus() {
    QMenu *epc = menuBar()->addMenu("&EPC");

    epc->addAction(
        style()->standardIcon(QStyle::SP_FileIcon),
        "&Create",
        this,
        SLOT(createCharacter()),
        QKeySequence(Qt::CTRL + Qt::Key_N));

    epc->addAction(
        style()->standardIcon(QStyle::SP_FileIcon),
        "C&lose",
        this,
        SLOT(closeCharacter()),
        QKeySequence(Qt::CTRL + Qt::Key_W));

    epc->addAction(
        style()->standardIcon(QStyle::SP_DriveHDIcon),
        "Save",
        this,
        SLOT(saveActiveCharacter()),
        QKeySequence(Qt::CTRL + Qt::Key_S));

    epc->addAction(
        style()->standardIcon(QStyle::SP_DriveHDIcon),
        "Save &As...",
        this,
        SLOT(saveActiveCharacterAs()),
        QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));

    epc->addAction(
        style()->standardIcon(QStyle::SP_DriveHDIcon),
        "&Load...",
        this,
        SLOT(loadCharacter()),
        QKeySequence(Qt::CTRL + Qt::Key_O));

    epc->addAction(
        style()->standardIcon(QStyle::SP_DockWidgetCloseButton),
        "E&xit",
        this,
        SLOT(close()),
        QKeySequence(Qt::CTRL + Qt::Key_Q));

    QMenu *ex = menuBar()->addMenu("E&xport");

    ex->addAction(
        "Text summary",
        this,
        SLOT(textDumpCharacter()));

    QMenu *data = menuBar()->addMenu("&Data");

    data->addAction(
        "Load additional data...",
        this,
        SLOT(loadExtraData()));
    data->addAction(
        "Open Data Editor",
        this,
        SLOT(showDataEditor()));
}

void RootWindow::loadData() {
    m_coreState = new Data::State();

    m_coreState->loadFrom(
        QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("data/core/core.xml"));
    
    m_globalState = new Data::State(m_coreState);

    m_globalState->loadFrom(
        QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("data/modular/modular.xml"));

    DataInvariants di;
    di.run(m_globalState);
}

void RootWindow::createCharacter() {
    auto character = new Data::Character(m_globalState);
    character->newCharInit();

    CharacterRoot *cr = new CharacterRoot(character);
    static_cast<QTabWidget *>(centralWidget())->addTab(cr, "Unnamed character");
}

void RootWindow::closeCharacter() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;

    auto central = static_cast<QTabWidget *>(centralWidget());
    central->removeTab(central->currentIndex());

    delete active;
}

void RootWindow::saveActiveCharacter() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;

    if(active->filename() == "") {
        saveActiveCharacterAs();
        return;
    }

    QFile file(active->filename());

    pugi::xml_document doc;
    active->character()->saveInto(doc.append_child());
    doc.save_file(qUtf8Printable(active->filename()));
}

void RootWindow::saveActiveCharacterAs() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;

    QFileDialog saveDialog;
    QSettings qs;

    saveDialog.setAcceptMode(QFileDialog::AcceptSave);
    saveDialog.setDefaultSuffix(".epc");
    saveDialog.setNameFilters(QStringList() << "*.epc");
    saveDialog.setDirectory(qs.value("fileLocation").toString());

    if(!saveDialog.exec()) return;

    Q_ASSERT(saveDialog.selectedFiles().size() == 1);

    QString filename = saveDialog.selectedFiles()[0];

    if(filename == "") return;

    QFile file(filename);

    pugi::xml_document doc;
    active->character()->saveInto(doc.append_child());
    doc.save_file(qUtf8Printable(filename));

    QFileInfo fi(file);
    qs.setValue("fileLocation", fi.dir().absolutePath());

    active->setFilename(filename);
}

void RootWindow::loadCharacter() {
    QSettings qs;
    QString filter = QString("EPC files (*.epc)");
    QString filename = QFileDialog::getOpenFileName(
        this, QString("Load character..."),
        qs.value("fileLocation").toString(), filter, &filter);

    if(filename == "") return;

    QFile file(filename);

    pugi::xml_document doc;
    auto result = doc.load_file(qUtf8Printable(filename));

    if(!result) {
        qWarning("Couldn't open/parse file \"%s\"", qUtf8Printable(filename));
        return;
    }

    Data::Character *character = new Data::Character(m_globalState);
    character->loadFrom(doc.document_element());

    CharacterRoot *cr = new CharacterRoot(character);
    static_cast<QTabWidget *>(centralWidget())->addTab(cr, "Loaded character");

    QFileInfo fi(file);
    qs.setValue("fileLocation", fi.dir().absolutePath());
}

void RootWindow::debugCharacterEgo() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;


    auto state = active->cv()->ego()->state();
    if(!state) return;
    auto debug = new CharacterDebug(state);
    debug->show();
}

void RootWindow::debugCharacterMorph() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;


    auto morph = active->cv()->morph();
    if(!morph) return;
    auto state = morph->state();
    if(!state) return;
    auto debug = new CharacterDebug(state);
    debug->show();
}

void RootWindow::textDumpCharacter() {
    auto active = static_cast<CharacterRoot *>(m_central->currentWidget());
    if(!active) return;

    QString content;
    QTextStream ts(&content);
    
    active->textDump(ts);

    QDialog *dialog = new QDialog(this);
    dialog->setModal(true);
    
    QHBoxLayout *layout = new QHBoxLayout();
    dialog->setLayout(layout);

    QTextEdit *edit = new QTextEdit();
    edit->setFont(QFont("monospace"));
    layout->addWidget(edit);

    edit->setText(content);

    dialog->exec();

    delete dialog;
}

void RootWindow::loadExtraData() {
    QSettings qs;
    QString filter = QString("EPC data files (*.xml)");

    QString filename = QFileDialog::getOpenFileName(
        this, QString("Load extra data..."),
        qs.value("fileLocation").toString(), filter, &filter);
    
    if(filename == "") return;

    m_globalState->loadFrom(filename);

    DataInvariants di;
    di.run(m_globalState);
}

void RootWindow::showDataEditor() {
    DataEditor *editor = new DataEditor(m_coreState);
    editor->show();
}

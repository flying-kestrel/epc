#pragma once

#include <QObject>

#include "data/State.h"

class StatDisplay;
class StatLabel;
class QHBoxLayout;
class CharacterWidget;

class StatDisplayColumn : public QObject {
public:
    virtual void populate(Data::State *state, QHBoxLayout *layout,
        Data::Stat *stat) = 0;

    virtual void updateDisplay(Data::State *) {}
};

class StatDisplayValueColumn : public StatDisplayColumn {
private:
    std::set<StatLabel *> m_labels;
public:
    virtual void populate(Data::State *state, QHBoxLayout *layout,
        Data::Stat *stat);

    virtual void updateDisplay(Data::State *state);
};

class StatDisplayAdjustColumn : public StatDisplayColumn {
protected:
    CharacterWidget *m_widget;
    Data::OffsetModifier *m_modifier;
public:
    StatDisplayAdjustColumn(CharacterWidget *widget,
        Data::OffsetModifier *modifier)
        : m_widget(widget), m_modifier(modifier) {}

    virtual void populate(Data::State *state, QHBoxLayout *layout,
        Data::Stat *stat);
protected:
    virtual bool shouldShow(Data::Stat *stat) = 0;
    virtual void adjust(Data::State *state, Data::Stat *stat, int by) = 0;
private:
    void adjustWrapper(Data::State *state, Data::Stat *stat, int dir);
};

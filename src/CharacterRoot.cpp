#include <QTabWidget>
#include <QStringBuilder>
#include <QDebug>
#include <QVBoxLayout>
#include <QSplitter>
#include <QTextStream>
#include <QAction>
#include <QShortcut>

#include "data/State.h"

#include "CharacterWidget.h"
#include "CharacterRoot.h"
#include "CharacterGear.h"
#include "CharacterNotes.h"
#include "CharacterView.h"
#include "CharacterEgo.h"
#include "CharacterInfo.h"
#include "CharacterCareer.h"
#include "StatLabel.h"

#include "moc_CharacterRoot.cpp"

CharacterRoot::CharacterRoot(Data::Character *character)
    : m_character(character), m_lastWidget(nullptr) {

    QHBoxLayout *layout = new QHBoxLayout();
    setLayout(layout);

    QSplitter *splitter = new QSplitter();

    m_characterTabs = new QTabWidget();

    m_cv = new CharacterView(m_character);
    m_characterTabs->setCornerWidget(m_cv);
    connect(m_cv, &CharacterView::performUpdate, [=]() {
        auto cw
            = static_cast<CharacterWidget *>(m_characterTabs->currentWidget());
        cw->updateDisplay();
    });

    splitter->addWidget(m_characterTabs);
    auto info = new CharacterInfo(m_cv);
    connect(m_cv, &CharacterView::performUpdate, [=]() { info->updateDisplay(); });
    splitter->addWidget(info);
    layout->addWidget(splitter);

    CharacterWidget *widget;

    widget = new CharacterEgo(m_cv);
    m_characterTabs->addTab(widget, "&Ego");
    m_widgets.push_back(widget);

    widget = new CharacterGear(m_cv);
    m_characterTabs->addTab(widget, "Morphs && &Gear");
    m_widgets.push_back(widget);

    widget = new CharacterNotes(m_character);
    m_characterTabs->addTab(widget, "&Notes");
    m_widgets.push_back(widget);

    widget = new CharacterCareer(m_cv);
    m_characterTabs->addTab(widget, "&Career");
    m_widgets.push_back(widget);

    connect(m_characterTabs, &QTabWidget::currentChanged, [=](int to) {
        auto cw = static_cast<CharacterWidget *>(m_characterTabs->widget(to));
        cw->updateDisplay();
    });

    static_cast<CharacterWidget *>(m_characterTabs->currentWidget())->updateDisplay();
}

CharacterRoot::~CharacterRoot() {
    delete m_character;
}

void CharacterRoot::textDump(QTextStream &stream) {
    for(auto widget : m_widgets) widget->textDump(stream);
}

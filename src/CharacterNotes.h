#pragma once

#include "CharacterWidget.h"

#include "data/Character.h"

class CharacterNotes : public CharacterWidget {
private:
    Data::Character *m_character;
public:
    CharacterNotes(Data::Character *character);

    virtual void textDump(QTextStream &stream);
    virtual void updateDisplay();
};

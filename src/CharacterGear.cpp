#include <QFormLayout>
#include <QComboBox>
#include <QGridLayout>
#include <QListWidget>
#include <QTreeWidget>
#include <QLabel>
#include <QPushButton>
#include <QDialog>
#include <QInputDialog>
#include <QMenu>
#include <QLineEdit>
#include <QScrollArea>
#include <QDropEvent>
#include <QMimeData>
#include <QStandardItemModel>
#include <QHeaderView>

#include "data/State.h"

#include "CharacterGear.h"
#include "StatDisplay.h"
#include "CharacterBPSummary.h"
#include "EditorTree.h"
#include "CharacterView.h"
#include "SelectionDialog.h"

CharacterGear::CharacterGear(CharacterView *cv) : CharacterWidget(cv) {
    m_character = const_cast<Data::Character *>(cv->character());

    QHBoxLayout *layout = new QHBoxLayout();
    setLayout(layout);

    QVBoxLayout *leftColumn = new QVBoxLayout();
    layout->addLayout(leftColumn);
    QHBoxLayout *leftColumnHeader = new QHBoxLayout();
    leftColumn->addLayout(leftColumnHeader);
    QPushButton *newLocation = new QPushButton("New location");
    connect(newLocation, &QPushButton::clicked, [=]() {
        addNewLocation();
    });
    leftColumnHeader->addWidget(newLocation, 1);

    class GearTree : public EditorTree {
    private:
        CharacterGear *m_cg;
    public:
        GearTree(CharacterGear *cg) : m_cg(cg) {}
    private:
        virtual void dropEvent(QDropEvent *event) {
            if(m_cg->gearTreeDropEvent(event)) {
                m_cg->updateDisplay();
            }
        }
    };

    m_gearTree = new GearTree(this);
    m_gearTree->setColumnCount(3);
    m_gearTree->setColumnWidth(1, 15);
    m_gearTree->setColumnWidth(2, 25);
    auto header = new QTreeWidgetItem(QStringList() << "Item" << "" << "?");
    m_gearTree->setHeaderItem(header);
    m_gearTree->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    m_gearTree->header()->setStretchLastSection(false);
    m_gearTree->setDragEnabled(true);
    m_gearTree->viewport()->setAcceptDrops(true);
    m_gearTree->setDropIndicatorShown(true);
    m_gearTree->setDragDropMode(QAbstractItemView::InternalMove);
    m_gearTree->setSortingEnabled(true);
    m_gearTree->sortByColumn(0, Qt::AscendingOrder);

    leftColumn->addWidget(m_gearTree);
    connect(m_gearTree, &QTreeWidget::itemChanged,
        [=](QTreeWidgetItem *item, int column) {

        if(m_synchronizing) return;
        if(column != 2) return;

        bool active = item->checkState(2) == Qt::Checked;
        auto gear = m_gearTree->dataOf<Data::GearNode *>(item);
        // need to get the relevant morph
        QTreeWidgetItem *mc = item;
        while(mc && m_gearTree->typeOf(mc) != MorphType) {
            mc = mc->parent();
        }
        if(mc) {
            auto morph = m_gearTree->dataOf<Data::Morph *>(mc);
            gear->changeActive(morph->state(), active);
        }
        // TODO: invariant checking (only one weapon, etc?)
        updateDisplay();
    });

    connect(m_gearTree, &QTreeWidget::itemSelectionChanged, [=]() {
        if(!m_gearTree->synchronizing()) selectionChanged();
    });

    setupGearTreeMenus();

    m_selectionDetails = new QLabel();
    leftColumn->addWidget(m_selectionDetails);
}

void CharacterGear::textDump(QTextStream &stream) {
    
}

void CharacterGear::setupGearTreeMenus() {
    /* LocationType */
    auto menu = new QMenu();
    connect(menu->addAction("Add Morph..."), &QAction::triggered, [=]() {
        addNewMorph(m_gearTree->dataOf<Data::Location *>(m_gearTree->menuFor()));
    });
    m_gearTree->addContextMenu(LocationType, [=](QTreeWidgetItem *) { return menu; });

    /* MorphType */
    menu = new QMenu();
    connect(menu->addAction("Remove Morph"), &QAction::triggered, [=]() {
        // addNewMorph(m_gearTree->dataOf<Data::Location *>(m_gearTree->menuFor()));
        qDebug("NYI");
    });
    auto setALI = menu->addAction("Set ALI...");
    connect(setALI, &QAction::triggered, [=]() {
        SelectionDialog sd(m_cv->charState(), "alicat", "");

        if(sd.exec() != SelectionDialog::Accepted) return;

        auto chosen = sd.chosen();

        auto morph = m_gearTree->dataOf<Data::Morph *>(m_gearTree->menuFor());

        if(m_character->ego(morph->name())) {
            qWarning("Ego with name \"%s\" already exists!",
                qUtf8Printable(morph->name()));
            return;
        }

        m_character->createEgo(morph->name());

        auto ali = m_character->createGear(chosen, true);
        ali->isFixed = true;
        morph->gear.children.push_back(ali);

        m_cv->updateView();
    });
    m_gearTree->addContextMenu(MorphType,
        [=](QTreeWidgetItem *item) {

        auto morph = m_gearTree->dataOf<Data::Morph *>(item);
        auto col = m_character->state()->getCollectionByName(morph->type());
        setALI->setEnabled(col->hasTag("morph:unsleevable"));
        bool hasEgo = false;
        for(auto c : morph->gear.children) {
            if(c->isFixed &&
                m_cv->charState()->getCollectionByName(c->name)->hasTag("gear:isego")) {

                hasEgo = true;
            }
        }
        if(hasEgo) setALI->setEnabled(false);
        return menu; 
    });

    /* GearType */
    menu = new QMenu();
    connect(menu->addAction("Add Gear..."), &QAction::triggered, [=]() {
        Data::Morph *morph = nullptr;

        auto node = m_gearTree->menuFor();
        while(m_gearTree->typeOf(node) == GearType) {
            node = node->parent();
        }
        if(m_gearTree->typeOf(node) == MorphType) {
            morph = m_gearTree->dataOf<Data::Morph *>(node);
        }

        auto gear = m_gearTree->dataOf<Data::GearNode *>(m_gearTree->menuFor());
        QString filter;
        if(!gear->isContainer()) {
            auto col
                = m_character->state()->getCollectionByName(gear->name);
            filter = col->map("mods");
        }
        addNewGear(gear, filter, morph);
    });
    connect(menu->addAction("Add gear container..."), &QAction::triggered, [=]() {
        auto gear = m_gearTree->dataOf<Data::GearNode *>(m_gearTree->menuFor());
        auto name = QInputDialog::getText(this, "Add new gear container", "Enter container name:");
        if(name.length() > 0) {
            auto ngear = m_character->createGear(name, false, 0);
            gear->children.push_back(ngear);
            updateDisplay();
        }
    });
    connect(menu->addAction("Gear notes..."), &QAction::triggered, [=]() {
        auto gear = m_gearTree->dataOf<Data::GearNode *>(m_gearTree->menuFor());
        bool ok;
        auto result = QInputDialog::getMultiLineText(this, "Gear notes", "", gear->note, &ok);
        if(ok) gear->note = result;
        updateDisplay();
    });
    auto removeGear = menu->addAction("Remove gear");
    connect(removeGear, &QAction::triggered, [=]() {
        auto gear = m_gearTree->dataOf<Data::GearNode *>(m_gearTree->menuFor());

        if(m_gearTree->menuFor()->flags() & Qt::ItemIsDragEnabled) {
            auto parent = m_gearTree->menuFor()->parent();
            auto parentGear = m_gearTree->dataOf<Data::GearNode *>(parent);

            if(gear->egoName != "") {
                m_character->removeEgo(gear->egoName);
            }

            for(unsigned i = 0; i < parentGear->children.size(); i ++) {
                if(parentGear->children[i] == gear) {
                    /* XXX: should remove relevant modifiers from state */
                    parentGear->children.erase(parentGear->children.begin() + i);
                    updateDisplay();
                    break;
                }
            }
        }
    });
    m_gearTree->addContextMenu(GearType, [=](QTreeWidgetItem *item) {
        auto gear = m_gearTree->dataOf<Data::GearNode *>(item);
        removeGear->setDisabled(gear->isFixed);
        return menu;
    });

    /* TraitContainerType */
    menu = new QMenu();
    connect(menu->addAction("Add..."), &QAction::triggered, [=]() {
        addNewTrait(m_gearTree->menuFor());
    });
    m_gearTree->addContextMenu(TraitContainerType, [=](QTreeWidgetItem *) { return menu; });

    /* TraitType */
    menu = new QMenu();
    connect(menu->addAction("Remove Trait"), &QAction::triggered, [=]() {
        auto proxy = m_gearTree->dataOf<TraitProxy::Proxy *>(m_gearTree->menuFor());

        auto parent = m_gearTree->menuFor()->parent();
        // are we in a morph?
        if(parent->parent()
            && m_gearTree->typeOf(parent->parent()) == MorphType) {

            auto morph = m_gearTree->dataOf<Data::Morph *>(parent->parent());
            morph->removeTrait(proxy->name, false);
        }
        // ego?
        else if(parent->parent() == nullptr) {
            m_character->removeTrait(proxy->name);
        }
        updateDisplay();
    });
    m_gearTree->addContextMenu(TraitType, [=](QTreeWidgetItem *) { return menu; });
}

void CharacterGear::updateDisplay() {
    m_gearTree->setSortingEnabled(false);

    m_cv->updateView();

    // update tree
    auto locationNames = m_character->locationList();

    std::vector<Data::Location *> locations;
    for(auto l : locationNames) {
        locations.push_back( m_character->location(l));
    }

    m_gearTree->synchronize(m_gearTree->invisibleRootItem(), locations, LocationType);

    // update all the locations individually, and the ego traits as well
    for(int i = 0; i < m_gearTree->topLevelItemCount(); i ++) {
        auto item = m_gearTree->topLevelItem(i);
        auto type = m_gearTree->typeOf(item);
        if(type == LocationType) {
            updateLocation(item);
        }
    }

    m_gearTree->setSortingEnabled(true);
    m_gearTree->resizeColumnToContents(0);
    m_gearTree->resizeColumnToContents(1);

    selectionChanged();
}

void CharacterGear::updateLocation(QTreeWidgetItem *locationRoot) {
    auto location = m_gearTree->dataOf<Data::Location *>(locationRoot);

    // new location?
    if(locationRoot->childCount() == 0) {
        std::vector<Data::GearNode *> gearPtr;
        gearPtr.push_back(&location->gear);

        m_gearTree->synchronize(locationRoot, gearPtr, GearType);

        locationRoot->setFlags(Qt::ItemIsEnabled);
        locationRoot->child(0)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
    }

    std::vector<Data::Morph *> morphPtrs;
    for(auto l : location->morphs()) {
        morphPtrs.push_back(l.second);
    }

    // add/remove morph nodes
    m_gearTree->synchronize(locationRoot, morphPtrs, MorphType);

    // all sub-nodes have been created, time to populate/update
    for(int i = 0; i < locationRoot->childCount(); i ++) {
        auto item = locationRoot->child(i);
        auto type = m_gearTree->typeOf(item);
        if(type == MorphType) updateMorph(item);
        else if(type == GearType) updateGearNode(item);
    }

    int mpUsed = 0;
    int gpUsed = 0;
    location->used(m_cv->charState(), gpUsed, mpUsed);

    locationRoot->setText(0, location->name + " ["
        + QString::number(gpUsed)
        + " GP, "
        + QString::number(mpUsed)
        + " MP]");
}

void CharacterGear::updateMorph(QTreeWidgetItem *morphItem) {
    auto morph = m_gearTree->dataOf<Data::Morph *>(morphItem);

    auto morphCollection =
        m_character->state()->getCollectionByName(morph->type());

    // new morph?
    if(morphItem->childCount() == 0) {
        morphItem->setText(0, morph->name() + " [" + morphCollection->desc() + "]");
        morphItem->setFlags(Qt::ItemIsEnabled);

        std::vector<Data::GearNode *> gearPtr;
        gearPtr.push_back(&morph->gear);

        m_gearTree->synchronize(morphItem, gearPtr, GearType);

        morphItem->child(0)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);

        auto traits = new QTreeWidgetItem(QStringList() << "Morph traits");
        m_gearTree->setTypeOf(traits, TraitContainerType);
        m_gearTree->setDataOf(traits, QString("trait:morph"));
        traits->setFlags(Qt::ItemIsEnabled);
        morphItem->addChild(traits);
    }

    for(int i = 0; i < morphItem->childCount(); i ++) {
        auto ci = morphItem->child(i);
        int type = m_gearTree->typeOf(ci);
        if(type == GearType) updateGearNode(ci);
        else if(type == TraitContainerType) updateTraitContainer(ci, morph);
    }
}

int CharacterGear::updateGearNode(QTreeWidgetItem *gearItem) {
    int cost = 0;
    auto gear = m_gearTree->dataOf<Data::GearNode *>(gearItem);

    if(gear->isContainer()) {
        gearItem->setText(0, gear->name);
        gearItem->setFlags(gearItem->flags() | Qt::ItemIsDropEnabled);
    }
    else {
        auto col = m_character->state()->getCollectionByName(gear->name);
        gearItem->setText(0, col->desc());

        if(gear->isFixed) {
            gearItem->setTextColor(0, Qt::darkBlue);
            gearItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        }
        else {
            cost = col->map("cost").toInt();
            gearItem->setText(1, col->map("cost"));
            gearItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
        }

        if(col->hasTag("gear:terminator")) {
            gearItem->setCheckState(2, gear->isActive ? Qt::Checked : Qt::Unchecked);
            gearItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled
                | Qt::ItemIsDragEnabled | Qt::ItemIsUserCheckable);
        }
    }

    /*if(gear->note == "") {
        gearItem->setBackgroundColor(1, Qt::white);
    }
    else {
        gearItem->setBackgroundColor(1, Qt::blue);
    }*/

    std::vector<Data::GearNode *> gearPtrs;
    for(auto c : gear->children) {
        gearPtrs.push_back(c);
    }
    m_gearTree->synchronize(gearItem, gearPtrs, GearType);

    int subcost = 0;
    for(int i = 0; i < gearItem->childCount(); i ++) {
        subcost += updateGearNode(gearItem->child(i));
    }

    if(gear->isContainer()) {
        gearItem->setText(1, "(" + QString::number(cost+subcost) + ")");
    }
    else {
        if(subcost > 0) {
            gearItem->setText(1, QString::number(cost) + " (" + QString::number(subcost) + ")");
        }
        else if(cost > 0) {
            gearItem->setText(1, QString::number(cost));
        }
        else {
            gearItem->setText(1, "-");
        }
    }

    return cost + subcost;
}

void CharacterGear::updateTraitContainer(QTreeWidgetItem *containerItem,
    Data::Morph *morph) {

    std::vector<TraitProxy::Proxy *> traitPtrs;

    for(auto t : morph->traits()) {
        traitPtrs.push_back(m_traitProxy.get(t));
    }
    auto col = m_character->state()->getCollectionByName(morph->type());
    for(auto t : col->map("traits").split(' ')) {
        if(t == "") continue;
        traitPtrs.push_back(m_traitProxy.get(t, true));
    }

    m_gearTree->synchronize(containerItem, traitPtrs, TraitType);

    for(int i = 0; i < containerItem->childCount(); i ++) {
        updateTrait(containerItem->child(i));
    }
}

void CharacterGear::updateTrait(QTreeWidgetItem *traitItem) {
    if(traitItem->text(0) != "") return;

    auto proxy = m_gearTree->dataOf<TraitProxy::Proxy *>(traitItem);

    auto col = m_character->state()->getCollectionByName(proxy->name);

    traitItem->setText(0, col->desc());
    if(proxy->fixed) {
        traitItem->setTextColor(0, Qt::darkBlue);
    }
    else {
        Data::State st(m_character->state());
        col->addToState(&st);
        auto basecalc = m_character->state()->calculate();
        auto calc = st.calculate();
        auto mp = st.getStatByName("mp:used");
        auto cp = st.getStatByName("cp:used");

        int cost = calc.values[mp].die(0) - basecalc.values[mp].die(0);
        cost += calc.values[cp].die(0) - basecalc.values[cp].die(0);

        traitItem->setText(1, QString::number(cost));

        if(cost < 0) traitItem->setTextColor(1, Qt::darkRed);
    }
}

#if 0
void CharacterGear::updateMessages() {
    // XXX: this will work somewhat differently
    #if 0
    auto morph = m_character->currentMorph();
    if(!morph) {
        m_messages->clear();
        return;
    }

    std::set<QStringList> messages;
    std::function<void (Data::GearNode *)> collectGear = [&](Data::GearNode *gn) {
        if(!gn->isContainer()) {
            auto col = m_character->state()->getCollectionByName(gn->name);
            if(col->hasTag("gear:terminator") && gn->isActive == false) return;

            auto msg = col->map("message");
            if(msg.length() > 0) messages.insert(QStringList() << col->desc() << msg);
        }
        for(auto ch : gn->children) collectGear(ch);
    };
    collectGear(&morph->gear);

    std::set<QString> traits;
    traits.insert(m_character->traits().begin(), m_character->traits().end());
    traits.insert(m_character->fixedTraits().begin(), m_character->fixedTraits().end());
    traits.insert(morph->traits.begin(), morph->traits.end());

    for(auto trait : traits) {
        auto col = m_character->state()->getCollectionByName(trait);
        auto msg = col->map("message");
        if(msg.length() > 0) messages.insert(QStringList() << col->desc() << msg);
    }

    m_messages->clear();
    for(auto msg : messages) m_messages->addItem("[" + msg.front() + "]: " + msg.back());
    #endif
}
#endif

void CharacterGear::addNewLocation() {
    QString result = QInputDialog::getText(this,
        "Enter new location name", "Location name:");

    if(result == "" || m_character->location(result)) return;

    m_character->makeLocation(result);
    updateDisplay();
}

void CharacterGear::addNewMorph(Data::Location *location) {
    SelectionDialog sd(m_cv->charState(), "morphcat", "");
    if(!sd.exec()) return;

    QString name = QInputDialog::getText(this, "Morph creation", "Morph name?");
    if(name == "") return;

    m_character->createMorphIn(location, sd.chosen(), name);

    updateDisplay();
}

void CharacterGear::addNewGear(Data::GearNode *node, QString filter,
    Data::Morph *morph) {

    std::vector<QString> disallow, reallow;

    if(morph) {
        auto col = m_cv->charState()->getCollectionByName(morph->type());
        if(col->hasTag("morph:bio")) {
            disallow = {"ware:h", "ware:m"};
            reallow = {"ware:c", "ware:n", "ware:b"};
        }
        else if(col->hasTag("morph:info")) {
            disallow = {"ware:h", "ware:c", "ware:b", "ware:n"};
            reallow = {"ware:m"};
        }
        else if(col->hasTag("morph:pod")) {
            disallow = {};
            reallow = {"ware:b", "ware:m", "ware:n", "ware:h", "ware:c"};
        }
        else if(col->hasTag("morph:synth")) {
            disallow = {"ware:b"};
            reallow = {"ware:m", "ware:n", "ware:h", "ware:c"};
        }
    }

    SelectionDialog sd(m_cv->charState(), "gearcat", filter,
        [=](Data::CollectionModifier *col) {

        bool allowed = true;
        for(auto d : disallow) {
            if(col->hasTag(d)) allowed = false;
        }
        if(!allowed) {
            for(auto r : reallow) {
                if(col->hasTag(r)) allowed = true;
            }
        }

        return allowed;
    });

    if(!sd.exec()) return;

    auto gearType = sd.chosen();

    auto cnode = m_character->createGear(gearType, true);

    node->children.push_back(cnode);

    updateDisplay();
}

void CharacterGear::addNewTrait(QTreeWidgetItem *to) {
    Data::Morph *morph = nullptr;
    morph = m_gearTree->dataOf<Data::Morph *>(to->parent());

    QStringList exclude;
    exclude << "trait:nondefault";
    for(auto trait : morph->traits()) {
        auto col = m_cv->ego()->state()->getCollectionByName(trait);
        auto it = col->getMap().find("category");
        if(it == col->getMap().end()) continue;
        exclude << it->second;
    }

    SelectionDialog sd(morph->state(), "traitcat:morph", "",
        [=](Data::CollectionModifier *col) {

        for(auto tag : exclude) if(col->hasTag(tag)) return false;
        return true;
    });

    if(!sd.exec()) return;

    auto trait = sd.chosen();

    morph->addTrait(trait, false);

    updateDisplay();
}

void CharacterGear::selectionChanged() {
    auto selList = m_gearTree->selectedItems();
    if(selList.size() != 1) return;
    auto sel = selList[0];

    Data::Location *location = nullptr;
    Data::Morph *morph = nullptr;
    Data::GearNode *gear = nullptr;
    // find location and morph
    auto cursor = sel;
    while(cursor) {
        auto type = static_cast<ItemType>(m_gearTree->typeOf(cursor));
        if(type == LocationType) {
            location = m_gearTree->dataOf<Data::Location *>(cursor);
        }
        else if(type == MorphType) {
            morph = m_gearTree->dataOf<Data::Morph *>(cursor);
        }
        else if(type == GearType && !gear) {
            gear = m_gearTree->dataOf<Data::GearNode *>(cursor);
        }

        cursor = cursor->parent();
    }

    m_selectionDetails->clear();
    if(!gear) return;

    Data::State st(m_character->state());
    // add!
    gear->addTo(&st);

    auto cs = st.calculate();

    std::vector<QString> lines;
    for(auto value : cs.values) {
        if(value.second == Data::StatValue()) continue;
        if(value.first->hasTag("internal")) continue;
        if(value.first->hasTag("buildpoint")) continue;

        auto sources = st.linkSources(value.first);
        bool delta = false;
        for(auto source : sources) {
            if(value.second != cs.values[source]) delta = true;
        }
        if(!delta && sources.size()) continue;

        lines.push_back(value.first->longDesc() + ": " + value.second.format());
    }
    std::sort(lines.begin(), lines.end());
    QString result;
    for(auto line : lines) result += line + "\n";

    std::vector<QString> messages;
    for(auto col : st.getCollections()) {
        if(!col->active(&st)) continue;
        if(col->map("message").length() > 0) {
            messages.push_back(col->map("message"));
        }
    }
    std::sort(messages.begin(), messages.end());
    for(auto msg : messages) result += msg + "\n";

    m_selectionDetails->setText(result);

    gear->removeFrom(&st);
}

bool CharacterGear::gearTreeDropEvent(QDropEvent *event) {
    auto target = m_gearTree->itemAt(event->pos());
    if(!target) return false;

    QStandardItemModel m;
    m.dropMimeData(event->mimeData(), Qt::CopyAction, 0, 0, QModelIndex());
    auto source = m.item(0);

    auto itemList
        = m_gearTree->findItems("", Qt::MatchContains | Qt::MatchRecursive);
    QTreeWidgetItem *sourceItem = nullptr;
    for(auto item : itemList) {
        /* XXX: hack */
        if(m_gearTree->dataOf<QVariant>(item) == source->data(EditorTree::dataRole)) {
            sourceItem = item;
            break;
        }
    }

    if(!sourceItem) return false;
    // only internal nodes, for now
    if(!sourceItem->parent()) return false;

    // only consider if source/target have the same type
    if(m_gearTree->typeOf(target) != m_gearTree->typeOf(sourceItem)) {
        return false;
    }

    // for now, only consider if moving a gearnode
    if(m_gearTree->typeOf(sourceItem) != GearType) return false;

    // move in internal representation.
    auto sourceNode = m_gearTree->dataOf<Data::GearNode *>(sourceItem);
    auto parentNode = m_gearTree->dataOf<Data::GearNode *>(sourceItem->parent());
    auto targetNode = m_gearTree->dataOf<Data::GearNode *>(target);

    auto it = std::find(parentNode->children.begin(), parentNode->children.end(), sourceNode);
    // sanity check...
    if(it == parentNode->children.end()) return false;

    // XXX: this doesn't handle gear modifiers properly!
    // force recalculation
    parentNode->children.erase(it);
    targetNode->children.push_back(sourceNode);

    return true;
}

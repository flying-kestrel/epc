#include <QTextEdit>
#include <QVBoxLayout>
#include <QTextDocument>

#include "CharacterNotes.h"

#include "data/State.h"

CharacterNotes::CharacterNotes(Data::Character *character)
    : m_character(character) {

    Data::CollectionModifier *col;
    if(!character->state()->stateHasCollection("character:notes")) {
        col = new Data::CollectionModifier("character:notes", "Internal singleton");
        character->state()->addCollection(col);
    }
    col = character->state()->getCollectionByName("character:notes");

    QTextEdit *editor = new QTextEdit();
    editor->setAcceptRichText(true);

    editor->document()->setHtml(col->map("html"));

    connect(editor, &QTextEdit::textChanged, [=]() {
        col->setMap("html", editor->document()->toHtml());
    });

    QVBoxLayout *layout = new QVBoxLayout();
    setLayout(layout);
    layout->addWidget(editor);
}

void CharacterNotes::textDump(QTextStream &stream) {
    
}

void CharacterNotes::updateDisplay() {
    
}

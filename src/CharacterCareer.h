#pragma once

#include "data/BaseModifiers.h"

#include "CharacterWidget.h"

class QStackedWidget;

class CharacterCareer : public CharacterWidget { Q_OBJECT
public:
    struct CareerEvent {
        enum Type {
            Session,
            StressChange,
            DamageChange
        } type;
        QString desc;
        QVariantList params;
    };
private:
    QStackedWidget *m_displayStack;
public:
    CharacterCareer(CharacterView *cv);

    virtual void textDump(QTextStream &) {}

    virtual void updateDisplay();
private:
    Data::OffsetModifier *careerBPOff();
    void syncBPOff();

    std::vector<CareerEvent *> parseEvents();
    CareerEvent *parseEvent(QString from);
    void addEvent(CareerEvent *event);
signals:
    void updateLabelsWith(Data::CalculatedStats &cs);
};

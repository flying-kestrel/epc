#include "DataInvariants.h"

bool DataInvariants::run(Data::State *state) {
    m_state = state;
    m_ok = true;
    checkGearCosts();
    checkMorphReferences();
    checkTraitCustomize();
    return m_ok;
}

void DataInvariants::checkGearCosts() {
    auto gearList = m_state->getCollectionsByTag("gear");
    for(auto gear : gearList) {
        auto cost = gear->map("cost");
        if(cost == "") {
            qDebug("No cost specified for gear %s!", qPrintable(gear->name()));
            m_ok = false;
            continue;
        }
        bool ok;
        int value = cost.toInt(&ok);
        if(!ok || value <= 0) {
            qDebug("Invalid cost %s for gear %s!", qPrintable(cost),
                qPrintable(gear->name()));
            ok = false;
        }
    }
}

void DataInvariants::checkMorphReferences() {
    auto morphlist = m_state->getCollectionsByTag("morph");
    for(auto morph : morphlist) {
        auto gear = morph->map("gear");
        for(auto g : gear.split(" ")) {
            if(g == "") continue;
            if(!m_state->collectionExists(g)) {
                qDebug("Morph %s refers to unknown gear %s!",
                    qPrintable(morph->name()), qPrintable(g));
                m_ok = false;
            }
        }
        auto traits = morph->map("traits");
        for(auto t : traits.split(" ")) {
            if(t == "") continue;
            if(!m_state->collectionExists(t)) {
                qDebug("Morph %s refers to unknown trait %s!",
                    qPrintable(morph->name()), qPrintable(t));
                m_ok = false;
            }
        }
    }
}

void DataInvariants::checkTraitCustomize() {
    auto traitlist = m_state->getCollectionsByTag("trait");
    for(auto trait : traitlist) {
        auto customize = trait->map("customize");
        if(customize.length() == 0) continue;

        if(customize == "rep") {
            // OK
        }
        else if(customize == "string") {
            // OK
        }
        else if(customize == "skill") {
            // OK
        }
        else {
            qDebug("Trait %s has unknown customize value %s",
                qPrintable(trait->name()), qPrintable(customize));
            m_ok = false;
        }
    }
}

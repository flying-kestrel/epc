#include <QHBoxLayout>
#include <QDebug>
#include <QTextStream>

#include "StatDisplay.h"
#include "StatLabel.h"
#include "StatDisplayColumn.h"

#include "moc_StatDisplay.cpp"

StatDisplay::StatDisplay(Data::State *state, QString lastStage)
    : m_state(state), m_lastStage(lastStage) {

    m_layout = new QVBoxLayout();
    m_layout->addStretch(1);
    setLayout(m_layout);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void StatDisplay::textDump(QTextStream &stream) {
    auto cs = m_state->calculate();

    for(auto statdesc : m_statLabels) {
        Data::Stat *stat = nullptr;
        for(auto x : m_lastStats) {
            if(x->longDesc() == statdesc) { stat = x; break; }
        }
        if(!stat) continue;

        stream.setFieldWidth(25);
        stream << statdesc;
        stream.setFieldWidth(0);
        stream << ": " << cs.values[stat].format();
        stream << "\n";
    }
}

void StatDisplay::updateDisplay() {
    std::set<Data::Stat *> stats;

    for(auto tag : m_tags) {
        auto result = m_state->getStatsByTag(tag);
        stats.insert(result.begin(), result.end());
    }

    if(m_displayFilter) {
        std::vector<Data::Stat *> toerase;
        for(auto s : stats) if(!m_displayFilter(s)) toerase.push_back(s);
        for(auto x : toerase) stats.erase(x);
    }

    // create/delete stat entries as needed
    removeOldStats(stats);
    addNewStats(stats);

    /* update each of the columns as needed */
    for(auto col : m_columns) col->updateDisplay(m_state);

    std::swap(m_lastStats, stats);
}

void StatDisplay::removeOldStats(std::set<Data::Stat *> &newStats) {
    for(auto x : m_lastStats) {
        if(newStats.count(x)) continue;

        // find index
        unsigned index = 0;
        for(index = 0; index < m_statLabels.size(); index ++) {
            if(m_statLabels[index] == x->longDesc()) {
                break;
            }
        }
        Q_ASSERT(index != m_statLabels.size());

        auto rowWidget = m_layout->takeAt(index);
        delete rowWidget->widget();
        delete rowWidget;

        m_statLabels.erase(m_statLabels.begin() + index);
    }
}

void StatDisplay::addNewStats(std::set<Data::Stat *> &newStats) {
    for(auto x : newStats) {
        if(m_lastStats.count(x)) continue;

        // find index
        auto it = std::lower_bound(m_statLabels.begin(), m_statLabels.end(), x->longDesc());

        unsigned index = it - m_statLabels.begin();

        QWidget *rowWidget = new QWidget();
        rowWidget->setContentsMargins(0,0,0,0);
        QHBoxLayout *rowLayout = new QHBoxLayout();
        rowWidget->setLayout(rowLayout);
        rowLayout->setContentsMargins(0,0,0,0);

        QLabel *descLabel = new QLabel(x->longDesc());
        // descLabel->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
        rowLayout->addWidget(descLabel);
        descLabel->setMinimumWidth(150);

        for(auto col : m_columns) {
            col->populate(m_state, rowLayout, x);
        }

        rowLayout->addStretch(100);

        m_layout->insertWidget(index, rowWidget);
        m_statLabels.insert(it, x->longDesc());
    }
}

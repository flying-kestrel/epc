#pragma once

#include <map>

#include <QString>

class TraitProxy {
public:
    struct Proxy {
        QString name;
        bool fixed;
    };
private:
    std::map<std::pair<QString, bool>, Proxy *> m_proxies;
public:
    Proxy *get(QString name, bool fixed = false) {
        auto key = std::make_pair(name, fixed);
        auto it = m_proxies.find(key);
        if(it != m_proxies.end()) return it->second;

        return m_proxies[key] = new Proxy{name, fixed};
    }
};

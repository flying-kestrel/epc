#pragma once

#include <QWidget>

#include "data/State.h"

class QLabel;

class CharacterBPSummary : public QWidget { Q_OBJECT
public:
    struct Display {
        QString str;
        bool formatting = false;
    };
    struct Transform {
        int fromAmount;
        QString from;
        int targetAmount;
        QString target;
    };
private:
    const Data::State *m_state;
    QLabel *m_label;

    QString m_display;
public:
    CharacterBPSummary(const Data::State *state,
        Data::OffsetModifier *bpTransforms,
        QString display,
        const std::vector<Transform> &transforms = {});

    void updateDisplay();
signals:
    void amountChanged();
};

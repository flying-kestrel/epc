#include <QScrollArea>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMenu>

#include "CharacterInfo.h"
#include "CharacterInfoDisplay.h"
#include "CharacterView.h"

CharacterInfo::CharacterInfo(CharacterView *cv) : CharacterWidget(cv) {
    setContentsMargins(0, 0, 0, 0);
    QVBoxLayout *layout = new QVBoxLayout();
    setLayout(layout);
    layout->setContentsMargins(0,0,0,0);
    QScrollArea *scrollArea = new QScrollArea();

    QPushButton *addView = new QPushButton("Add view...");
    addView->setMenu(new QMenu());
    // layout->addWidget(addView);
    layout->addWidget(scrollArea);

    QVBoxLayout *scrollLayout = new QVBoxLayout();
    m_scrollLayout = scrollLayout;

    QVBoxLayout *scrollAreaLayout = new QVBoxLayout();
    scrollAreaLayout->addLayout(scrollLayout);
    scrollAreaLayout->addStretch(1);
    QWidget *container = new QWidget();
    container->setContentsMargins(0,0,0,0);
    container->setLayout(scrollAreaLayout);
    scrollArea->setWidget(container);
    scrollArea->setWidgetResizable(true);

    scrollLayout->setContentsMargins(0,0,0,0);
    scrollAreaLayout->setContentsMargins(0,0,0,0);

    auto egom = addView->menu()->addMenu("Ego");
    connect(egom->addAction("Build points"), &QAction::triggered, [=]() {
        addEgoBuildPointDisplay();
        updateDisplay();
    });

    auto locationm = addView->menu()->addMenu("Location");
    connect(locationm->addAction("Build points"), &QAction::triggered, [=]() {
        addLocationBuildPointDisplay();
        updateDisplay();
    });

    addEgoBuildPointDisplay();
    addLocationBuildPointDisplay();
    addEgoStatDisplay();
    addMorphStatDisplay();
    addSkillDisplay();
    addCheckDisplay();
    addPoolDisplay();
    addMessageDisplay(true, false);
    addMessageDisplay(false, true);

    updateDisplay();
}

void CharacterInfo::updateDisplay() {
    for(auto iw : m_infoWidgets) {
        iw->updateDisplay();
    }
}

void CharacterInfo::addEgoBuildPointDisplay() {
    auto bp = new CharacterInfoTemplateDisplay(m_cv);
    bp->setWantsEgo();
    bp->setup("Ego build points");
    bp->setTemplate(
        "CP: ${cp:used}/${cp:total}\n"
        "Apt: ${aptitude:used}/${aptitude:total}\n"
        "Rep: ${rep:used}/${rep:total}\n"
        "AS: ${active-skills:used}/${active-skills:total}\n"
        "KS: ${know-skills:used}/${know-skills:total}");
    m_infoWidgets.insert(bp);
    m_scrollLayout->addWidget(bp);
}

void CharacterInfo::addLocationBuildPointDisplay() {
    class LocationDisplay : public CharacterInfoLabelDisplay {
    public:
        using CharacterInfoLabelDisplay::CharacterInfoLabelDisplay;
    protected:
        virtual bool wantLocation() const { return true; }
        virtual void updateInfoDisplay(const Data::CalculatedStats &,
            Data::Ego *, Data::Location *location, Data::Morph *) {

            int gp, mp;
            location->used(m_cv->charState(), gp, mp);

            label()->setText(QString("GP: " + QString::number(gp) + " MP: " + QString::number(mp)));
        }
    };
    auto bp = new LocationDisplay(m_cv);
    bp->setup("Location build points");
    m_infoWidgets.insert(bp);
    m_scrollLayout->addWidget(bp);
}

void CharacterInfo::addEgoStatDisplay() {
    auto sd = new CharacterInfoStatDisplay(m_cv);
    sd->setWantsEgo();
    sd->setup("Ego stats");
    sd->setTags(QStringList() << "aptitude" << "rep"); // << "pool" << "check" << "aptitude-derived");
    sd->setDisjunction(true);
    m_infoWidgets.insert(sd);
    m_scrollLayout->addWidget(sd);
}

void CharacterInfo::addMorphStatDisplay() {
    auto sd = new CharacterInfoStatDisplay(m_cv);
    sd->setWantsMorph();
    sd->setup("Morph stats");
    sd->setTags(QStringList() << "morphstat" << "morph-derived");
    sd->setDisjunction(true);
    m_infoWidgets.insert(sd);
    m_scrollLayout->addWidget(sd);
}

void CharacterInfo::addSkillDisplay() {
    auto sd = new CharacterInfoStatDisplay(m_cv);
    sd->setOnlyDeltas();
    sd->setWantsEgo();
    sd->setWantsMorph();
    sd->setup("Skills");
    sd->setTags(QStringList() << "skill");
    sd->setDisjunction(true);
    m_infoWidgets.insert(sd);
    m_scrollLayout->addWidget(sd);
}

void CharacterInfo::addCheckDisplay() {
    auto sd = new CharacterInfoStatDisplay(m_cv);
    sd->setWantsEgo();
    sd->setWantsMorph();
    sd->setup("Checks");
    sd->setTags(QStringList() << "check" << "aptitude-derived" << "resleeve");
    sd->setDisjunction(true);
    m_infoWidgets.insert(sd);
    m_scrollLayout->addWidget(sd);
}

void CharacterInfo::addPoolDisplay() {
    auto sd = new CharacterInfoStatDisplay(m_cv);
    sd->setWantsEgo();
    sd->setWantsMorph();
    sd->setup("Pools");
    sd->setTags(QStringList() << "pool");
    sd->setDisjunction(true);
    m_infoWidgets.insert(sd);
    m_scrollLayout->addWidget(sd);
}

void CharacterInfo::addMessageDisplay(bool ego, bool morph) {
    auto md = new CharacterInfoMessageDisplay(m_cv);
    if(ego) md->setWantsEgo();
    if(morph) md->setWantsMorph();
    md->setup("Conditional stats");
    m_infoWidgets.insert(md);
    m_scrollLayout->addWidget(md);
}

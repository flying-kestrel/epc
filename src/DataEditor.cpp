#include <QTableWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QMenu>
#include <QInputDialog>
#include <QMenuBar>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>
#include <QXmlStreamWriter>

#include "DataEditor.h"
#include "DataCollectionEditor.h"

DataEditor::DataEditor(Data::State *globalState)
    : m_globalState(globalState) {

    m_state = nullptr;

    setWindowTitle("Data editor");
    setContentsMargins(0,0,0,0);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->setContentsMargins(0,0,0,0);
    QMenuBar *menu = new QMenuBar();
    layout->addWidget(menu);

    auto m = menu->addMenu("&Editor");
    connect(m->addAction("&New data file"), &QAction::triggered, [=]() {
        delete m_state;
        m_state = new Data::State(globalState);
    });
    connect(m->addAction("&Save data file"), &QAction::triggered, [=]() {
        QFileDialog saveDialog;
        QSettings qs;

        saveDialog.setAcceptMode(QFileDialog::AcceptSave);
        saveDialog.setDefaultSuffix(".xml");
        saveDialog.setNameFilters(QStringList() << "*.xml");
        saveDialog.setDirectory(qs.value("fileLocation").toString());

        if(!saveDialog.exec()) return;

        Q_ASSERT(saveDialog.selectedFiles().size() == 1);

        QString filename = saveDialog.selectedFiles()[0];

        if(filename == "") return;

        QFile file(filename);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qDebug("Couldn't open file \"%s\" for writing", filename.toStdString().c_str());
            return;
        }

        pugi::xml_document doc;
        m_state->saveInto(doc.document_element());
        if(!doc.save_file(qUtf8Printable(filename))) {
            qDebug("Couldn't open file \"%s\" for writing", qUtf8Printable(filename));
        }
    });
    connect(m->addAction("&Load data file"), &QAction::triggered, [=]() {
        QSettings qs;
        QString filter = QString("EPC data files (*.xml)");
        QString filename = QFileDialog::getOpenFileName(
            this, QString("Load data file..."),
            qs.value("fileLocation").toString(), filter, &filter);

        if(filename == "") return;
        clearState();
        if(!m_state->loadFrom(filename)) {
            QMessageBox::warning(this, "Failed to load", "Could not load data file!");
            clearState();
        }

        updateDisplay();
    });
    connect(m->addAction("&Close Data Editor"), &QAction::triggered, [=]() {
        this->deleteLater();
    });

    m_tabs = new QTabWidget(this);
    setLayout(layout);
    layout->addWidget(m_tabs);

    setupStatTab();
    setupCollectionsTab();

    setMinimumSize(800, 600);

    clearState();

    updateDisplay();
}

void DataEditor::clearState() {
    auto newState = new Data::State(m_globalState);
    m_colEdit->setState(newState);
    delete m_state;
    m_state = newState;
}

void DataEditor::setupStatTab() {
    QWidget *statContainer = new QWidget();
    m_tabs->addTab(statContainer, "Stats");
    QVBoxLayout *layout = new QVBoxLayout();
    statContainer->setLayout(layout);

    QTableWidget *stats = new QTableWidget();
    stats->setColumnCount(5);
    layout->addWidget(stats);
    m_statTable = stats;

    QPushButton *addStat = new QPushButton("Add new stat...");
    layout->addWidget(addStat);
}

void DataEditor::reloadStatTab() {
    m_statTable->clear();
    m_statTable->setRowCount(0);
    m_statTable->setHorizontalHeaderLabels(QStringList()
        << "Name"
        << "Short desc"
        << "Long desc"
        << "Tags"
        << "Custom?");

    auto appendRow = [=](Data::Stat *stat, bool custom) {
        int row = m_statTable->rowCount();
        m_statTable->insertRow(row);

        QTableWidgetItem *nameitem = new QTableWidgetItem(stat->name());
        m_statTable->setItem(row, 0, nameitem);
        QTableWidgetItem *shortitem = new QTableWidgetItem(stat->shortDesc());
        m_statTable->setItem(row, 1, shortitem);
        QTableWidgetItem *longitem = new QTableWidgetItem(stat->longDesc());
        m_statTable->setItem(row, 2, longitem);
        QTableWidgetItem *tagsitem = new QTableWidgetItem();
        QStringList tags;
        for(auto tag : stat->tags()) tags << tag;
        tagsitem->setText(tags.join(","));
        m_statTable->setItem(row, 2, tagsitem);
        QTableWidgetItem *customitem = new QTableWidgetItem();
        if(custom) {
            customitem->setCheckState(Qt::Checked);
            customitem->setFlags(customitem->flags() & ~Qt::ItemIsUserCheckable);
        }
        m_statTable->setItem(row, 3, customitem);
    };

    for(auto stat : m_state->stateStats()) {
        appendRow(stat, true);
    }

    const Data::State *wrap = m_state->wrap();

    while(wrap) {
        for(auto stat : wrap->stateStats()) {
            appendRow(stat, false);
        }
        wrap = wrap->wrap();
    }

    m_statTable->resizeColumnsToContents();
}

void DataEditor::setupCollectionsTab() {
    m_colEdit = new DataCollectionEditor();
    m_tabs->addTab(m_colEdit, "Collections");
}

void DataEditor::updateDisplay() {
    reloadStatTab();
    m_colEdit->updateDisplay();
}

#include <QVBoxLayout>
#include <QMenu>
#include <QInputDialog>
#include <QPushButton>
#include <QFormLayout>
#include <QListWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QTableWidget>

#include "EditorTree.h"

#include "data/BaseModifiers.h"

#include "DataCollectionEditor.h"

DataCollectionEditor::DataCollectionEditor() : m_state(nullptr) {
    QVBoxLayout *layout = new QVBoxLayout();
    setLayout(layout);

    QHBoxLayout *filterLine = new QHBoxLayout();
    m_filter = new QLineEdit();
    m_filter->setPlaceholderText("Enter name filter...");
    connect(m_filter, &QLineEdit::textChanged, [=]() {
        updateDisplay();
    });
    filterLine->addWidget(m_filter);

    m_filterMode = new QComboBox();
    m_filterMode->addItem("Name", NameFilter);
    m_filterMode->addItem("Tag", TagFilter);
    filterLine->addWidget(m_filterMode);
    connect(m_filterMode, &QComboBox::currentTextChanged, [=]() {
        updateDisplay();
    });
    layout->addLayout(filterLine);

    m_colTree = new EditorTree();
    m_colTree->setColumnCount(3);
    m_colTree->setHeaderLabels(QStringList() << "Name" << "Desc" << "Tags" << "Custom?");
    m_colTree->setSortingEnabled(true);
    m_colTree->sortByColumn(0, Qt::AscendingOrder);
    layout->addWidget(m_colTree);

    /* CollectionType */
    auto menu = new QMenu();
    connect(menu->addAction("Add tags..."), &QAction::triggered, [=]() {
        QString tags = QInputDialog::getText(this, "Add tags", "Space-separated tags to add:");
        if(tags == "") return;
        for(auto tag : tags.split(" ")) {
            if(tag == "") continue;

            auto col = m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());
            col->addTag(tag);
        }

        updateDisplay();
    });
    connect(menu->addAction("Remove tag..."), &QAction::triggered, [=]() {
        auto col = m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());
        QStringList tags;
        for(auto tag : col->tags()) tags << tag;
        auto sel = QInputDialog::getItem(this, "Remove tag", "Select which to remove:", tags, 0, false);
        if(sel == "") return;

        col->removeTag(sel);

        updateDisplay();
    });
    connect(menu->addAction("Change description"), &QAction::triggered, [=]() {
        auto col = m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());

        bool ok;
        auto desc = QInputDialog::getText(this, "Change description",
            "Description:", QLineEdit::Normal, col->desc(), &ok);
        if(desc == "" || !ok) return;
        col->changeDesc(desc);

        updateDisplay();
    });
    connect(menu->addAction("Duplicate collection"), &QAction::triggered, [=]() {
        auto col = 
            m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());
        auto newcol = new Data::CollectionModifier(col->name() + ":duplicate", col->desc());

        for(auto tag : col->tags()) newcol->addTag(tag);
        for(auto mod : col->collection()) {
            if(auto offset = dynamic_cast<Data::OffsetModifier *>(mod.second)) {
                newcol->addToCollection(mod.first,
                    new Data::OffsetModifier(*offset));
            }
            else if(auto link = dynamic_cast<Data::LinkedModifier *>(mod.second)) {
                newcol->addToCollection(mod.first,
                    new Data::LinkedModifier(*link));
            }
            else {
                // ?!?
                qFatal("Unknown modifier type");
            }
        }
        for(auto map : col->getMap()) {
            newcol->setMap(map.first, map.second);
        }

        m_state->addCollection(newcol);

        updateDisplay();
    });
    connect(menu->addAction("Rename collection"), &QAction::triggered, [=]() {
        auto col =
            m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());
        QString nname = QInputDialog::getText(this, "Rename collection",
            "New name:", QLineEdit::Normal, col->name());
        if(nname == "") return;
        col->rename(nname);
        updateDisplay();
    });
    connect(menu->addAction("Remove collection"), &QAction::triggered, [=]() {
        auto col =
            m_colTree->dataOf<Data::CollectionModifier *>(m_colTree->menuFor());
        //qDebug("Remove collection NYI!");
        m_state->removeCollection(col->name());
    });
    m_colTree->addContextMenu(CollectionType,
        [=](QTreeWidgetItem *) { return menu; });

    /* CollectionMapsType */
    menu = new QMenu();
    connect(menu->addAction("Add map..."), &QAction::triggered, [=]() {
        QString from = QInputDialog::getText(this, "Add map", "Map from:");
        if(from == "") return;
        QString to = QInputDialog::getText(this, "Add map", "Value:");
        if(to == "") return;

        auto col = m_colTree->dataOf<Data::CollectionModifier *>(
            m_colTree->menuFor()->parent());
        col->setMap(from, to);

        updateDisplay();
    });
    m_colTree->addContextMenu(CollectionMapsType,
        [=](QTreeWidgetItem *) { return menu; });

    /* MapType */
    menu = new QMenu();
    connect(menu->addAction("Edit map"), &QAction::triggered, [=]() {
        auto col = m_colTree->dataOf<Data::CollectionModifier *>(
            m_colTree->menuFor()->parent()->parent());
        auto from = m_colTree->dataOf<QString>(
            m_colTree->menuFor());

        QString result = QInputDialog::getText(this, "Edit map", "Map value:",
            QLineEdit::Normal, col->map(from));
        if(result != "") {
            col->setMap(from, result);

            updateDisplay();
        }
    });
    connect(menu->addAction("Remove map"), &QAction::triggered, [=]() {
        auto col = m_colTree->dataOf<Data::CollectionModifier *>(
            m_colTree->menuFor()->parent()->parent());
        auto from = m_colTree->dataOf<QString>(
            m_colTree->menuFor());
        col->clearMap(from);

        updateDisplay();
    });
    m_colTree->addContextMenu(MapType,
        [=](QTreeWidgetItem *) { return menu; });

    /* CollectionModifiersType */
    menu = new QMenu();
    connect(menu->addAction("Add offset modifier"), &QAction::triggered, [=]() {
        QStringList stages;
        for(auto stage : m_state->stages()) stages << stage;
        auto stage = QInputDialog::getItem(this, "Add modifier", "Stage:", stages, 0, false);
        if(stage == "") return;
        auto desc = QInputDialog::getText(this, "Add modifier", "Description:");
        if(desc == "") return;

        auto mod = new Data::OffsetModifier(desc);

        auto col = m_colTree->dataOf<Data::CollectionModifier *>(
            m_colTree->menuFor()->parent());
        col->addToCollection(stage, mod);

        updateDisplay();
    });
    m_colTree->addContextMenu(CollectionModifiersType,
        [=](QTreeWidgetItem *) { return menu; });

    /* generic to all modifiers */
    auto populateModifierMenu = [=](QMenu *menu) {
        connect(menu->addAction("Change description"), &QAction::triggered, [=]() {
            auto mod = m_colTree->dataOf<Data::StatModifier *>(
                m_colTree->menuFor());

            bool ok;
            auto desc = QInputDialog::getText(this, "Change description",
                "Description:", QLineEdit::Normal, mod->desc(), &ok);
            if(desc == "" || !ok) return;
            mod->changeDesc(desc);

            updateDisplay();
        });
        connect(menu->addAction("Change stage"), &QAction::triggered, [=]() {
            QStringList stages;
            for(auto stage : m_state->stages()) stages << stage;
            auto stage = QInputDialog::getItem(this, "Change stage", "Stage:", stages, 0, false);
            if(stage == "") return;

            auto col = m_colTree->dataOf<Data::CollectionModifier *>(
                m_colTree->menuFor()->parent());
            auto mod = m_colTree->dataOf<Data::StatModifier *>(
                m_colTree->menuFor());
            col->removeFromCollection(mod);
            col->addToCollection(stage, mod);

            updateDisplay();
        });
    };

    /* OffsetModifierType */
    menu = new QMenu();
    populateModifierMenu(menu);
    connect(menu->addAction("Add offset"), &QAction::triggered, [=]() {
        QStringList stats;
        const Data::State *state = m_state;
        while(state) {
            for(auto stat : state->stateStats()) stats << stat->name();
            state = state->wrap();
        }
        auto statName = QInputDialog::getItem(this, "Add offset", "Stat:", stats, 0, false);
        if(statName == "") return;
        QString value = QInputDialog::getText(this, "Add offset", "Value:");
        if(value == "") return;

        Data::StatValue sv = Data::StatValue::parse(value);

        auto mod = m_colTree->dataOf<Data::OffsetModifier *>(
            m_colTree->menuFor());
        mod->amount[m_state->getStatByName(statName)] = sv;

        updateDisplay();
    });
    m_colTree->addContextMenu(OffsetModifierType,
        [=](QTreeWidgetItem *) { return menu; });

    /* OffsetModifierOffsetType */
    menu = new QMenu();
    connect(menu->addAction("Remove offset"), &QAction::triggered, [=]() {
        auto mod = m_colTree->dataOf<Data::OffsetModifier *>(
            m_colTree->menuFor()->parent());
        auto stat = m_colTree->dataOf<Data::Stat *>(
            m_colTree->menuFor());
        mod->amount.erase(stat);

        updateDisplay();
    });
    m_colTree->addContextMenu(OffsetModifierOffsetType,
        [=](QTreeWidgetItem *) { return menu; });

    QPushButton *addCollection = new QPushButton("Add empty collection...");
    layout->addWidget(addCollection);
    connect(addCollection, &QPushButton::clicked, [=]() {
        QString name = QInputDialog::getText(this, "Add collection", "Collection name:");
        if(name == "") return;
        QString desc = QInputDialog::getText(this, "Add collection", "Collection description:");
        if(desc == "") return;

        Data::CollectionModifier *col = new Data::CollectionModifier(name, desc);
        m_state->addCollection(col);

        updateDisplay();
    });
    QPushButton *addGear = new QPushButton("Add new gear template...");
    connect(addGear, &QPushButton::clicked, [=]() {
        QDialog *dialog = new QDialog();
        QFormLayout *layout = new QFormLayout();
        dialog->setLayout(layout);

        QLineEdit *name = new QLineEdit();
        name->setPlaceholderText("Will have gear: prepended");
        layout->addRow("Name:", name);
        QLineEdit *desc = new QLineEdit();
        layout->addRow("Desc:", desc);
        QLineEdit *cost = new QLineEdit();
        cost->setValidator(new QIntValidator(-10, 10));
        layout->addRow("Cost:", cost);

        QLineEdit *message = new QLineEdit();
        message->setPlaceholderText("Optional message describing gear effects");
        layout->addRow("Message:", message);

        QComboBox *cat = new QComboBox();
        for(auto gc : m_state->getCollectionsByTag("gearcat")) {
            cat->addItem(gc->desc(), gc->map("gearcat"));
        }
        layout->addRow("Category:", cat);

        QListWidget *tags = new QListWidget();
        std::set<QString> allTags;
        for(auto g : m_state->getCollectionsByTag("gear")) {
            for(auto t : g->tags()) allTags.insert(t);
        }
        for(auto t : allTags) tags->addItem(t);
        for(int i = 0; i < tags->count(); i ++) {
            tags->item(i)->setCheckState(Qt::Unchecked);
        }
        layout->addRow("Tags:", tags);

        QCheckBox *offset = new QCheckBox("Offset?");
        offset->setChecked(false);
        layout->addRow("Create gear offset modifier?", offset);

        QComboBox *stage = new QComboBox();
        for(auto s : m_state->stages()) {
            stage->addItem(s);
        }
        layout->addRow("Offset modifier stage:", stage);

        QPushButton *addOffset = new QPushButton("Add new offset");
        layout->addRow(addOffset);

        QTableWidget *offsetContainer = new QTableWidget();
        layout->addRow("Offsets:", offsetContainer);

        offsetContainer->setColumnCount(2);
        offsetContainer->setVerticalHeaderLabels(QStringList() << "Stat" << "Amount");

        connect(addOffset, &QPushButton::clicked, [=]() {
            int row = offsetContainer->rowCount();
            QTableWidgetItem *emptyRow = new QTableWidgetItem();
            offsetContainer->setRowCount(row+1);
            offsetContainer->setItem(row, 1, emptyRow);

            QComboBox *statselect = new QComboBox();
            statselect->addItem("");
            for(auto s : m_state->allStats()) statselect->addItem(s->name());
            offsetContainer->setCellWidget(row, 0, statselect);

            offsetContainer->resizeColumnToContents(0);
        });

        QPushButton *create = new QPushButton("Create");
        layout->addRow(create);
        connect(create, &QPushButton::clicked, [=]() { dialog->accept(); });

        while(true) {
            int ret = dialog->exec();
            if(ret == QDialog::Rejected) {
                delete dialog;
                return;
            }

            // TODO: check data!
            auto col = new Data::CollectionModifier("gear:" + name->text(), desc->text());
            col->addTag("gear");
            col->addTag(cat->currentData().toString());
            for(auto t : tags->selectedItems()) {
                col->addTag(t->data(Qt::UserRole).toString());
            }

            col->setMap("cost", QString::number(cost->text().toInt()));

            if(message->text().length() > 0) col->setMap("message", message->text());

            if(offset->checkState() == Qt::Checked) {
                auto offset = new Data::OffsetModifier(desc->text());
                col->addToCollection(stage->currentText(), offset);
                for(int i = 0; i < offsetContainer->rowCount(); i ++) {
                    auto stat = static_cast<QComboBox *>(offsetContainer->cellWidget(i, 0));
                    auto statname = stat->currentText();
                    if(statname == "") continue;

                    QString value = offsetContainer->item(i, 1)->text();
                    if(value != "") {
                        offset->amount[m_state->getStatByName(statname)]
                            = Data::StatValue::parse(value);
                    }
                }
            }

            m_state->addCollection(col);

            break;
        }

        delete dialog;
        updateDisplay();
    });
    layout->addWidget(addGear);
    QPushButton *addTrait = new QPushButton("Add new trait template...");
    layout->addWidget(addTrait);
}

void DataCollectionEditor::setState(Data::State *state) {
    m_colTree->clear();
    m_state = state;
}

void DataCollectionEditor::updateDisplay() {
    std::vector<Data::CollectionModifier *> allCollections
        = m_state->getCollections();

    m_colTree->synchronize(m_colTree->invisibleRootItem(), allCollections,
        CollectionType);

    m_colTree->setSortingEnabled(false);
    for(int i = 0; i < m_colTree->topLevelItemCount(); i ++) {
        updateCollection(m_colTree->topLevelItem(i));
    }
    m_colTree->setSortingEnabled(true);

    // filter
    auto filter = m_filter->text();
    int mode = m_filterMode->currentData().toInt();
    for(int i = 0; i < m_colTree->topLevelItemCount(); i ++) {
        auto item = m_colTree->topLevelItem(i);
        if(filter == "") item->setHidden(false);
        else if(mode == NameFilter) item->setHidden(item->text(0).indexOf(filter) == -1);
        else if(mode == TagFilter) {
            item->setHidden(
                !m_colTree->dataOf<Data::CollectionModifier *>(item)->hasTag(filter));
        }
        else item->setHidden(true);
    }

    m_colTree->resizeColumnToContents(0);
}

void DataCollectionEditor::updateCollection(QTreeWidgetItem *item) {
    auto col = m_colTree->dataOf<Data::CollectionModifier *>(item);
    item->setText(0, col->name());
    item->setText(1, col->desc());
    QStringList tags;
    for(auto tag : col->tags()) tags << tag;
    item->setText(2, tags.join(","));
    // do we need to fill the item?
    QTreeWidgetItem *maps, *mods;
    if(item->childCount() == 0) {
        maps = new QTreeWidgetItem(QStringList() << "Maps");
        m_colTree->setTypeOf(maps, CollectionMapsType);
        mods = new QTreeWidgetItem(QStringList() << "Modifiers");
        m_colTree->setTypeOf(mods, CollectionModifiersType);

        item->addChild(maps);
        item->addChild(mods);
    }
    else {
        maps = item->child(0);
        mods = item->child(1);
    }

    std::vector<QString> mapkeys;
    for(auto x : col->getMap()) mapkeys.emplace_back(x.first);
    m_colTree->synchronize(maps, mapkeys, MapType);
    for(int i = 0; i < maps->childCount(); i ++) {
        auto mitem = maps->child(i);
        auto from = m_colTree->dataOf<QString>(mitem);
        mitem->setText(0, from);
        mitem->setText(1, col->map(from));
    }

    std::vector<Data::StatModifier *> modlist;
    for(auto x : col->collection()) {
        modlist.push_back(x.second);
    }
    m_colTree->synchronize(mods, modlist, ModifierType);
    for(int i = 0; i < mods->childCount(); i ++) {
        auto mitem = mods->child(i);
        auto mod = m_colTree->dataOf<Data::StatModifier *>(mitem);
        updateModifier(mitem);

        if(mitem->text(1) == "") {
            for(auto x : col->collection()) {
                if(x.second == mod) { mitem->setText(1, x.first); break; }
            }
        }
    }
}

void DataCollectionEditor::updateModifier(QTreeWidgetItem *item) {
    auto mod = m_colTree->dataOf<Data::StatModifier *>(item);
    if(auto offset = dynamic_cast<Data::OffsetModifier *>(mod)) {
        item->setText(0, mod->desc() + " [Offset]");
        m_colTree->setTypeOf(item, OffsetModifierType);

        std::vector<Data::Stat *> statList;
        for(auto x : offset->amount) statList.push_back(x.first);
        m_colTree->synchronize(item, statList, OffsetModifierOffsetType);

        for(int i = 0; i < item->childCount(); i ++) {
            auto citem = item->child(i);
            auto stat = m_colTree->dataOf<Data::Stat *>(citem);
            citem->setText(0, stat->name());
            citem->setText(1, offset->amount[stat].format());
        }
    }
    else if(auto link = dynamic_cast<Data::LinkedModifier *>(mod)) {
        item->setText(0, mod->desc() + " [Link]");
        QTreeWidgetItem *source, *target, *mult;
        if(item->childCount() == 0) {
            mult = new QTreeWidgetItem();
            mult->setText(0, "Multiplier");
            source = new QTreeWidgetItem();
            source->setText(0, "Source");
            target = new QTreeWidgetItem();
            target->setText(0, "Target");

            item->addChild(mult);
            item->addChild(source);
            item->addChild(target);
        }
        else {
            item->sortChildren(0, Qt::AscendingOrder);
            mult = item->child(0);
            source = item->child(1);
            target = item->child(2);
        }

        mult->setText(1, QString::number(link->multiplier()));
        source->setText(1, link->source()->name());
        target->setText(1, link->target()->name());
    }
    else {
        item->setText(0, mod->desc() + "[Unhandled!]");
    }
}

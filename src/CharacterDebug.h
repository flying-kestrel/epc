#pragma once

#include <QWidget>
#include <QDialog>

#include "data/Character.h"

class QTreeWidgetItem;

class CharacterDebug : public QDialog {
private:
    Data::State *m_state;
public:
    CharacterDebug(Data::State *state);

private:
    QWidget *stats();
    void formatModifier(QTreeWidgetItem *item, Data::StatModifier *modifier);
    QWidget *modifiers();
    QWidget *collections();
};

#include <QPushButton>
#include <QGridLayout>
#include <QComboBox>
#include <QListWidget>
#include <QFormLayout>
#include <QLabel>
#include <QInputDialog>

#include "SelectionDialog.h"

SelectionDialog::SelectionDialog(Data::State *state, QString cattag,
    QString filterString, ItemFilterFunction itemFilter) : m_state(state) {

    setup(cattag, filterString, itemFilter);
}

void SelectionDialog::setup(QString cattag, QString filterString,
    ItemFilterFunction itemFilter) {

    QGridLayout *layout = new QGridLayout();
    setLayout(layout);

    QComboBox *filter = new QComboBox();
    layout->addWidget(filter, 0, 1, 1, 3);

    auto colcats = m_state->getCollectionsByTag(cattag);

    filter->addItem("All", "");

    std::vector<std::pair<QString, QString>> cats;
    for(auto cat : colcats) {
        cats.push_back(std::make_pair(cat->map("category"), cat->desc()));
    }
    std::sort(cats.begin(), cats.end());

    if(filterString == "") {
        for(auto cat : cats) {
            filter->addItem(cat.second, cat.first);
        }
    }
    else {
        for(auto str : filterString.split(" ", QString::SkipEmptyParts)) {
            QString desc;
            for(auto c : cats) {
                if(c.first == str) desc = c.second;
            }
            filter->addItem(desc, str);
        }
    }

    filter->setCurrentIndex(0);

    QListWidget *select = new QListWidget();
    layout->addWidget(select, 1, 1, 3, 3);

    for(auto cat : cats) {
        for(auto m : m_state->getCollectionsByTag(cat.first)) {
            if(itemFilter && !itemFilter(m)) continue;

            auto item = new QListWidgetItem();
            item->setText(m->desc());
            item->setData(Qt::UserRole, m->name());

            select->addItem(item);
        }
    }

    select->sortItems();

    QFormLayout *detailsLayout = new QFormLayout();
    layout->addLayout(detailsLayout, 1, 4, 2, 3);

    auto name = new QLabel("");
    name->setMinimumWidth(200);
    detailsLayout->addRow("Name:", name);

    auto cost = new QLabel("");
    detailsLayout->addRow("Cost:", cost);

    auto effects = new QLabel("");
    detailsLayout->addRow("Details:", effects);

    connect(select, &QListWidget::itemSelectionChanged, [=](){
        auto sellist = select->selectedItems();
        if(sellist.size() != 1) return;

        auto item = sellist[0];

        auto colname = item->data(Qt::UserRole).toString();
        auto col = m_state->getCollectionByName(colname);

        name->setText(col->desc());

        // details
        QString ge = "";
        Data::State st(m_state->wrap());
        col->addToState(&st);
        auto cs = st.calculate();
        for(auto s : cs.values) {
            if(s.second == Data::StatValue()) continue;
            if(s.first->hasTag("internal")) continue;
            if(s.first->hasTag("buildpoint")) continue;

            auto sources = st.linkSources(s.first);
            bool delta = false;
            for(auto source : sources) {
                if(s.second != cs.values[source]) delta = true;
            }
            if(!delta && sources.size()) continue;

            if(ge.length() > 0) ge += "\n";
            ge += s.first->longDesc() + ": " + s.second.format();
        }
        col->removeFromState(&st);

        if(col->map("cost") != "") cost->setText(col->map("cost"));
        else {
            cost->setText("");
            for(auto bpstat : st.getStatsByTags(
                QStringList() << "buildpoint" << "bp:used", false)) {

                if(cs.values.count(bpstat) == 0) continue;
                cost->setText(cost->text()
                    + (cs.values[bpstat] * -1).format() + " "
                    + bpstat->longDesc());
            }
        }
        if(col->map("message") != "") {
            if(ge.length() > 0) ge += "\n";
            ge += col->map("message");
        }
        effects->setText(ge);
    });

    connect(filter, &QComboBox::currentTextChanged, [=]() {
        auto filterString = filter->currentData().toString();
        for(int i = 0; i < select->count(); i ++) {
            auto item = select->item(i);
            if(!item) continue;

            if(filterString == "") item->setHidden(false);
            else {
                auto userdata = item->data(Qt::UserRole).toString();
                auto col = m_state->getCollectionByName(userdata);

                item->setHidden(!col->hasTag(filterString));
            }
        }
    });


    QPushButton *cancel = new QPushButton("Cancel");
    connect(cancel, SIGNAL(clicked()), this, SLOT(reject()));
    layout->addWidget(cancel, 4, 1);

    QPushButton *add = new QPushButton("Add");
    connect(add, &QPushButton::clicked, [=]() {
        auto sellist = select->selectedItems();
        if(sellist.size() != 1) return;
        auto sel = sellist[0];
        m_chosen = sel->data(Qt::UserRole).toString();

        auto col = m_state->getCollectionByName(m_chosen);
        if(col->map("customize").length()) {
            m_chosen = customize(m_chosen);
            if(m_chosen == "") {
                reject();
                return;
            }
        }

        accept();
    });
    layout->addWidget(add, 4, 3);
}

QString SelectionDialog::customize(QString prototype) {
    QString nname;
    Data::Stat *source = nullptr;
    QString suffix;

    auto col = m_state->getCollectionByName(prototype);
    auto customize = col->map("customize");

    if(customize == "rep") {
        bool ok = false;
        QStringList reps;

        auto replist = m_state->getStatsByTag("rep");
        for(auto rep : replist) reps.push_back(rep->longDesc());

        reps.sort();

        QString sel = QInputDialog::getItem(
            this, "Customize", "Rep:", reps, 0, false, &ok);
        if(!ok || sel == "") return "";

        nname = prototype + ":" + sel;

        for(auto rep : replist) if(rep->longDesc() == sel) source = rep;
    }
    else if(customize == "string") {
        bool ok = false;

        QString sel = QInputDialog::getText(
            this, "Customize", "Subject:", QLineEdit::Normal, "", &ok);
        if(!ok || sel == "") return "";

        suffix = " [" + sel + "]";
        nname = prototype + ":" + sel.replace(' ', "").toLower();
    }
    else if(customize == "skill") {
        bool ok = false;
        QStringList skills;

        auto skilllist = m_state->getStatsByTag("skill");
        for(auto skill : skilllist) skills.push_back(skill->longDesc());

        skills.sort();

        QString sel = QInputDialog::getItem(
            this, "Customize", "Skill:", skills, 0, false, &ok);
        if(!ok || sel == "") return "";

        for(auto skill : skilllist) if(skill->longDesc() == sel) source = skill;
        if(!source) qFatal("Logic error! Skill doesn't exist?");
        nname = prototype + ":" + source->name();
    }
    else if(customize == "knowskill") {
        bool ok = false;
        QStringList skills;

        auto skilllist = m_state->getStatsByTag("know");
        for(auto skill : skilllist) skills.push_back(skill->longDesc());

        skills.sort();

        QString sel = QInputDialog::getItem(
            this, "Customize", "Skill:", skills, 0, false, &ok);
        if(!ok || sel == "") return "";

        for(auto skill : skilllist) if(skill->longDesc() == sel) source = skill;
        if(!source) qFatal("Logic error! Skill doesn't exist?");
        nname = prototype + ":" + source->name();
    }
    else {
        qFatal("customization type \"%s\" NYI", qPrintable(customize));
    }

    // exists?
    if(!m_state->stateHasCollection(nname)) {
        if(source) suffix = " [" + source->longDesc() + "]";

        auto ncol = new Data::CollectionModifier(nname, col->desc() + suffix);
        for(auto tag : col->tags()) ncol->addTag(tag);
        ncol->addTag("customized");

        for(auto x : col->collection()) {
            ncol->addToCollection(x.first, x.second);
        }
        for(auto x : col->getMap()) {
            if(x.second == "") continue;

            if(x.first == "message") ncol->setMap(x.first, x.second + suffix);
            else ncol->setMap(x.first, x.second);
        }

        auto cstage = col->map("customize:stage");
        if(col->map("customize:offset").length()) {
            auto by = col->map("customize:offset").toInt();
            auto mod = new Data::OffsetModifier(col->desc() + suffix);
            mod->amount[source] = Data::StatValue(by);
            ncol->addToCollection(cstage, mod);
        }

        m_state->addCollection(ncol);
    }
    else {
        // no need to create.
    }

    return nname;
}

#pragma once

#include <functional>

#include <QWidget>
#include <QVBoxLayout>

#include "data/State.h"

#include "StatDisplayColumn.h"

class StatLabel;
class QTextStream;

class StatDisplay : public QWidget { Q_OBJECT
public:
    typedef std::function<bool (Data::Stat *)> DisplayFilter;
private:
    Data::State *m_state;
    QString m_lastStage;
    std::vector<QString> m_tags;

    std::set<Data::Stat *> m_lastStats;
    std::vector<QString> m_statLabels;
    QVBoxLayout *m_layout;

    std::vector<StatDisplayColumn *> m_columns;
    DisplayFilter m_displayFilter;
public:
    StatDisplay(Data::State *state, QString lastStage = "");

    Data::State *state() const { return m_state; }

    void addTag(const QString &tag) { m_tags.push_back(tag); }

    void addColumn(StatDisplayColumn *col)
        { m_columns.push_back(col); }

    void setDisplayFilter(DisplayFilter filter) { m_displayFilter = filter; }

    void updateDisplay();

    void textDump(QTextStream &stream);
private:
    void removeOldStats(std::set<Data::Stat *> &newStats);
    void addNewStats(std::set<Data::Stat *> &newStats);
};

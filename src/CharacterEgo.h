#pragma once

#include "CharacterWidget.h"

#include "data/Ego.h"
#include "data/Character.h"
#include "TraitProxy.h"

class CharacterView;
class EditorTree;
class QTreeWidgetItem;
class QSpinBox;

class CharacterEgo : public CharacterWidget {
private:
    enum ItemType {
        NoType,
        /* data 0: Ego Fixed index, data 1: summary text */
        AdjustableContainer,
        /* data 0: Data::Stat pointer */
        StatItem,
        TraitContainer,
        Trait,
        SkillContainer,
        Skill
    };
private:
    EditorTree *m_egoTree;

    QTreeWidgetItem *m_sleights[2];

    QWidget *m_tabOrder;
    TraitProxy m_traitProxy;
    bool m_synchronizing = false;
public:
    CharacterEgo(CharacterView *cv);

    virtual void textDump(QTextStream &) {}

    virtual void updateDisplay();
private:
    void updateItem(Data::CalculatedStats &cs, QTreeWidgetItem *item);
    void updateAdjustableContainer(Data::CalculatedStats &cs,
        QTreeWidgetItem *item);
    void updateStat(Data::CalculatedStats &cs, QTreeWidgetItem *item);
    void updateTraitContainer(QTreeWidgetItem *item);
    void updateTrait(QTreeWidgetItem *item);
    void updateSkillContainer(Data::CalculatedStats &cs, QTreeWidgetItem *item);
    void updateSkill(Data::CalculatedStats &cs, QTreeWidgetItem *item);

    void containerSetup(Data::Ego::FixedOffset which,
        QString label, QString summary, QString tag);
    void setupAdjuster(Data::Stat *stat, Data::Ego::FixedOffset offset,
        QSpinBox *adjuster);

    void flexSetup();
    void traitSetup();
    void skillSetup();

    void addSkillField(Data::Stat *stat);
    void addSkillSpec(Data::Stat *stat);
};

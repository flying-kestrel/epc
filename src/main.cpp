#include <QApplication>

#include "RootWindow.h"

int main(int argc, char *argv[]) {
    QApplication qapp(argc, argv);

    QCoreApplication::setOrganizationName("epc");
    QCoreApplication::setOrganizationDomain("zephyr.ethv.net");
    QCoreApplication::setApplicationName("epc");

    RootWindow rw;
    rw.show();

    return qapp.exec();
}

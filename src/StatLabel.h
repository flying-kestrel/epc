#pragma once

#include <QLabel>

#include "data/Stat.h"

namespace Data {
class Stat;
} // namespace Data

class StatLabel : public QLabel { Q_OBJECT
private:
    Data::Stat *m_stat;
public:
    StatLabel(Data::Stat *stat);// : QLabel("TBU"), m_stat(stat) {}
public slots:
    void updateValue(Data::CalculatedStats &map);
};

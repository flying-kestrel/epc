#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QToolButton>

#include "data/Template.h"

#include "CharacterBPSummary.h"

#include "moc_CharacterBPSummary.cpp"

CharacterBPSummary::CharacterBPSummary(const Data::State *state,
    Data::OffsetModifier *bpTransforms,
    QString display,
    const std::vector<CharacterBPSummary::Transform> &transforms)
    : m_state(state), m_display(display) {

    setContentsMargins(0,0,0,0);
    QHBoxLayout *layout = new QHBoxLayout();
    setLayout(layout);

    m_label = new QLabel();
    layout->addWidget(m_label);

    for(auto transform : transforms) {
        QToolButton *cft = new QToolButton();
        cft->setArrowType(Qt::LeftArrow);
        layout->addWidget(cft);

        auto label = new QLabel();
        layout->addWidget(label);

        QToolButton *ctf = new QToolButton();
        ctf->setArrowType(Qt::RightArrow);
        layout->addWidget(ctf);

        auto from = m_state->getStatByName(transform.from);
        auto to = m_state->getStatByName(transform.target);

        label->setText(
            QString::number(transform.fromAmount) + " " + from->shortDesc()
            + " : "
            + QString::number(transform.targetAmount) + " " + to->shortDesc());

        connect(cft, &QPushButton::clicked, [=]() {
            if(bpTransforms->amount[from].die(0) > 0) {
                bpTransforms->amount[from] += Data::StatValue(-transform.fromAmount);
                bpTransforms->amount[to] += Data::StatValue(-transform.targetAmount);

                emit amountChanged();
            }
        });

        connect(ctf, &QPushButton::clicked, [=]() {
            bpTransforms->amount[from] += Data::StatValue(transform.fromAmount);
            bpTransforms->amount[to] += Data::StatValue(transform.targetAmount);

            emit amountChanged();
        });
    }

    layout->addStretch(1);
}

void CharacterBPSummary::updateDisplay() {
    m_label->setText(Data::Template::expand(m_state, m_display));
}

#pragma once

#include "data/State.h"

class DataInvariants {
private:
    Data::State *m_state;
    bool m_ok;
public:
    bool run(Data::State *state);
private:
    void checkGearCosts();
    void checkMorphReferences();
    void checkTraitCustomize();
};

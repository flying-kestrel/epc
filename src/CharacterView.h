#pragma once

#include <QWidget>

#include "data/Character.h"

class QComboBox;

class CharacterView : public QWidget { Q_OBJECT
private:
    Data::Character * const m_character;

    Data::Ego *m_currentEgo;
    Data::Morph *m_currentMorph;

    QComboBox *m_egoSelect, *m_morphSelect;
    bool m_updating;
public:
    CharacterView(Data::Character *character);

    const Data::Character *character() const { return m_character; }
    bool updating() const { return m_updating; }

    Data::State *charState() const { return m_character->state(); }
    Data::Ego *ego() const { return m_currentEgo; }
    Data::Morph *morph() const { return m_currentMorph; }

    void updateView();

    Data::CalculatedStats calculate() const;
    Data::CalculatedStats calculate(Data::Ego *ego, Data::Morph *morph) const;
signals:
    void performUpdate();
};

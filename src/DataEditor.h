#pragma once

#include <QTabWidget>

#include "data/State.h"
#include "EditorTree.h"

class QTableWidget;
class QTreeWidget;
class QTreeWidgetItem;
class DataCollectionEditor;

class DataEditor : public QWidget {
private:
private:
    Data::State *m_globalState, *m_state;
    QTabWidget *m_tabs;

    QTableWidget *m_statTable;
    DataCollectionEditor *m_colEdit;
    EditorTree *m_colTree;
public:
    DataEditor(Data::State *globalState);
private:
    void clearState();

    void setupStatTab();
    void reloadStatTab();

    void setupCollectionsTab();

    void updateDisplay();
};

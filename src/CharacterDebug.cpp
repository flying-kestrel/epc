#include <QGridLayout>
#include <QTreeWidget>
#include <QTabWidget>
#include <QComboBox>
#include <QTableWidget>

#include "data/State.h"

#include "CharacterDebug.h"

CharacterDebug::CharacterDebug(Data::State *state)
    : m_state(state) {

    QGridLayout *layout = new QGridLayout();
    setLayout(layout);

    QTabWidget *tabs = new QTabWidget();
    layout->addWidget(tabs, 0, 0);

    tabs->addTab(stats(), "Stats");
    tabs->addTab(modifiers(), "Modifiers");
    tabs->addTab(collections(), "Collections");
}

QWidget *CharacterDebug::stats() {
    QWidget *root = new QWidget();
    QGridLayout *layout = new QGridLayout();
    root->setLayout(layout);
    QComboBox *stages = new QComboBox();

    for(auto s : m_state->stages()) {
        stages->addItem(s);
    }

    layout->addWidget(stages, 1, 1);

    QTableWidget *values = new QTableWidget();
    values->setColumnCount(5);
    values->setVerticalHeaderLabels(QStringList() << "Name" << "Short desc" << "Long desc" << "Value" << "Derivation");
    layout->addWidget(values, 2, 0, 1, 2);

    connect(stages, &QComboBox::currentTextChanged, [=](const QString &stage) {
        values->clear();

        /* XXX */
        Data::CalculatedStats cs = m_state->calculateTo(m_state->wrapGenerator, stage);

        int row = 0;
        for(auto stat : cs.values) {
            values->setRowCount(row+1);
            values->setItem(row, 0, new QTableWidgetItem(stat.first->name()));
            values->setItem(row, 1, new QTableWidgetItem(stat.first->shortDesc()));
            values->setItem(row, 2, new QTableWidgetItem(stat.first->longDesc()));
            values->setItem(row, 3, new QTableWidgetItem(stat.second.format()));
            values->setItem(row, 4, new QTableWidgetItem(cs.derivation[stat.first]));

            row ++;
        }

        values->sortItems(0);
        values->resizeColumnsToContents();
    });

    return root;
}

void CharacterDebug::formatModifier(QTreeWidgetItem *item, Data::StatModifier *modifier) {
    if(auto link = dynamic_cast<Data::LinkedModifier *>(modifier)) {
        new QTreeWidgetItem(item,
            QStringList()
                << "Link"
                << link->desc()
                << link->source()->name()
                << link->target()->name()
                << "x" + QString::number(link->multiplier()));
    }
    else if(auto offset = dynamic_cast<Data::OffsetModifier *>(modifier)) {
        auto offsetItem = new QTreeWidgetItem(item,
            QStringList()
                << "Offsets"
                << offset->desc());
        for(auto amount : offset->amount) {
            new QTreeWidgetItem(offsetItem,
                QStringList() << amount.first->name() << amount.second.format());
        }
    }
    else {
        new QTreeWidgetItem(item, QStringList() << "Unknown modifier type!");
    }
}

QWidget *CharacterDebug::modifiers() {
    QWidget *root = new QWidget();
    QGridLayout *layout = new QGridLayout();
    root->setLayout(layout);

    QTreeWidget *tree = new QTreeWidget();
    tree->setColumnCount(5);
    layout->addWidget(tree, 1, 1);

    for(auto stage : m_state->stages()) {
        auto mods = m_state->stageModifiers(stage);
        QTreeWidgetItem *stageItem = new QTreeWidgetItem(tree, QStringList() << stage);

        for(auto mod : mods) {
            formatModifier(stageItem, mod);
        }
    }

    connect(tree, &QTreeWidget::itemExpanded, [=]() {
        for(int i = 0; i < tree->columnCount(); i ++) {
            tree->resizeColumnToContents(i);
        }
    });

    return root;
}

QWidget *CharacterDebug::collections() {
    QWidget *root = new QWidget();
    QGridLayout *layout = new QGridLayout();
    root->setLayout(layout);

    QTreeWidget *tree = new QTreeWidget();
    tree->setColumnCount(5);
    layout->addWidget(tree, 1, 1);

    for(auto col : m_state->getCollections()) {
        auto colitem = new QTreeWidgetItem(tree,
            QStringList() << col->name() << col->desc());

        const Data::State *s = m_state;
        bool active = false;
        while(s) {
            if(col->active(s)) active = true;
            s = s->wrap();
        }
        if(active) colitem->setBackgroundColor(0, Qt::lightGray);

        std::map<QString, std::vector<Data::StatModifier *>> stages;
        for(auto mod : col->collection()) {
            stages[mod.first].push_back(mod.second);
        }

        for(auto x : stages) {
            auto stageitem = new QTreeWidgetItem(colitem, QStringList() << x.first);
            for(auto mod : x.second) formatModifier(stageitem, mod);
        }
    }

    connect(tree, &QTreeWidget::itemExpanded, [=]() {
        for(int i = 0; i < tree->columnCount(); i ++) {
            tree->resizeColumnToContents(i);
        }
    });

    return root;
}

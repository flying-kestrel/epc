#pragma once

#include <QDialog>

#include "data/State.h"

class SelectionDialog : public QDialog {
public:
    typedef std::function<bool (Data::CollectionModifier *)> ItemFilterFunction;
private:
    QString m_chosen;
    Data::State *m_state;
public:
    SelectionDialog(Data::State *state, QString cattag, QString filterString,
        ItemFilterFunction itemFilter = nullptr);

    QString chosen() const { return m_chosen; }

    static QStringList costs(Data::State *state, QString colname);
    static QStringList effects(Data::State *state, QString colname);
private:
    void setup(QString cattag, QString filterString,
        ItemFilterFunction itemFilter);
    QString customize(QString prototype);
};

#include <QHBoxLayout>
#include <QPushButton>
#include <QStyle>
#include <QApplication>

#include "StatDisplayColumn.h"
#include "StatLabel.h"
#include "AdjustBy.h"
#include "CharacterWidget.h"

void StatDisplayValueColumn::populate(Data::State *state, QHBoxLayout *layout,
    Data::Stat *stat) {

    auto label = new StatLabel(stat);
    m_labels.insert(label);
    layout->addWidget(label);
    label->setMinimumWidth(50);

    connect(label, &QObject::destroyed, [=](QObject *ptr) {
        m_labels.erase(static_cast<StatLabel *>(ptr));
    });
}

void StatDisplayValueColumn::updateDisplay(Data::State *state) {
    auto cs = state->calculate();
    for(auto label : m_labels) label->updateValue(cs);
}

void StatDisplayAdjustColumn::populate(Data::State *state, QHBoxLayout *layout,
    Data::Stat *stat) {

    if(!shouldShow(stat)) return;

    // for style info
    QWidget temp;
    
    QPushButton *increase =
        new QPushButton(temp.style()->standardIcon(QStyle::SP_ArrowUp), "");
    QPushButton *decrease =
        new QPushButton(temp.style()->standardIcon(QStyle::SP_ArrowDown), "");

    layout->addWidget(increase);
    layout->addWidget(decrease);

    QObject::connect(increase, &QPushButton::clicked, [=](){
        adjustWrapper(state, stat, +1);
    });
    QObject::connect(decrease, &QPushButton::clicked, [=](){
        adjustWrapper(state, stat, -1);
    });
}

void StatDisplayAdjustColumn::adjustWrapper(Data::State *state, Data::Stat *stat, int dir) {
    int by = AdjustBy::modifier() * dir;

    // default limits
    int max = 100, min = 0;
    #if 0
    Data::CalculatedStats cs;
    sd->state()->calculateTo("limits-stage");
    auto maxname = stat->name() + "-max";
    auto maxstat = sd->state()->getStatByName(maxname);
    if(maxstat) max = cs.values[maxstat].die(0);
    auto minname = stat->name() + "-min";
    auto minstat = sd->state()->getStatByName(minname);
    if(minstat) min = cs.values[minstat].die(0);
    #endif

    int old_value = m_modifier->amount[stat].die(0);
    int new_value = old_value + by;
    if(new_value < min) new_value = min;
    if(new_value > max) new_value = max;

    adjust(state, stat, new_value - old_value);
    m_widget->updateDisplay();
}

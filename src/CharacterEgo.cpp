#include <algorithm>

#include <QMessageBox>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QMenu>
#include <QSpinBox>
#include <QPushButton>

#include "CharacterEgo.h"
#include "data/Template.h"

#include "EditorTree.h"
#include "CharacterView.h"
#include "SelectionDialog.h"

CharacterEgo::CharacterEgo(CharacterView *cv) : CharacterWidget(cv) {
    m_egoTree = new EditorTree();
    m_egoTree->setColumnCount(6);

    m_egoTree->setSortingEnabled(true);
    m_egoTree->sortByColumn(0, Qt::AscendingOrder);

    QVBoxLayout *layout = new QVBoxLayout();
    setLayout(layout);
    layout->addWidget(m_egoTree);

    containerSetup(Data::Ego::Aptitudes, "Aptitudes",
        "${aptitude:used}/${aptitude:total}", "aptitude");
    containerSetup(Data::Ego::Reps, "Reps", "${rep:used}/${rep:total}", "rep");

    connect(m_cv, &CharacterView::performUpdate, [=]() { updateDisplay(); });

    flexSetup();
    traitSetup();
    skillSetup();
}

void CharacterEgo::updateDisplay() {
    if(m_cv->ego() == nullptr) {
        qDebug("updateDisplay(), NULL ego...");
        return;
    }
    if(m_synchronizing) return;

    m_synchronizing = true;
    m_egoTree->setSortingEnabled(false);
    m_tabOrder = m_egoTree;
    m_cv->updateView();

    Data::CalculatedStats cs = m_cv->calculate();

    int psiLevel = cs.values[m_cv->charState()->getStatByName("trait:psi-level")].die(0);
    for(int i = 0; i < psiLevel && i < 2; i ++) m_sleights[i]->setHidden(false);
    for(int i = psiLevel; i < 2; i ++) m_sleights[i]->setHidden(true);

    updateItem(cs, m_egoTree->invisibleRootItem());

    for(int i = 0; i < m_egoTree->columnCount(); i ++) {
        m_egoTree->resizeColumnToContents(i);
    }
    m_synchronizing = false;
    m_egoTree->setSortingEnabled(true);
}

void CharacterEgo::updateItem(Data::CalculatedStats &cs,
    QTreeWidgetItem *item) {

    int type = m_egoTree->typeOf(item);
    switch(type) {
    case AdjustableContainer:
        updateAdjustableContainer(cs, item);
        break;
    case StatItem:
        updateStat(cs, item);
        return;
    case TraitContainer:
        updateTraitContainer(item);
        break;
    case Trait:
        updateTrait(item);
        return;
    case SkillContainer:
        updateSkillContainer(cs, item);
        break;
    case Skill:
        updateSkill(cs, item);
        return;
    default:
        break;
    }
    for(int i = 0; i < item->childCount(); i ++) updateItem(cs, item->child(i));
}

void CharacterEgo::updateAdjustableContainer(Data::CalculatedStats &cs,
    QTreeWidgetItem *item) {

    item->setText(2,
        Data::Template::expand(m_cv->charState(), cs,
        m_egoTree->dataOf<QString, 1>(item)));
}

void CharacterEgo::updateStat(Data::CalculatedStats &cs,
    QTreeWidgetItem *item) {

    auto stat = m_egoTree->dataOf<Data::Stat *>(item);
    bool isAdjustable = (m_egoTree->typeOf(item->parent()) == AdjustableContainer);
    // setup?
    if(item->text(0) == 0) {
        item->setText(0, stat->longDesc());

        if(isAdjustable) {
            QSpinBox *adjuster = new QSpinBox();
            m_egoTree->setItemWidget(item, 1, adjuster);
            setupAdjuster(stat, static_cast<Data::Ego::FixedOffset>(
                m_egoTree->dataOf<int>(item->parent())), adjuster);
        }
    }

    if(isAdjustable) {
        QSpinBox *adjuster
            = static_cast<QSpinBox *>(m_egoTree->itemWidget(item, 1));
        adjuster->setValue(cs.values[stat].die(0));
        QWidget::setTabOrder(m_tabOrder, adjuster);
        m_tabOrder = adjuster;
    }
    else {
        item->setText(1, cs.values[stat].format());
    }
}

void CharacterEgo::updateTraitContainer(QTreeWidgetItem *item) {
    std::vector<TraitProxy::Proxy *> traits;

    QString tag = m_egoTree->dataOf<QString>(item);

    for(auto t : m_cv->ego()->traits()) {
        if(tag == ""
            || m_cv->charState()->getCollectionByName(t)->hasTag(tag)) {

            traits.push_back(m_traitProxy.get(t));
        }
    }

    m_egoTree->synchronize(item, traits, Trait);
}

void CharacterEgo::updateTrait(QTreeWidgetItem *item) {
    auto traitProxy = m_egoTree->dataOf<TraitProxy::Proxy *>(item);
    auto trait = m_cv->charState()->getCollectionByName(traitProxy->name);
    if(item->text(0) == "") {
        item->setText(0, trait->desc());
    }
}

void CharacterEgo::updateSkillContainer(Data::CalculatedStats &cs,
    QTreeWidgetItem *item) {

    std::vector<Data::Stat *> skillList
        = m_cv->charState()->getStatsByTag("skill");

    m_egoTree->synchronize(item, skillList, Skill);

    item->setText(2,
        Data::Template::expand(m_cv->charState(), cs,
        "Active: ${active-skills:used}/${active-skills:total}"
        " Know: ${know-skills:used}/${know-skills:total}"));
}

void CharacterEgo::updateSkill(Data::CalculatedStats &cs,
    QTreeWidgetItem *item) {

    auto stat = m_egoTree->dataOf<Data::Stat *>(item);

    if(item->text(0) == "") {
        item->setText(0, stat->longDesc());

        // add / adjuster / value of skill
        if(stat->hasTag("specskill")) {
            // constant, for a specialization
            item->setText(1, "10");
        }
        else if(stat->hasTag("field")) {
            QPushButton *addField = new QPushButton(
                style()->standardIcon(QStyle::SP_FileDialogNewFolder), "");
            addField->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            m_egoTree->setItemWidget(item, 1, addField);

            connect(addField, &QPushButton::clicked, [=]() {
                addSkillField(stat);
            });
        }
        else {
            QSpinBox *adjuster = new QSpinBox();
            m_egoTree->setItemWidget(item, 1, adjuster);
            setupAdjuster(stat, Data::Ego::BaseSkills, adjuster);
        }

        // specialization button
        if(!stat->hasTag("field") && !stat->hasTag("specskill")) {
            QPushButton *specialize = new QPushButton(style()->standardIcon(
                QStyle::SP_FileDialogDetailedView), "");
            m_egoTree->setItemWidget(item, 3, specialize);
            connect(specialize, &QPushButton::clicked, [=]() {
                addSkillSpec(stat);
            });
        }

        // remove button
        if(stat->hasTag("subskill")) {
            QPushButton *remove = new QPushButton(
                style()->standardIcon(QStyle::SP_TrashIcon), "");
            m_egoTree->setItemWidget(item, 4, remove);
            connect(remove, &QPushButton::clicked, [=]() {
                // two possibilities: has an adjuster, or doesn't.
                if(stat->hasTag("specskill")) {
                    m_cv->ego()->fixed(Data::Ego::SpecSkills)->amount.erase(stat);
                    auto cp = m_cv->charState()->getStatByName("cp:used");
                    m_cv->ego()->fixed(Data::Ego::SpecSkills)->amount[cp] += Data::StatValue(-1);
                }
                else {
                    QSpinBox *adjuster =
                        static_cast<QSpinBox *>(m_egoTree->itemWidget(item, 1));
                    adjuster->setValue(0);
                    m_cv->ego()->fixed(Data::Ego::BaseSkills)->amount.erase(stat);
                }

                updateDisplay();
            });
        }
    }

    if(stat->hasTag("specskill")) {
    }
    else if(stat->hasTag("field")) {
    }
    else {
        QSpinBox *adjuster = static_cast<QSpinBox *>(m_egoTree->itemWidget(item, 1));
        const auto &amount = m_cv->ego()->fixed(Data::Ego::BaseSkills)->amount;
        auto it = amount.find(stat);//[stat].die(0)
        if(it == amount.end()) adjuster->setValue(0);
        else adjuster->setValue(it->second.die(0));
    }

    if(!stat->hasTag("field")) {
        item->setText(2, cs.values[stat].format());
        item->setToolTip(2, cs.derivation[stat]);
    }

    auto ego = m_cv->ego();
    if(stat->hasTag("specskill")) {
        item->setHidden(ego->fixed(Data::Ego::SpecSkills)->amount.count(stat) == 0);
    }
    else if(stat->hasTag("subskill")) {
        item->setHidden(ego->fixed(Data::Ego::BaseSkills)->amount.count(stat) == 0);
    }
}

void CharacterEgo::containerSetup(Data::Ego::FixedOffset which,
    QString label, QString summary, QString tag) {

    auto container = new QTreeWidgetItem();
    m_egoTree->addTopLevelItem(container);
    m_egoTree->setTypeOf(container, AdjustableContainer);
    m_egoTree->setDataOf<0>(container, which);
    m_egoTree->setDataOf<1>(container, summary);
    container->setText(0, label);

    std::vector<Data::Stat *> stats
        = m_cv->charState()->getStatsByTag(tag);

    /* sort stats by name */
    std::sort(stats.begin(), stats.end(),
        [](const auto &a, const auto &b) { return a->name() > b->name(); });
    m_egoTree->synchronize(container, stats, StatItem);
}

void CharacterEgo::setupAdjuster(Data::Stat *stat,
    Data::Ego::FixedOffset offset, QSpinBox *adjuster) {

    adjuster->setMinimum(0);

    Data::Stat *tracker = nullptr;
    int multiplier = 1;
    switch(offset) {
    case Data::Ego::Aptitudes:
        tracker = m_cv->charState()->getStatByName("aptitude:used");
        adjuster->setMaximum(30);
        break;
    case Data::Ego::Reps:
        tracker = m_cv->charState()->getStatByName("rep:used");
        adjuster->setMaximum(100);
        break;
    case Data::Ego::BaseSkills:
        if(stat->hasTag("active")) {
            tracker = m_cv->charState()->getStatByName("active-skills:used");
        }
        else {
            tracker = m_cv->charState()->getStatByName("know-skills:used");
        }
        adjuster->setMaximum(100);
        break;
    case Data::Ego::Pools:
        tracker = m_cv->charState()->getStatByName("cp:used");
        adjuster->setMinimum(1);
        adjuster->setMaximum(3);
        multiplier = 2;
        break;
    default:
        tracker = m_cv->charState()->getStatByName("cp:used");
        break;
    }

    connect(adjuster, QOverload<int>::of(&QSpinBox::valueChanged), [=](int to) {
        if(m_synchronizing) return;

        Data::StatMap &statmap = m_cv->ego()->fixed(offset)->amount;
        int oldValue = statmap[stat].die(0);
        statmap[stat] = Data::StatValue(to);
        int delta = to - oldValue;
        if(tracker) {
            statmap[tracker] += Data::StatValue(delta * multiplier);
        }

        updateDisplay();
    });
}

void CharacterEgo::flexSetup() {
    auto egoFlexContainer = new QTreeWidgetItem(QStringList() << "Ego pools");
    m_egoTree->setTypeOf(egoFlexContainer, AdjustableContainer);
    m_egoTree->setDataOf(egoFlexContainer, Data::Ego::Pools);
    m_egoTree->addTopLevelItem(egoFlexContainer);
    std::vector<Data::Stat *> flex;
    flex.push_back(m_cv->charState()->getStatByName("pool:flex"));
    m_egoTree->synchronize(egoFlexContainer, flex, StatItem);
}

void CharacterEgo::traitSetup() {
    // add ego traits container
    auto egoTraits = new QTreeWidgetItem(QStringList() << "Ego traits");
    m_egoTree->setTypeOf(egoTraits, TraitContainer);
    m_egoTree->setDataOf<0>(egoTraits, QString("trait:ego"));
    m_egoTree->setDataOf<1>(egoTraits, QString("traitcat:ego"));
    m_egoTree->addTopLevelItem(egoTraits);

    m_sleights[0] = new QTreeWidgetItem(QStringList() << "Psi-Chi sleights");
    m_egoTree->setTypeOf(m_sleights[0], TraitContainer);
    m_egoTree->setDataOf<0>(m_sleights[0], QString("psi:chi"));
    m_egoTree->setDataOf<1>(m_sleights[0], QString("traitcat:psi:chi"));
    m_egoTree->addTopLevelItem(m_sleights[0]);
    m_sleights[1] = new QTreeWidgetItem(QStringList() << "Psi-Gamma sleights");
    m_egoTree->setTypeOf(m_sleights[1], TraitContainer);
    m_egoTree->setDataOf<0>(m_sleights[1], QString("psi:gamma"));
    m_egoTree->setDataOf<1>(m_sleights[1], QString("traitcat:psi:gamma"));
    m_egoTree->addTopLevelItem(m_sleights[1]);

    /* TraitContainerType */
    auto menu = new QMenu();
    connect(menu->addAction("Add..."), &QAction::triggered, [=]() {
        auto category = m_egoTree->dataOf<QString, 1>(m_egoTree->menuFor());
        // exclude trait categories from the current ego
        QStringList exclude;
        exclude << "trait:nondefault";
        for(auto trait : m_cv->ego()->traits()) {
            auto col = m_cv->ego()->state()->getCollectionByName(trait);
            auto it = col->getMap().find("category");
            if(it == col->getMap().end()) continue;
            exclude << it->second;
        }

        SelectionDialog sd(m_cv->charState(), category, "",
            [=](Data::CollectionModifier *col) {

            for(auto tag : exclude) if(col->hasTag(tag)) return false;
            return true;
        });
        if(sd.exec() != QDialog::Accepted) return;

        m_cv->ego()->addTrait(sd.chosen());
        updateDisplay();
    });
    m_egoTree->addContextMenu(TraitContainer, [=](QTreeWidgetItem *) { return menu; });

    /* TraitType */
    menu = new QMenu();
    auto removeTrait = menu->addAction("Remove Trait");
    connect(removeTrait, &QAction::triggered, [=]() {
        auto proxy = m_egoTree->dataOf<TraitProxy::Proxy *>(m_egoTree->menuFor());

        m_cv->ego()->removeTrait(proxy->name);
        updateDisplay();
    });
    m_egoTree->addContextMenu(Trait, [=](QTreeWidgetItem *item) {
        auto proxy = m_egoTree->dataOf<TraitProxy::Proxy *>(item);
        removeTrait->setDisabled(proxy->fixed);
        return menu;
    });
}

void CharacterEgo::skillSetup() {
    // root skill container
    auto skillContainer = new QTreeWidgetItem(QStringList() << "Skills");
    m_egoTree->setTypeOf(skillContainer, SkillContainer);
    m_egoTree->addTopLevelItem(skillContainer);
}

void CharacterEgo::addSkillField(Data::Stat *stat) {
    QDialog *dialog = new QDialog();

    QFormLayout *layout = new QFormLayout();
    dialog->setLayout(layout);

    QComboBox *link = new QComboBox();
    layout->addRow("Aptitude:", link);
    auto apts = m_cv->charState()->getStatsByTag("aptitude");
    std::sort(apts.begin(), apts.end(),
        [](const auto &a, const auto &b) { return a->name() > b->name(); });

    for(auto apt : apts) {
        if(stat->hasTag("field-" + apt->name())) {
            link->addItem(apt->longDesc(), apt->name());
        }
    }

    QComboBox *name = new QComboBox();
    name->setEditable(true);
    layout->addRow("Name:", name);
    name->addItem("");

    auto subskills = m_cv->charState()->getStatsByTag("subskill");
    for(auto ss : subskills) {
        if(ss->name().startsWith(stat->name())) {
            name->addItem(ss->shortDesc(), ss->name());
        }
    }

    connect(name, QOverload<int>::of(&QComboBox::currentIndexChanged),
        [=](int index) {

        QString statname = name->itemData(index).toString();
        if(statname == "") return;

        auto stat = m_cv->charState()->getStatByName(statname);
        auto linkFrom = m_cv->charState()->linkSources(stat);
        for(auto s : linkFrom) {
            if(s->hasTag("aptitude")) {
                link->setCurrentText(s->longDesc());
            }
        }
    });
    name->setCurrentIndex(0);

    QPushButton *create = new QPushButton("&Create");
    layout->addRow(create);
    connect(create, &QPushButton::clicked, [=]() { dialog->accept(); });

    if(dialog->exec() != QDialog::Accepted) {
        delete dialog;
        return;
    }

    // build new stat
    auto subname = name->currentText();
    auto substatname
        = stat->name() + ":" + name->currentText().replace(' ', "").toLower();
    Data::Stat *substat = nullptr;
    bool exists = m_cv->charState()->statExists(substatname);
    auto apt = m_cv->charState()->getStatByName(link->currentData().toString());
    if(!exists) {
        substat = new Data::Stat(
            substatname,
            subname, 
            stat->longDesc() + " [" + subname + "]");

        /* transfer/create tags */
        substat->addTag("skill");
        substat->addTag("subskill");
        if(stat->hasTag("active")) substat->addTag("active");
        if(stat->hasTag("know")) substat->addTag("know");

        m_cv->charState()->addStat(substat);
        m_cv->character()->createSubskill(substat, stat, apt);
    }
    else substat = m_cv->charState()->getStatByName(substatname);

    delete dialog;

    m_cv->ego()->fixed(Data::Ego::BaseSkills)->amount[substat] = Data::StatValue();

    updateDisplay();
}

void CharacterEgo::addSkillSpec(Data::Stat *stat) {
    auto state = m_cv->charState();

    /* build dialog */
    QDialog dialog;
    dialog.setModal(true);

    QFormLayout *layout = new QFormLayout();
    dialog.setLayout(layout);

    QComboBox *name = new QComboBox();
    layout->addRow("Specialization:", name);
    name->setEditable(true);
    name->addItem("");

    auto subskills = state->getStatsByTag("subskill");
    for(auto ss : subskills) {
        if(ss->name().startsWith(stat->name())) {
            name->addItem(ss->shortDesc(), ss->name());
        }
    }

    QPushButton *add = new QPushButton("&Add");
    layout->addRow(add);
    connect(add, SIGNAL(clicked()), &dialog, SLOT(accept()));

    /* show dialog and wait for non-empty name */
    while(1) {
        if(!dialog.exec()) return;

        if(name->currentText() == "") continue;
        break;
    }

    /* build new Stat */
    auto subname = name->currentText();
    auto substatname = stat->name() + ":" + name->currentText().replace(' ', "").toLower();
    /* don't re-create */
    bool exists = state->statExists(substatname);
    Data::Stat *substat;
    if(!exists) {
        substat = new Data::Stat(
            substatname,
            subname,
            stat->longDesc() + " (" + subname + ")");

        /* create tags */
        substat->addTag("skill");
        substat->addTag("subskill");
        substat->addTag("specskill");
        state->addStat(substat);

        /* add link modifier */
        m_cv->character()->createSubskill(substat, stat, stat);
    }
    else {
        substat = state->getStatByName(substatname); 
    }

    auto spec = m_cv->ego()->fixed(Data::Ego::SpecSkills);

    if(spec->amount[substat] != Data::StatValue()) {
        QMessageBox::information(this, "Specialization already exists",
            "The specialization " + subname + " already exists!");
        return;
    }
    spec->amount[substat] = Data::StatValue(10);
    auto cp = state->getStatByName("cp:used");
    spec->amount[cp] += Data::StatValue(1);

    updateDisplay();
    
}

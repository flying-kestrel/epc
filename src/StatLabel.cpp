#include <QToolTip>

#include "StatLabel.h"

#include "moc_StatLabel.cpp"

StatLabel::StatLabel(Data::Stat *stat) : QLabel("TBU"), m_stat(stat) {
}

void StatLabel::updateValue(Data::CalculatedStats &cs) {
    if(cs.values.count(m_stat) == 0) {
        setText("<stat '" + m_stat->name() + "' not present>");
        return;
    }
    setText(cs.values[m_stat].format());
    setToolTip(cs.derivation[m_stat]);
}

#pragma once

#include <functional>
#include <set>
#include <type_traits>

#include <QTreeWidget>

class EditorTree : public QTreeWidget {
public:
    static const int typeRole = Qt::UserRole + 0;
    static const int dataRole = Qt::UserRole + 1;
private:
    std::map<int, std::function<QMenu * (QTreeWidgetItem *)>> m_contextMenus;
    bool m_synchronizing;
    QTreeWidgetItem *m_menuFor;
public:
    EditorTree();

    void addContextMenu(int type,
        const std::function<QMenu * (QTreeWidgetItem *)> &menu) {

        m_contextMenus[type] = menu;
    }

    /* item data management */

    QTreeWidgetItem *menuFor() const { return m_menuFor; }

    bool synchronizing() const { return m_synchronizing; }
    int typeOf(QTreeWidgetItem *item) const {
        bool ok;
        int ret = item->data(0, typeRole).toInt(&ok);
        if(!ok) return -1;
        return ret;
    }

    void setTypeOf(QTreeWidgetItem *item, int type) {
        item->setData(0, typeRole, type);
    }

    // QVariant data
    template<typename T, int n = 0>
    typename std::enable_if<std::is_same<T, QVariant>::value, T>::type
    dataOf(QTreeWidgetItem *item) const {
        return item->data(0, dataRole + n);
    }

    // QString data
    template<typename T, int n = 0>
    typename std::enable_if<std::is_same<T, QString>::value, T>::type
    dataOf(QTreeWidgetItem *item) const {
        return item->data(0, dataRole + n).toString();
    }

    // pointer data
    template<typename T, int n = 0>
    typename std::enable_if<std::is_pointer<T>::value, T>::type
    dataOf(QTreeWidgetItem *item) const {
        return reinterpret_cast<T>(
            item->data(0, dataRole + n).value<uintptr_t>());
    }

    // integral data
    template<typename T, int n = 0>
    typename std::enable_if<std::is_integral<T>::value, T>::type
    dataOf(QTreeWidgetItem *item) const {
        return item->data(0, dataRole + n).value<T>();
    }

    template<int n = 0>
    void setDataOf(QTreeWidgetItem *item, QString data) {
        item->setData(0, dataRole + n, data);
    }

    template<int n = 0>
    void setDataOf(QTreeWidgetItem *item, QVariant data) {
        item->setData(0, dataRole + n, data);
    }

    template<int n = 0>
    void setDataOf(QTreeWidgetItem *item, const int &data) {
        item->setData(0, dataRole + n, data);
    }

    template<typename T, int n = 0>
    void setDataOf(QTreeWidgetItem *item, T *data) {
        item->setData(0, dataRole + n,
            QVariant::fromValue(reinterpret_cast<uintptr_t>(data)));
    }

    template<typename T>
    void synchronize(QTreeWidgetItem *item,
        const std::vector<T> &values, int type, int offset=0);
};

template<typename T>
void EditorTree::synchronize(QTreeWidgetItem *item,
    const std::vector<T> &values, int type, int offset) {

    m_synchronizing = true;

    std::set<T> toAdd;
    for(auto &x : values) toAdd.insert(x);

    std::vector<int> toRemove;
    for(int i = offset; i < item->childCount(); i ++) {
        auto ci = item->child(i);
        auto data = dataOf<T>(ci);
        int itype = typeOf(ci);

        if(itype != type) continue;

        if(toAdd.count(data) == 0) toRemove.push_back(i);
        toAdd.erase(data);
    }

    while(toRemove.size()) {
        delete item->takeChild(toRemove.back());
        toRemove.pop_back();
    }

    for(auto a : toAdd) {
        QTreeWidgetItem *ci = new QTreeWidgetItem();
        setTypeOf(ci, type);
        setDataOf(ci, a);
        ci->setExpanded(true);
        item->addChild(ci);
        item->setExpanded(true);
    }

    m_synchronizing = false;
}

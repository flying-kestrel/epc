#pragma once

#include <QWidget>

#include "data/State.h"

class EditorTree;
class QLineEdit;
class QComboBox;
class QTreeWidgetItem;

class DataCollectionEditor : public QWidget {
public:
    enum CollectionItemType {
        NoType,
        CollectionType,
        CollectionMapsType,
        MapType,
        CollectionModifiersType,
        ModifierType,
        OffsetModifierType,
        OffsetModifierOffsetType,
    };
    enum FilterMode {
        NameFilter,
        TagFilter
    };
private:
    Data::State *m_state;
    QLineEdit *m_filter;
    QComboBox *m_filterMode;
    EditorTree *m_colTree;
public:
    DataCollectionEditor();

    void setState(Data::State *state);

    void updateDisplay();
protected:
    void updateCollection(QTreeWidgetItem *item);
    void updateModifier(QTreeWidgetItem *item);
};

#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>

#include "CharacterView.h"

#include "moc_CharacterView.cpp"

CharacterView::CharacterView(Data::Character *character)
    : m_character(character) {

    m_currentEgo = nullptr;
    m_currentMorph = nullptr;

    QHBoxLayout *layout = new QHBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    m_egoSelect = new QComboBox();
    layout->addWidget(m_egoSelect);
    connect(m_egoSelect,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [=](int to) {

        if(m_updating) return;

        auto toName = m_egoSelect->itemData(to).toString();

        m_currentEgo = m_character->ego(toName);
        updateView();
    });

    m_morphSelect = new QComboBox();
    layout->addWidget(m_morphSelect);
    connect(m_morphSelect,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [=](int to) {

        if(m_updating) return;

        auto toPtr = m_morphSelect->itemData(to).value<uintptr_t>();
        m_currentMorph = reinterpret_cast<Data::Morph *>(toPtr);
        updateView();
    });

    m_updating = false;
    updateView();

    m_egoSelect->setCurrentIndex(0);
}

void CharacterView::updateView() {
    if(m_updating) return;
    m_updating = true;
    /* update egos */
    auto egoCurrentText = m_egoSelect->currentText();

    m_egoSelect->clear();
    auto egos = m_character->egoMap();
    for(auto ego : egos) {
        m_egoSelect->addItem(ego.second->col()->desc(), ego.second->name());
    }

    /* update morphs */
    auto morphCurrentText = m_morphSelect->currentText();

    m_morphSelect->clear();
    m_morphSelect->addItem("<no morph>");
    for(auto ln : m_character->locationList()) {
        for(auto morph : m_character->location(ln)->morphs()) {
            m_morphSelect->addItem(ln + ": " + morph.first,
                QVariant::fromValue(reinterpret_cast<uintptr_t>(morph.second)));
        }
    }

    m_egoSelect->setCurrentIndex(m_egoSelect->findText(egoCurrentText));
    m_morphSelect->setCurrentIndex(m_morphSelect->findText(morphCurrentText));
    emit performUpdate();
    m_updating = false;
}

Data::CalculatedStats CharacterView::calculate() const {
    return calculate(m_currentEgo, m_currentMorph);
}

Data::CalculatedStats CharacterView::calculate(Data::Ego *ego, Data::Morph *morph) const {
    auto egoState = ego ? ego->state() : nullptr;
    auto morphState = morph ? morph->state() : nullptr;

    // XXX: this is horrendously inefficient.
    if(morph) morph->syncGear();

    Data::State::StateGenerator generator =
        [&](const Data::State *state) -> const Data::State * {

        if(state == morphState) return egoState ? egoState : state->wrap();
        else return state->wrap();
    };

    if(morphState) return morphState->calculate(generator);
    else if(egoState) return egoState->calculate();
    else return m_character->state()->calculate();
}

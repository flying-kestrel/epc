#pragma once

#include <QWidget>
#include <QTextStream>

#include "data/Stat.h"

class CharacterView;

class CharacterWidget : public QWidget {
private:
    bool m_active = false;
protected:
    CharacterView *m_cv;
public:
    CharacterWidget() : m_cv(nullptr) {}
    CharacterWidget(CharacterView *cv) : m_cv(cv) {}

    virtual void textDump(QTextStream &stream) = 0;

    /* XXX: should be pure virtual... */
    virtual void updateDisplay() {}
};

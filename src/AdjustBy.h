#pragma once

#include <QApplication>

class AdjustBy {
public:
    static int modifier() {
        int by = 5;
        auto kmod = QGuiApplication::keyboardModifiers();
        if(kmod & Qt::ShiftModifier) by = 10;
        else if(kmod & Qt::ControlModifier) by = 1;
        return by;
    }
};

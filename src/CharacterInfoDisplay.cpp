#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>

#include "CharacterInfoDisplay.h"
#include "CharacterView.h"
#include "EditorTree.h"

#include "data/Template.h"

CharacterInfoDisplay::CharacterInfoDisplay(CharacterView *cv)
    : CharacterWidget(cv), m_wantsEgo(false), m_wantsLocation(false),
    m_wantsMorph(false) {}

void CharacterInfoDisplay::setup(QString desc) {
    if(wantLocation() && wantMorph()) qDebug("location and morph both wanted!");

    setContentsMargins(0,0,0,0);
    QVBoxLayout *outerLayout = new QVBoxLayout();
    m_outer = new QFrame();
    m_outer->setFrameStyle(QFrame::Panel | QFrame::Raised);
    m_outer->setLineWidth(2);
    outerLayout->addWidget(m_outer);
    setLayout(outerLayout);

    QVBoxLayout *layout = new QVBoxLayout();

    auto header = new QLineEdit();
    header->setText(desc);
    layout->addWidget(header);

    QHBoxLayout *topLayout = new QHBoxLayout();
    layout->addLayout(topLayout);

    auto toggle = new QPushButton("Toggle");
    connect(toggle, &QPushButton::clicked, [=]() {
        m_container->setVisible(!m_container->isVisible());
    });
    topLayout->addWidget(toggle);
    topLayout->addStretch(1);

    if(wantEgo()) {
        QComboBox *ego = new QComboBox();
        connect(ego,
            QOverload<int>::of(&QComboBox::currentIndexChanged),
            [=](int to) {

            if(m_cv->updating()) return;

            m_ego = ego->itemData(to).toString();
            updateDisplay();
        });

        connect(m_cv, &CharacterView::performUpdate, [=]() {
            ego->clear();
            ego->addItem("<default ego>", "");
            auto egos = m_cv->charState()->getCollectionsByTag("ego");
            for(auto e : egos) {
                QString name = e->name();
                name.remove(0, 4);
                ego->addItem(e->desc(), name);
            }
            int index = ego->findData(m_ego);
            if(index == -1) index = 0;
            ego->setCurrentIndex(index);
            // may be extraneous updates
            updateDisplay();
        });
        topLayout->addWidget(ego);
    }
    if(wantLocation()) {
        QComboBox *location = new QComboBox();
        connect(location,
            QOverload<const QString &>::of(&QComboBox::currentIndexChanged),
            [=](const QString &to) {

            if(m_cv->updating()) return;

            m_location = to;
            updateDisplay();
        });

        connect(m_cv, &CharacterView::performUpdate, [=]() {
            location->clear();
            for(auto l : m_cv->character()->locationList()) {
                location->addItem(l);
            }
            int index = location->findText(m_location);
            if(index == -1) index = 0;
            location->setCurrentIndex(index);
            // may be extraneous updates
            updateDisplay();
        });
        topLayout->addWidget(location);
    }
    if(wantMorph()) {
        QComboBox *morph = new QComboBox();
        connect(morph,
            QOverload<int>::of(&QComboBox::currentIndexChanged),
            [=](int to) {

            if(m_cv->updating()) {
                return;
            }

            auto d = morph->itemData(to).value<QStringList>();

            m_location = d[0];
            m_morph = d[1];
            updateDisplay();
        });

        connect(m_cv, &CharacterView::performUpdate, [=]() {
            morph->clear();

            morph->addItem("<default morph>",
                QStringList() << "" << "");

            for(auto ln : m_cv->character()->locationList()) {
                for(auto m : m_cv->character()->location(ln)->morphs()) {
                    morph->addItem(ln + ": " + m.first,
                        QStringList() << ln << m.first);
                }
            }
            int index = morph->findData(QStringList() << m_location << m_morph);
            if(index == -1) index = 0;
            morph->setCurrentIndex(index);

            // may be extraneous updates
            updateDisplay();
        });
        topLayout->addWidget(morph);
    }

    m_container = new QWidget();
    layout->addWidget(m_container);
    m_outer->setLayout(layout);
}

void CharacterInfoDisplay::updateDisplay() {
    Data::Ego *ego = nullptr;
    Data::Location *location = nullptr;
    Data::Morph *morph = nullptr;
    bool ok = true;
    do {
        if(wantEgo()) {
            if(m_ego == "") ego = m_cv->ego();
            else {
                ego = m_cv->character()->ego(m_ego);
            }
            if(!ego) { ok = false; break; }
        }
        if(wantMorph() && m_morph == "") {
            morph = m_cv->morph();
            if(!morph) { ok = false; break; }
        }
        else if(wantLocation() || wantMorph()) {
            if(m_location == "" && m_cv->character()->locationList().size() > 0) {
                location = m_cv->character()->location(
                    m_cv->character()->locationList()[0]);
            }
            else {
                location = m_cv->character()->location(m_location);
            }
            if(!location) { ok = false; break; }
        }
        if(wantMorph() && m_morph != "") {
            auto it = location->morphs().find(m_morph);
            if(it == location->morphs().end()) { ok = false; break; }
            morph = it->second;
        }

        m_container->setEnabled(true);

        auto cs = m_cv->calculate(ego, morph);
        updateInfoDisplay(cs, ego, location, morph);
        return;
    } while(false);

    if(!ok) {
        m_container->setEnabled(false);
    }
}

void CharacterInfoLabelDisplay::setup(QString desc) {
    CharacterInfoDisplay::setup(desc);

    auto layout = new QVBoxLayout();
    layout->setContentsMargins(0,0,0,0);
    m_label = new QLabel();
    layout->addWidget(m_label);
    container()->setLayout(layout);
}

void CharacterInfoTemplateDisplay::updateInfoDisplay(
    const Data::CalculatedStats &cs, Data::Ego *, Data::Location *, Data::Morph *) {

    label()->setText(Data::Template::expand(m_cv->charState(), cs, m_template));
    // label()->setMinimumHeight(20);
}

void CharacterInfoStatDisplay::setup(QString desc) {
    CharacterInfoDisplay::setup(desc);
    m_layout = new QVBoxLayout();
    m_tree = new EditorTree();
    m_tree->setColumnCount(3);
    m_tree->setHeaderHidden(true);
    m_tree->setColumnHidden(2, true);
    m_tree->sortByColumn(2, Qt::AscendingOrder);

    m_layout->addWidget(m_tree);

    container()->setLayout(m_layout);
}

void CharacterInfoStatDisplay::updateInfoDisplay(
    const Data::CalculatedStats &cs, Data::Ego *, Data::Location *,
    Data::Morph *) {

    std::vector<Data::Stat *> stats
        = m_cv->charState()->getStatsByTags(m_tags, m_disjunction);
    std::sort(stats.begin(), stats.end(),
        [](const auto &x, const auto &y) { return x->name() > y->name(); });
    m_tree->synchronize(m_tree->invisibleRootItem(), stats, 0);
    m_tree->setSortingEnabled(false);

    for(int i = 0; i < m_tree->topLevelItemCount(); i ++) {
        auto item = m_tree->topLevelItem(i);
        auto stat = m_tree->dataOf<Data::Stat *>(item);
        item->setText(0, stat->longDesc());
        item->setText(2, stat->name());
        auto it = cs.values.find(stat);
        if(it == cs.values.end()) {
            item->setText(1, "<not present>");
            item->setHidden(true);
        }
        else {
            item->setText(1, it->second.format());
            auto dit = cs.derivation.find(stat);
            if(dit == cs.derivation.end()) {
                qDebug("Value but no derivation for %s",
                    qPrintable(stat->name()));
            }
            else item->setToolTip(1, dit->second);
        }

        if(m_onlyDeltas) {
            bool show = true;
            auto sources = m_cv->charState()->linkSources(stat);
            for(auto source : sources) {
                auto sourceit = cs.values.find(source);
                if(sourceit->second == it->second) show = false;
            }
            item->setHidden(!show);
        }
    }
    m_tree->setSortingEnabled(true);

    // resize to fit everything, size is no object!
    std::vector<QTreeWidgetItem *> search;
    int height = 0;
    search.push_back(m_tree->invisibleRootItem());
    while(search.size()) {
        auto next = search.back(); search.pop_back();
        int nh = m_tree->visualItemRect(next).height();
        height += nh;

        for(int i = 0; i < next->childCount(); i ++) {
            search.push_back(next->child(i));
        }
    }

    if(height < 0) height = 0;

    // +10 for some padding... TODO: don't magic-number this
    m_tree->setMinimumHeight(height + 10);
    m_tree->resizeColumnToContents(0);
}

void CharacterInfoMessageDisplay::setup(QString desc) {
    CharacterInfoDisplay::setup(desc);
    QVBoxLayout *layout = new QVBoxLayout();
    //m_list = new EditorTree();
    //m_list->setHeaderHidden(true);
    m_label = new QLabel();
    //layout->addWidget(m_list);
    layout->addWidget(m_label);

    container()->setLayout(layout);
}

void CharacterInfoMessageDisplay::updateInfoDisplay(
    const Data::CalculatedStats &, Data::Ego *ego, Data::Location *,
    Data::Morph *morph) {

    std::vector<QString> messages;

    auto process = [&](Data::State *state) {
        for(auto col : state->getCollections()) {
            if(!col->active(state)) continue;
            auto msg = col->map("message");
            if(msg.length() > 0) {
                messages.push_back("[" + col->desc() + "]:\n\t" + msg);
            }
        }
    };

    if(ego) process(ego->state());
    if(morph) process(morph->state());

    std::sort(messages.begin(), messages.end());
    QStringList msgList = QStringList(
        QList<QString>::fromVector(QVector<QString>::fromStdVector(messages)));
    m_label->setText(msgList.join("\n"));

    return;

    m_list->setWordWrap(true);
    m_list->synchronize(m_list->invisibleRootItem(), messages, 0);
    for(int i = 0; i < m_list->topLevelItemCount(); i ++) {
        auto item = m_list->topLevelItem(i);
        item->setText(0, m_list->dataOf<QString>(item));
    }

    // resize to fit everything, size is no object!
    std::vector<QTreeWidgetItem *> search;
    int height = 0;
    search.push_back(m_list->invisibleRootItem());
    while(search.size()) {
        auto next = search.back(); search.pop_back();
        int nh = m_list->visualItemRect(next).height();
        height += nh;

        for(int i = 0; i < next->childCount(); i ++) {
            search.push_back(next->child(i));
        }
    }

    if(height < 0) height = 0;

    // +10 for some padding... TODO: don't magic-number this
    m_list->setMinimumHeight(height + 10);
    m_list->resizeColumnToContents(0);
}

#include <QVBoxLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QLabel>
#include <QInputDialog>
#include <QMessageBox>
#include <QFormLayout>

#include "CharacterCareer.h"
#include "CharacterView.h"
#include "StatLabel.h"

#include "moc_CharacterCareer.cpp"

CharacterCareer::CharacterCareer(CharacterView *cv) : CharacterWidget(cv) {
    m_displayStack = new QStackedWidget();
    auto layout = new QVBoxLayout();
    setLayout(layout);
    layout->addWidget(m_displayStack);

    auto createContainer = new QWidget();
    auto createLayout = new QVBoxLayout();
    createContainer->setLayout(createLayout);
    auto careerButton = new QPushButton("Move to career mode!");

    connect(careerButton, &QPushButton::clicked, [=]() {
        if(!m_cv->ego()) {
            qDebug("Somehow no ego in move-to-career mode?");
            return;
        }

        /* do some buildpoint checks */
        auto state = m_cv->ego()->state();
        auto cs = state->calculate();
        std::map<QString, int> used;
        for(auto bp : state->getStatsByTag("bp:used")) {
            used[bp->longDesc()] = cs.values[bp].die(0);
        }
        std::map<QString, int> allowed;
        for(auto bp : state->getStatsByTag("bp:total")) {
            allowed[bp->longDesc()] = cs.values[bp].die(0);
        }

        if(used.size() != allowed.size()) {
            qWarning("Logic error: different amounts of buildpoint types used and available.");
            return;
        }

        bool warned = false;
        for(auto bp : used) {
            if(bp.second > allowed[bp.first]) {
                QMessageBox::warning(this, "Buildpoint warning",
                    QString("Used more %1 than allowed!\n").arg(bp.first));
                warned = true;
            }
            else if(bp.second < allowed[bp.first]) {
                QMessageBox::warning(this, "Buildpoint warning",
                    QString("Used less %1 than allowed!\n").arg(bp.first));
                warned = true;
            }
        }

        if(warned) {
            auto result = QMessageBox::question(this, "Buildpoint warning",
                "Still want to move to career mode?");
            if(result != QMessageBox::Yes) return;
        }

        auto careerBPOff = new Data::OffsetModifier();
        m_cv->ego()->col()->addToCollection("setup-stage", careerBPOff);
        // XXX: hack, add to current state because it's brand-new...
        m_cv->charState()->addModifier("setup-stage", careerBPOff);

        careerBPOff->amount[state->getStatByName("rp:total")] = 0;
        m_cv->ego()->col()->setMap("career", "true");

        // will update view
        syncBPOff();
    });

    createLayout->addWidget(careerButton);
    createLayout->addStretch();
    m_displayStack->addWidget(createContainer);

    auto careerContainer = new QWidget();
    auto careerLayout = new QVBoxLayout();
    careerContainer->setLayout(careerLayout);
    m_displayStack->addWidget(careerContainer);

    auto rpContainer = new QFormLayout();
    careerLayout->addLayout(rpContainer);
    auto rpLabel = new StatLabel(m_cv->charState()->getStatByName("rp:total"));
    rpContainer->addRow("Rez points: ", rpLabel);
    connect(this, &CharacterCareer::updateLabelsWith, rpLabel, &StatLabel::updateValue);

    auto spend = new QPushButton("Spend rez points");
    auto rpSpend = [=](QString label, const char *statName, int amount) -> QWidget * {
        auto button = new QPushButton(label);
        connect(button, &QPushButton::clicked, [=]() {
            // only do anything if we actually have RP to spend
            if(careerBPOff()->amount[m_cv->charState()->getStatByName("rp:total")].die(0) == 0) return;

            if(QMessageBox::question(this, "Spend RP", "Are you sure?") != QMessageBox::Yes) {
                return;
            }

            auto stat = m_cv->charState()->getStatByName(QString(statName) + ":total");
            careerBPOff()->amount[stat] += amount;
            careerBPOff()->amount[m_cv->charState()->getStatByName("rp:total")] += -1;

            m_cv->updateView();
        });
        
        return button;
    };
    rpContainer->addRow(rpSpend("Buy CP", "cp", 1));
    rpContainer->addRow(rpSpend("Buy aptitude point", "aptitude", 1));
    rpContainer->addRow(rpSpend("Buy skill points", "active-skills", 5));
    rpContainer->addRow(rpSpend("Buy rep", "rep", 5));
    auto syncRp = new QPushButton("Finalize RP spending");
    rpContainer->addRow("", syncRp);
    connect(syncRp, &QPushButton::clicked, this, &CharacterCareer::syncBPOff);
    auto addRp = new QPushButton("Add RP reward...");
    rpContainer->addRow("", addRp);
    connect(addRp, &QPushButton::clicked, [=]() {
        bool ok = false;
        int result = QInputDialog::getInt(this, "RP gain", "Congratulations! How much?", 1, 1, 100, 1, &ok);
        if(!ok) return;

        careerBPOff()->amount[m_cv->charState()->getStatByName("rp:total")] += result;

        m_cv->updateView();
    });

    careerLayout->addStretch();
}

void CharacterCareer::updateDisplay() {
    auto cs = m_cv->ego()->state()->calculate();
    updateLabelsWith(cs);

    auto career = m_cv->ego()->col()->map("career");
    if(career == "") {
        m_displayStack->setCurrentIndex(0);
    }
    else {
        m_displayStack->setCurrentIndex(1);
    }
}

Data::OffsetModifier *CharacterCareer::careerBPOff() {
    /* XXX: assumption is that there's only one setup-stage modifier, and that
        is the BP offsets. */
    for(auto c : m_cv->ego()->col()->collection()) {
        if(c.first == "setup-stage") return dynamic_cast<Data::OffsetModifier *>(c.second);
    }
    return nullptr;
    
}

void CharacterCareer::syncBPOff() {
    auto state = m_cv->ego()->state();
    auto bpoff = careerBPOff();
    std::map<QString, int> used, allowed;

    auto cs = state->calculate();
    for(auto bp : state->getStatsByTag("bp:used")) {
        used[bp->longDesc()] = cs.values[bp].die(0);
    }
    for(auto bp : state->getStatsByTag("bp:total")) {
        allowed[bp->longDesc()] = cs.values[bp].die(0);
    }

    for(auto u : used) {
        if(u.second > allowed[u.first]) {
            u.second = allowed[u.first];
        }
    }

    for(auto bp : state->getStatsByTag("bp:total")) {
        bpoff->amount[bp] += -used[bp->longDesc()];
    }
    for(auto bp : state->getStatsByTag("bp:used")) {
        bpoff->amount[bp] += -used[bp->longDesc()];
    }

    m_cv->updateView();
}

std::vector<CharacterCareer::CareerEvent *> CharacterCareer::parseEvents() {
    auto ego = m_cv->ego()->col();

    int count = ego->map("event-count").toInt();

    std::vector<CharacterCareer::CareerEvent *> ret;

    for(int i = 0; i < count; i ++) {
        ret.push_back(parseEvent(ego->map("event-" + QString::number(i))));
    }

    return ret;
}


CharacterCareer::CareerEvent *CharacterCareer::parseEvent(QString from) {
    auto rootSplit = from.split("::");
    auto fields = rootSplit.front().split(":");
    auto type = fields[0];

    CareerEvent *ret = new CareerEvent();
    ret->desc = rootSplit.back();

    if(type == "session") {
        ret->type = CareerEvent::Session;
    }
}

void CharacterCareer::addEvent(CharacterCareer::CareerEvent *event) {
    
}

#pragma once

#include <QTabWidget>
#include <QLabel>

#include "data/Character.h"

class QTextStream;

class CharacterView;
class CharacterWidget;
class StatLabel;

class CharacterRoot : public QWidget { Q_OBJECT
private:
    Data::Character *m_character;
    QTabWidget *m_characterTabs;

    CharacterView *m_cv;
    std::vector<CharacterWidget *> m_widgets;
    CharacterWidget *m_lastWidget;

    QString m_filename;
public:
    CharacterRoot(Data::Character *character);
    ~CharacterRoot();

    Data::Character *character() const { return m_character; }
    CharacterView *cv() const { return m_cv; }

    const QString &filename() const { return m_filename; }
    void setFilename(const QString &fn) { m_filename = fn; }

    void textDump(QTextStream &stream);
};

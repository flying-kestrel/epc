#pragma once

#include <QMainWindow>

namespace Data {
class State;
} // namespace Data

class QTabWidget;

class RootWindow : public QMainWindow { Q_OBJECT
private:
    QTabWidget *m_central;
    Data::State *m_coreState, *m_globalState;
public:
    RootWindow();
    virtual ~RootWindow() {}
private:
    void buildMenus();
    void loadData();
private slots:
    void createCharacter();
    void closeCharacter();
    void saveActiveCharacter();
    void saveActiveCharacterAs();
    void loadCharacter();
    void debugCharacterEgo();
    void debugCharacterMorph();

    void textDumpCharacter();

    void loadExtraData();
    void showDataEditor();
};

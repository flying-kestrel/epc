### Cross-compilation Setup ###

Clone (or symlink) mxe into the root of the project and build a shared 32-bit
cross-compilation environment:

    git clone https://github.com/mxe/mxe
    cd mxe ; make MXE_TARGETS=i686-w64-mingw32.shared qt5-base ; cd -

Then build using the `Makefile.win` makefile:

    make -f Makefile.win regen && make


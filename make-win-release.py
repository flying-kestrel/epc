#!/usr/bin/env python3

import sys
import os
import shutil
import subprocess
import hashlib

try:
    shutil.rmtree("upload/win-release")
except:
    pass

os.mkdir("upload/win-release")

try:
    shutil.copy("upload/win-release.txt", "upload/win-old-release.txt")
except:
    pass

latest = open("upload/latest-windows.json", "w")
latest.write("{\n")

def add_to_latest(fname):
    dlpath = os.path.join("win-release", fname)
    path = os.path.join("upload", dlpath)
    latest.write('"%s": "%s",\n' % (dlpath, hashlib.sha256(open(path, "rb").read()).hexdigest()))

def strip(fname):
    print("Stripping %s..." % fname)
    os.system("mxe/usr/bin/i686-w64-mingw32.shared-strip " + fname)

# Begin by including select files from the MXE environment
allfiles = []
try:
    allfiles = [x.strip() for x in open("mxe.index").readlines()]
except:
    pass
if len(allfiles) == 0:
    os.system("find mxe/usr/i686-w64-mingw32.shared > mxe.index")
    allfiles = open("mxe.index").readlines()
allfiles.sort()

include = [x.strip() for x in open("mxe.include").readlines()]

for fn in include:
    # find all matches
    matches = [x for x in allfiles if fn in x]
    # use shortest match
    matches = [(len(x), x) for x in matches]
    assert(len(matches) > 0)
    filepath = matches[0][1].strip()

    target = os.path.join("upload/win-release", fn)
    try:
        os.makedirs(os.path.dirname(target))
    except:
        pass
    shutil.copy(filepath, target)
    strip(target)
    add_to_latest(fn)

# Include executable
shutil.copy("epc.exe", "upload/win-release/")
strip("upload/win-release/epc.exe")
add_to_latest("epc.exe")

# Include XML files from data directory
exts = ['.xml']
datawalk = os.walk("data/")
for d in datawalk:
    os.makedirs(os.path.join("upload/win-release/", d[0]))
    for f in d[2]:
        if not any([f.endswith(ext) for ext in exts]):
            continue
        source = os.path.join(d[0], f)
        shutil.copy(source, os.path.join("upload/win-release/", d[0]))
        add_to_latest(source)

# Include current git revision
current_revision = subprocess.check_output(["git", "rev-parse", "HEAD"]).decode().strip()
open("upload/win-release.txt", "w").write(current_revision)

latest.write('"win-release.txt": "%s"\n' % hashlib.sha256(current_revision.encode()).hexdigest())
latest.write("}\n")

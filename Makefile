.PHONY: all regen regen-win clean clean-win distclean

all:
	-$(MAKE) -C build

win:
	-$(MAKE) -f Makefile.win

regen:
	mkdir -p build
	cd build ; cmake ..

regen-win:
	-$(MAKE) -f Makefile.win regen

clean:
	$(MAKE) -C build clean

clean-win:
	-$(MAKE) -f Makefile.win clean

distclean:
	-rm -rf build/ build-win/
